<?
	$h1    		= 'Saco PEBD';
	$title 		= 'Saco PEBD';
	$desc  		= 'O saco PEBD pode ser personalizado conforme suas necessidades ou preferências. Por exemplo, confeccionando com ou sem impressão e acrescentando...';
	$key   		= 'sacos PEBDs, sacos PEBD, saco PEBDs';
	$legendaImagem 	= 'Foto ilustrativa '.$h1.'';
	$var 		= 'Sacos PEBDs';
	$produtos	= 'active';
	
	include('inc/head.php');
?>
<!-- função tabs regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
<script src="<?=$url;?>js/tabs.js" type="text/javascript"></script>

<!-- Tabs Regiões -->
<link rel="stylesheet" href="<?=$url;?>css/tabs.css" type="text/css" />
</head>
<body>

<div class="wrapper">
<? include('inc/topo.php');?>

    <main role="main">

        <section>

            <article>
            <?=$caminhoServicosDiversos?>  
            	<h1><?=$h1?></h1>
                <div class="picture-legend picture-right">
                    <img src="<?=$url;?><?=$pastaSacosDiversos?><?=$urlGaleria?>-01.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
                </div>
                <p>Embalagens devem ser de qualidade, com materiais resistentes e ideais para armazenar e transportar produtos. Por isso, conheça os <strong>sacos PEBD</strong>.</p>
                <p>Uma embalagem deve combinar características como a qualidade, a segurança e a resistência para que seu produto chegue até o consumidor em perfeitas condições. Por isso, conte com as vantagens do <strong>saco PEBD</strong>, que também é conhecido pela sigla PE.</p>
                <p>O <strong>saco PEBD</strong> (polietileno de baixa densidade) é uma embalagem versátil e utilizando em uma série de segmentos. Isso porque ele tem como características principais a boa resistência à tração e aos rasgos, além de ótima selagem.</p>
                <div class="picture-legend picture-left">
                    <img src="<?=$url;?><?=$pastaSacosDiversos?><?=$urlGaleria?>-02.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
                </div>
                <p>O <strong>saco PEBD</strong> pode ser personalizado conforme suas necessidades ou preferências. Por exemplo, confeccionando com ou sem impressão e acrescentando acessórios como alças, talas, fecho zip e aba adesiva. Também é possível aumentar a segurança do produto fabricando a embalagem com fecho adesivo hotmelt. Desta forma, a embalagem fica inviolável, sendo necessário danificá-la para ter acesso ao que está embalado ou sendo transportado.</p>
                <h2>Saco PEBD reciclado</h2>
                <p>Uma opção para contribuir com as causas ambientais é adotar as embalagens recicladas, que ainda trazem a vantagem de ter um custo em torno de 30% menor que as embalagens convencionais.</p>
                <p>O <strong>saco PEBD</strong> reciclado é produzido a partir de embalagens de alimentos e sacolas. Existem diversos modelos diferentes, confira:</p>
                
                <ul class="list">
                <li>Saco cristal: este modelo de <strong>saco PEBD</strong> é fabricado a partir de aparas de material virgem. Por causa do processo de reciclagem, o saco fica com um aspecto mais amarelado e com pontos, mas a transparência da embalagem é mantida.</li>
                <li>Saco canela: esta embalagem é feita a partir da mistura de aparas do material reciclado, fazendo com que a embalagem fique com um aspecto de cor de canela, mas também mantendo a transparência.</li>
                <li>Saco colorido: o <strong>saco PEBD</strong> colorido é feito misturando vários plásticos, resultando em embalagem sem padrão de cor e sem transparência.</li>
                </ul>
                <div class="picture-legend picture-right">
                    <img src="<?=$url;?><?=$pastaSacosDiversos?><?=$urlGaleria?>-03.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
                </div>
                <p>Outra opção é o saco com aditivo oxibiodegradável, um componente que faz com que a embalagem se degrade bem mais rapidamente em contato com o meio ambiente. Esta é uma opção sustentável e que ainda melhor a imagem da sua empresa, justamente pelo alinhamento com causas ambientais.</p>
                <p>Para adquirir o <strong>saco PEBD</strong>, conte com a JPR Embalagens. Temos as melhores soluções na área de embalagens flexíveis e nossos consultores proporcionam atendimento personalizado e voltado às suas necessidades. Solicite seu orçamento.</p>
                                
            <? include('inc/saiba-mais.php');?>

            </article>

            <? include('inc/coluna-lateral.php');?>

            <br class="clear" />

            <? include('inc/social-media.php');?>

            <? include('inc/regioes.php');?>

            <? include('inc/paginas-relacionadas.php');?>

            

            <? include('inc/copyright.php');?>
        </section>

    </main>

</div><!-- .wrapPEBDr -->
<? include('inc/footer.php');?>
</body>
</html>