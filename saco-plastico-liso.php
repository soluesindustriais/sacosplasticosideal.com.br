<?
	$h1    		= 'Saco Plástico Liso';
	$title 		= 'Saco Plástico Liso';
	$desc  		= 'O saco plástico liso é um produto que tem utilização ampla na área de confecções, laboratórios, gráficas e editoras, que se destaca...';
	$key   		= 'saco plastico Liso, sacos plastico Liso, saco plasticos Liso, saco plastico Lisos, sacos plástico Liso, saco plásticos Liso, saco plástico Lisos';
	$legendaImagem 	= 'Foto ilustrativa '.$h1.'';
	$var 		= 'Sacos Plasticos Liso';
	$produtos	= 'active';
	
	include('inc/head.php');
?>
<!-- função tabs regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
<script src="<?=$url;?>js/tabs.js" type="text/javascript"></script>

<!-- Tabs Regiões -->
<link rel="stylesheet" href="<?=$url;?>css/tabs.css" type="text/css" />
</head>
<body>

<div class="wrapper">
<? include('inc/topo.php');?>

    <main role="main">

        <section>

            <article>
            <?=$caminhoServicosPlasticos?>  
            	<h1><?=$h1?></h1>
                <div class="picture-legend picture-right">
                    <img src="<?=$url;?><?=$pastaSacosPlasticos?><?=$urlGaleria?>-01.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
                </div>
                <p>Confira opção de embalagem que se destaca pela transparência e pelo brilho. Conheça o <strong>saco plástico liso</strong>.</p>
                <p>Se você precisa de uma embalagem que tenha qualidade e proporcione segurança para o seu produto, conte com o <strong>saco plástico liso</strong>.</p>
                <p>O <strong>saco plástico liso</strong> é um produto que tem utilização ampla na área de confecções, laboratórios, gráficas e editoras, que se destaca por ser uma embalagem que tem uma excelente transparência e brilho. Além disso, este tipo de embalagem é empregado para embalar alimentos de giro rápido, como é o caso.</p>
                <div class="picture-legend picture-left">
                    <img src="<?=$url;?><?=$pastaSacosPlasticos?><?=$urlGaleria?>-02.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
                </div>
                <p>O <strong>saco plástico liso</strong> é uma embalagem que se destaca por ser de fácil abertura. Para o fechamento, há opção de ser fabricado com aba adesiva abre e fecha ou permanente. A opção de abre e fecha é altamente indicada para produtos que precisam ser acessados diversas vezes, como é o caso de roupas. Já a aba permanente tem como objetivo fazer com que o <strong>saco plástico liso</strong> liso só possa ser aberta se for totalmente danificada, o que proporciona maior segurança no transporte e no armazenamento de produtos.</p>
                
                <h2>Qualidade do <?=$h1;?></h2>
                
                <p>O <strong>saco plástico liso</strong> pode ser personalizado conforme a necessidade e as preferências do cliente, com possibilidade de fabricação sob medida, sem impressão ou com impressão com diferentes opções de cores.</p>
                <h2>Saco plástico liso sustentável</h2>
                
                <p>Uma opção para quem deseja contribuir com o meio ambiente é o <strong>saco plástico liso</strong> com aditivo oxibiodegradável. Este aditivo é adicionado durante o processo de produção da embalagem, na extrusão do filme. O objetivo dele é fazer com que a embalagem se degrade em curto espaço de tempo ao entrar em contato com o meio ambiente, sem deixar resíduos tóxicos.</p>
                <div class="picture-legend picture-right">
                    <img src="<?=$url;?><?=$pastaSacosPlasticos?><?=$urlGaleria?>-03.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
                </div>
                <p>Adquira já seu <strong>saco plástico liso</strong> convencional ou com aditivo oxibiodegradável na JPR Embalagens, empresa que atua há mais de 15 anos no mercado. Os profissionais da marca são altamente especializados e contam com vasta experiência na área de embalagens flexíveis.</p>
                <p>O <strong>saco plástico liso</strong> fabricado pela JPR Embalagens se destaca no mercado pela qualidade elevada, a segurança e proteção que proporcionam ao seu produto, além de ter produção com custos menores.</p>
                <p>Saiba mais entrando em contato com um dos consultores da empresa e tenha um atendimento totalmente personalizado e direcionado para as suas necessidades. Aproveite e solicite já seu orçamento.</p>
                
            <? include('inc/saiba-mais.php');?>

            </article>

            <? include('inc/coluna-lateral.php');?>

            <br class="clear" />

            <? include('inc/social-media.php');?>

            <? include('inc/regioes.php');?>

            <? include('inc/paginas-relacionadas.php');?>

            

            <? include('inc/copyright.php');?>
        </section>

    </main>

</div><!-- .wrapper -->
<? include('inc/footer.php');?>
</body>
</html>