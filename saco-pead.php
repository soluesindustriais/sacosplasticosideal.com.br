<?
	$h1    		= 'Saco PEAD';
	$title 		= 'Saco PEAD';
	$desc  		= 'O saco PEAD é amplamente utilizado para enviar jornais, revistas, mala direta, objetos leves, para proteção de eletrodoméstico, entre outras funções...';
	$key   		= 'sacos PEADs, sacos PEAD, saco PEADs';
	$legendaImagem 	= 'Foto ilustrativa '.$h1.'';
	$var 		= 'Sacos PEADs';
	$produtos	= 'active';
	
	include('inc/head.php');
?>
<!-- função tabs regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
<script src="<?=$url;?>js/tabs.js" type="text/javascript"></script>

<!-- Tabs Regiões -->
<link rel="stylesheet" href="<?=$url;?>css/tabs.css" type="text/css" />
</head>
<body>

<div class="wrapper">
<? include('inc/topo.php');?>

    <main role="main">

        <section>

            <article>
            <?=$caminhoServicosDiversos?>  
            	<h1><?=$h1?></h1>
                <div class="picture-legend picture-right">
                    <img src="<?=$url;?><?=$pastaSacosDiversos?><?=$urlGaleria?>-01.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
                </div>
                <p>Conte com produtos de qualidade na hora de embalar os seus produtos. Por isso, conheça as vantagens do <strong>saco PEAD</strong>.</p>
                <p>O tipo de embalagem utilizado é essencial para que o seu produto seja conservado durante o armazenamento ou durante o transporte. Por isso, uma opção é o <strong>saco PEAD</strong> (polietileno de alta densidade).</p>
                
                <p>O <strong>saco PEAD</strong> é amplamente utilizado para enviar jornais, revistas, mala direta, objetos leves, para proteção de eletrodoméstico, entre outras funções, justamente pela qualidade e pela segurança que ele oferce. O produto tem matéria prima com maior resistência à tração, e por isso é bastante utilizado para fabricar sacolas, bobinas picotadas, entre outros.</p>
                <div class="picture-legend picture-left">
                    <img src="<?=$url;?><?=$pastaSacosDiversos?><?=$urlGaleria?>-02.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
                </div>
                <p>O <strong>saco PEAD</strong> pode ser liso ou confeccionado de maneira personalizada, com impressão em até seis cores, e é bastante versátil, com possibilidade de ser lacrado com aba adesiva ou seladora manual.</p>
                <p>Uma opção para quem não armazena nem produtos alimentícios e nem medicinais é o <strong>saco PEAD</strong> reciclado. Este modelo de embalagem garante redução de até 30% dos seus custos em relação a embalagens feitas com material virgem. Outra grande vantagem é transmitir uma imagem melhor da sua empresa no mercado, ao indicar, por meio da embalagem, a sua preocupação com as causas ambientais.</p>
                <p>O <strong>saco PEAD</strong> é feito com reciclado cristal, que não tem o aspecto cristalino das embalagens feitas com matéria prima virgem, mas ainda permite total transparência, possibilitando visualizar o que há dentro da embalagem.</p>
                <div class="picture-legend picture-right">
                    <img src="<?=$url;?><?=$pastaSacosDiversos?><?=$urlGaleria?>-03.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
                </div>
                <h2>Saco PEAD da JPR Embalagens</h2>
                <p>E para adquirir <strong>saco PEAD</strong> com ótimas condições de pagamento e preços reduzidos, aproveite os benefícios da JPR Embalagens. A empresa atua na área de embalagens flexíveis a mais de 15 anos, com as melhores soluções para cada caso. A equipe da JPR Embalagens está sempre atualizada para buscar inovações que resultem em produtos com qualidade ainda maior e preços mais reduzidos, e esta queda no preço é repassada para os clientes.</p>
                <p>A JPR Embalagens tem um atendimento personalizado e totalmente voltado às necessidades específicas de cada cliente. Entre em contato com um dos consultores para maiores informações sobre o produto e aproveite as vantagens oferecidas para solicitar já o seu orçamento.</p>
                                
            <? include('inc/saiba-mais.php');?>

            </article>

            <? include('inc/coluna-lateral.php');?>

            <br class="clear" />

            <? include('inc/social-media.php');?>

            <? include('inc/regioes.php');?>

            <? include('inc/paginas-relacionadas.php');?>

            

            <? include('inc/copyright.php');?>
        </section>

    </main>

</div><!-- .wrapper -->
<? include('inc/footer.php');?>
</body>
</html>