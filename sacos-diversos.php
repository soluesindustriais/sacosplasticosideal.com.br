<?
	$h1    		= 'Sacos Diversos';
	$title 		= 'Sacos Diversos';
	$desc  		= 'Confira nossos incríveis modelos de Sacos, são vários estilos, como: Saco Aluminizado, Saco Antiestático, Saco Adesivado Transparente e muito mais!';
	$key   		= 'saco Ziplock, sacos Ziplocks, sacos Ziplock, saco Ziplocks, sacos com Ziplocks, sacos com Ziplock, saco com Ziplocks';
	$var 		= 'Saco Diversos';
	$produtos	= 'active';
	
	include('inc/head.php');
?>
<!-- função tabs regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
<script src="<?=$url;?>js/tabs.js" type="text/javascript"></script>

<!-- Tabs Regiões -->
<link rel="stylesheet" href="<?=$url;?>css/tabs.css" type="text/css" />
</head>
<body>

<div class="wrapper">
<? include('inc/topo.php');?>

    <main role="main">

        <section>

            <article>
            <?=$caminhoServicos?>  
            	<h1><?=$h1?></h1>
                
                <p>Confira abaixo o que a JPR Sacos oferece de melhor tratando-se de Sacos Flexíveis:</p>
                
                <ul class="thumbnails">
                 <li>
                      <a rel="nofollow" href="<?=$url;?>embalagem-saco-plastico" title="Embalagem Saco Plástico"><img src="<?=$url;?><?=$pastaSacosDiversos;?>thumb/embalagem-saco-plastico-01.jpg" alt="Embalagem Saco Plástico" title="Embalagem Saco Plástico" /></a>
                      <h4><a href="<?=$url;?>embalagem-saco-plastico" title="Embalagem Saco Plástico">Embalagem Saco Plástico</a></h4>
                 </li>
                 <li>
                      <a rel="nofollow" href="<?=$url;?>envelope-saco-personalizado" title="Envelope Saco Personalizado"><img src="<?=$url;?><?=$pastaSacosDiversos;?>thumb/envelope-saco-personalizado-01.jpg" alt="Envelope Saco Personalizado" title="Envelope Saco Personalizado" /></a>
                      <h4><a href="<?=$url;?>envelope-saco-personalizado" title="Envelope Saco Personalizado">Envelope Saco Personalizado</a></h4>
                 </li>
                 <li>
                      <a rel="nofollow" href="<?=$url;?>saco-adesivado-transparente" title="Saco Adesivado Transparente"><img src="<?=$url;?><?=$pastaSacosDiversos;?>thumb/saco-adesivado-transparente-01.jpg" alt="Saco Adesivado Transparente" title="Saco Adesivado Transparente" /></a>
                      <h4><a href="<?=$url;?>saco-adesivado-transparente" title="Saco Adesivado Transparente">Saco Adesivado Transparente</a></h4>
                 </li>
                 <li>
                      <a rel="nofollow" href="<?=$url;?>saco-aluminizado" title="Saco Aluminizado"><img src="<?=$url;?><?=$pastaSacosDiversos;?>thumb/saco-aluminizado-01.jpg" alt="Saco Aluminizado" title="Saco Aluminizado" /></a>
                      <h4><a href="<?=$url;?>saco-aluminizado" title="Saco Aluminizado">Saco Aluminizado</a></h4>
                 </li>
                 <li>
                      <a rel="nofollow" href="<?=$url;?>saco-antiestatico" title="Saco Antiestático"><img src="<?=$url;?><?=$pastaSacosDiversos;?>thumb/saco-antiestatico-01.jpg" alt="Saco Antiestático" title="Saco Antiestático" /></a>
                      <h4><a href="<?=$url;?>saco-antiestatico" title="Saco Antiestático">Saco Antiestático</a></h4>
                 </li>
                 <li>
                      <a rel="nofollow" href="<?=$url;?>saco-bolha-antiestatico" title="Saco Bolha Antiestático"><img src="<?=$url;?><?=$pastaSacosDiversos;?>thumb/saco-bolha-antiestatico-01.jpg" alt="Saco Bolha Antiestático" title="Saco Bolha Antiestático" /></a>
                      <h4><a href="<?=$url;?>saco-bolha-antiestatico" title="Saco Bolha Antiestático">Saco Bolha Antiestático</a></h4>
                 </li>
                 <li>
                      <a rel="nofollow" href="<?=$url;?>saco-bopp" title="Saco BOPP"><img src="<?=$url;?><?=$pastaSacosDiversos;?>thumb/saco-bopp-01.jpg" alt="Saco BOPP" title="Saco BOPP" /></a>
                      <h4><a href="<?=$url;?>saco-bopp" title="Saco BOPP">Saco BOPP</a></h4>
                 </li>
                 <li>
                      <a rel="nofollow" href="<?=$url;?>saco-fecho" title="Saco Com Fecho"><img src="<?=$url;?><?=$pastaSacosDiversos;?>thumb/saco-fecho-01.jpg" alt="Saco Com Fecho" title="Saco Com Fecho" /></a>
                      <h4><a href="<?=$url;?>saco-fecho" title="Saco Com Fecho">Saco Com Fecho</a></h4>
                 </li>
                 <li>
                      <a rel="nofollow" href="<?=$url;?>saco-entulho" title="Saco de Entulho"><img src="<?=$url;?><?=$pastaSacosDiversos;?>thumb/saco-entulho-01.jpg" alt="Saco de Entulho" title="Saco de Entulho" /></a>
                      <h4><a href="<?=$url;?>saco-entulho" title="Saco de Entulho">Saco de Entulho</a></h4>
                 </li>
                 <li>
                      <a rel="nofollow" href="<?=$url;?>saco-polietileno-baixa-densidade" title="Saco de Polietileno de Baixa Densidade"><img src="<?=$url;?><?=$pastaSacosDiversos;?>thumb/saco-polietileno-baixa-densidade-01.jpg" alt="Saco de Polietileno de Baixa Densidade" title="Saco de Polietileno de Baixa Densidade" /></a>
                      <h4><a href="<?=$url;?>saco-polietileno-baixa-densidade" title="Saco de Polietileno de Baixa Densidade">Saco de Polietileno de Baixa Densidade</a></h4>
                 </li>
                 <li>
                      <a rel="nofollow" href="<?=$url;?>saco-hamper" title="Saco Hamper"><img src="<?=$url;?><?=$pastaSacosDiversos;?>thumb/saco-hamper-01.jpg" alt="Saco Hamper" title="Saco Hamper" /></a>
                      <h4><a href="<?=$url;?>saco-hamper" title="Saco Hamper">Saco Hamper</a></h4>
                 </li>
                 <li>
                      <a rel="nofollow" href="<?=$url;?>saco-infectante" title="Saco Infectante"><img src="<?=$url;?><?=$pastaSacosDiversos;?>thumb/saco-infectante-01.jpg" alt="Saco Infectante" title="Saco Infectante" /></a>
                      <h4><a href="<?=$url;?>saco-infectante" title="Saco Infectante">Saco Infectante</a></h4>
                 </li>
                 <li>
                      <a rel="nofollow" href="<?=$url;?>saco-metalizado" title="Saco Metalizado"><img src="<?=$url;?><?=$pastaSacosDiversos;?>thumb/saco-metalizado-01.jpg" alt="Saco Metalizado" title="Saco Metalizado" /></a>
                      <h4><a href="<?=$url;?>saco-metalizado" title="Saco Metalizado">Saco Metalizado</a></h4>
                 </li>
                 <li>
                      <a rel="nofollow" href="<?=$url;?>saco-metalizado-zip" title="Saco Metalizado Com Zip"><img src="<?=$url;?><?=$pastaSacosDiversos;?>thumb/saco-metalizado-zip-01.jpg" alt="Saco Metalizado Com Zip" title="Saco Metalizado Com Zip" /></a>
                      <h4><a href="<?=$url;?>saco-metalizado-zip" title="Saco Metalizado Com Zip">Saco Metalizado Com Zip</a></h4>
                 </li>
                 <li>
                      <a rel="nofollow" href="<?=$url;?>saco-cadaver" title="Saco para Cadáver"><img src="<?=$url;?><?=$pastaSacosDiversos;?>thumb/saco-cadaver-01.jpg" alt="Saco para Cadáver" title="Saco para Cadáver" /></a>
                      <h4><a href="<?=$url;?>saco-cadaver" title="Saco para Cadáver">Saco para Cadáver</a></h4>
                 </li>
                 <li>
                      <a rel="nofollow" href="<?=$url;?>saco-pe" title="Saco PE"><img src="<?=$url;?><?=$pastaSacosDiversos;?>thumb/saco-pe-01.jpg" alt="Saco PE" title="Saco PE" /></a>
                      <h4><a href="<?=$url;?>saco-pe" title="Saco PE">Saco PE</a></h4>
                 </li>
                 <li>
                      <a rel="nofollow" href="<?=$url;?>saco-pead" title="Saco PEAD"><img src="<?=$url;?><?=$pastaSacosDiversos;?>thumb/saco-pead-01.jpg" alt="Saco PEAD" title="Saco PEAD" /></a>
                      <h4><a href="<?=$url;?>saco-pead" title="Saco PEAD">Saco PEAD</a></h4>
                 </li>
                 <li>
                      <a rel="nofollow" href="<?=$url;?>saco-pead-transparente" title="Saco PEAD Transparente"><img src="<?=$url;?><?=$pastaSacosDiversos;?>thumb/saco-pead-transparente-01.jpg" alt="Saco PEAD Transparente" title="Saco PEAD Transparente" /></a>
                      <h4><a href="<?=$url;?>saco-pead-transparente" title="Saco PEAD Transparente">Saco PEAD Transparente</a></h4>
                 </li>
                 <li>
                      <a rel="nofollow" href="<?=$url;?>saco-tipo-fronha" title="Saco Tipo Fronha"><img src="<?=$url;?><?=$pastaSacosDiversos;?>thumb/saco-tipo-fronha-01.jpg" alt="Saco Tipo Fronha" title="Saco Tipo Fronha" /></a>
                      <h4><a href="<?=$url;?>saco-tipo-fronha" title="Saco Tipo Fronha">Saco Tipo Fronha</a></h4>
                 </li>
                 <li>
                      <a rel="nofollow" href="<?=$url;?>saco-transparente-embalagem" title="Saco Transparente para Embalagem"><img src="<?=$url;?><?=$pastaSacosDiversos;?>thumb/saco-transparente-embalagem-01.jpg" alt="Saco Transparente para Embalagem" title="Saco Transparente para Embalagem" /></a>
                      <h4><a href="<?=$url;?>saco-transparente-embalagem" title="Saco Transparente para Embalagem">Saco Transparente para Embalagem</a></h4>
                 </li>
                 <li>
                      <a rel="nofollow" href="<?=$url;?>saco-transparente-presente" title="Saco Transparente para Presente"><img src="<?=$url;?><?=$pastaSacosDiversos;?>thumb/saco-transparente-presente-01.jpg" alt="Saco Transparente para Presente" title="Saco Transparente para Presente" /></a>
                      <h4><a href="<?=$url;?>saco-transparente-presente" title="Saco Transparente para Presente">Saco Transparente para Presente</a></h4>
                 </li>
                 <li>
                      <a rel="nofollow" href="<?=$url;?>saco-ziplock" title="Saco Ziplock"><img src="<?=$url;?><?=$pastaSacosDiversos;?>thumb/saco-ziplock-01.jpg" alt="Saco Ziplock" title="Saco Ziplock" /></a>
                      <h4><a href="<?=$url;?>saco-ziplock" title="Saco Ziplock">Saco Ziplock</a></h4>
                 </li>
                 <li>
                      <a rel="nofollow" href="<?=$url;?>sacos-adesivados" title="Sacos Adesivados"><img src="<?=$url;?><?=$pastaSacosDiversos;?>thumb/sacos-adesivados-01.jpg" alt="Sacos Adesivados" title="Sacos Adesivados" /></a>
                      <h4><a href="<?=$url;?>sacos-adesivados" title="Sacos Adesivados">Sacos Adesivados</a></h4>
                 </li>
                 <li>
                      <a rel="nofollow" href="<?=$url;?>sacos-polietileno" title="Sacos de Polietileno"><img src="<?=$url;?><?=$pastaSacosDiversos;?>thumb/sacos-polietileno-01.jpg" alt="Sacos de Polietileno" title="Sacos de Polietileno" /></a>
                      <h4><a href="<?=$url;?>sacos-polietileno" title="Sacos de Polietileno">Sacos de Polietileno</a></h4>
                 </li>
                 <li>
                      <a rel="nofollow" href="<?=$url;?>sacos-polipropileno" title="Sacos de Polipropileno"><img src="<?=$url;?><?=$pastaSacosDiversos;?>thumb/sacos-polipropileno-01.jpg" alt="Sacos de Polipropileno" title="Sacos de Polipropileno" /></a>
                      <h4><a href="<?=$url;?>sacos-polipropileno" title="Sacos de Polipropileno">Sacos de Polipropileno</a></h4>
                 </li>
                 <li>
                      <a rel="nofollow" href="<?=$url;?>sacos-pebd" title="Sacos PEBD"><img src="<?=$url;?><?=$pastaSacosDiversos;?>thumb/sacos-pebd-01.jpg" alt="Sacos PEBD" title="Sacos PEBD" /></a>
                      <h4><a href="<?=$url;?>sacos-pebd" title="Sacos PEBD">Sacos PEBD</a></h4>
                 </li>
                 <li>
                      <a rel="nofollow" href="<?=$url;?>sacos-reciclados" title="Sacos Reciclados"><img src="<?=$url;?><?=$pastaSacosDiversos;?>thumb/sacos-reciclados-01.jpg" alt="Sacos Reciclados" title="Sacos Reciclados" /></a>
                      <h4><a href="<?=$url;?>sacos-reciclados" title="Sacos Reciclados">Sacos Reciclados</a></h4>
                 </li>
                 <li>
                      <a rel="nofollow" href="<?=$url;?>sacos-valvulados" title="Sacos Valvulados"><img src="<?=$url;?><?=$pastaSacosDiversos;?>thumb/sacos-valvulados-01.jpg" alt="Sacos Valvulados" title="Sacos Valvulados" /></a>
                      <h4><a href="<?=$url;?>sacos-valvulados" title="Sacos Valvulados">Sacos Valvulados</a></h4>
                 </li>
                 <li>
                      <a rel="nofollow" href="<?=$url;?>sacos-zip-personalizados" title="Sacos ZIP Personalizados"><img src="<?=$url;?><?=$pastaSacosDiversos;?>thumb/sacos-zip-personalizados-01.jpg" alt="Sacos ZIP Personalizados" title="Sacos ZIP Personalizados" /></a>
                      <h4><a href="<?=$url;?>sacos-zip-personalizados" title="Sacos ZIP Personalizados">Sacos ZIP Personalizados</a></h4>
                 </li>
            </ul>
                
            <? include('inc/saiba-mais.php');?>

            </article>

            <? include('inc/coluna-lateral.php');?>

            <br class="clear" />

            <? include('inc/social-media.php');?>

            <? include('inc/regioes.php');?>

            <? include('inc/paginas-relacionadas.php');?>

            

            <? include('inc/copyright.php');?>
        </section>

    </main>

</div><!-- .wrapper -->
<? include('inc/footer.php');?>
</body>
</html>