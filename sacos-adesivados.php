<?
	$h1    		= 'Sacos Adesivados';
	$title 		= 'Sacos Adesivados';
	$desc  		= 'Os sacos adesivados podem ser fabricados em diversos materiais, como PEAD, PEBD, PP, BOPP e outros. Eles são ideais para embalar...';
	$key   		= 'saco adesivado, sacos adesivado, saco adesivados';
	$legendaImagem 	= 'Foto ilustrativa '.$h1.'';
	$var 		= 'Saco Adesivado';
	$produtos	= 'active';
	
	include('inc/head.php');
?>
<!-- função tabs regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
<script src="<?=$url;?>js/tabs.js" type="text/javascript"></script>

<!-- Tabs Regiões -->
<link rel="stylesheet" href="<?=$url;?>css/tabs.css" type="text/css" />
</head>
<body>

<div class="wrapper">
<? include('inc/topo.php');?>

    <main role="main">

        <section>

            <article>
            <?=$caminhoServicosDiversos?>  
            	<h1><?=$h1?></h1>
                <div class="picture-legend picture-right">
                    <img src="<?=$url;?><?=$pastaSacosDiversos?><?=$urlGaleria?>-01.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
                </div>
                <p>Conte com embalagens de qualidade e que oferecem segurança para armazenar e transportar os seus produtos. Conheça os <strong>sacos adesivados</strong>.</p>
                <p>Na hora de investir em uma embalagem, escolha opções de qualidade elevada para proteção adequada, como é o caso dos <strong>sacos adesivados</strong>.</p>
                <p>Os <strong>sacos adesivados</strong> podem ser fabricados em diversos materiais, como PEAD, PEBD, PP, BOPP e outros. Eles são ideais para embalar produtos que precisam de segurança para serem armazenados ou transportados.</p>
                <div class="picture-legend picture-left">
                    <img src="<?=$url;?><?=$pastaSacosDiversos?><?=$urlGaleria?>-02.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
                </div>
                <p>Os <strong>sacos adesivados</strong> podem ser feitos com diferentes tipos de sistemas de adesivo. Um deles é o adesivo abre e fecha, que é indicado para embalagens que precisam ser abertas várias vezes. Este tipo de embalagem é bastante usado em confecções. Também é possível adicionar adesivo hotmelt, que é permanente e tem a função de tornar a embalagem inviolável. Neste caso, é necessário danificar a embalagem para abri-la.</p>
                <p>Os <strong>sacos adesivados</strong> são uma embalagem com sistema de fechamento rápido, permitindo embalar mais produtos por muito, ou seja, ela é uma embalagem que otimiza processos e tempos. Além disso, dispensa a utilização de seladoras e proporciona maior segurança e acabamento ao produto.</p>
                <h2>Sacos adesivados ecologicamente corretos</h2>
                <p>Uma maneira de cuidar do meio ambiente é utilizando sacos adesivos feitos com matéria-prima reciclada. Além da questão da sustentabilidade do planeta, este tipo de saco é ideal para reduzir os seus custos com embalagem.</p>
                <p>Também é possível adquirir os <strong>sacos adesivados</strong> feitos com aditivo oxibiodegradável, um componente utilizado durante a fabricação que faz com que a embalagem se degrade em um período de até seis meses em contato com o meio ambiente.</p>
                <div class="picture-legend picture-right">
                    <img src="<?=$url;?><?=$pastaSacosDiversos?><?=$urlGaleria?>-03.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
                </div>
                <p>Estas opções são também uma forma de melhorar a imagem da sua empresa no mercado, justamente porque, ao receber a embalagem, o cliente fica atento à sua preocupação com as causas ambientais.</p>
                <p>Para adquirir os <strong>sacos adesivados</strong>, conte com os benefícios da JPR Embalagens. Com mais de 15 anos de atuação no mercado, a empresa leva até os clientes as melhores soluções na área de embalagens flexíveis.</p>
                <p>Os <strong>sacos adesivados</strong> podem ser feitos sob medida, conforme a sua necessidade. Entre em contato com um dos consultores para saber mais e solicite seu orçamento.</p>
                
            <? include('inc/saiba-mais.php');?>

            </article>

            <? include('inc/coluna-lateral.php');?>

            <br class="clear" />

            <? include('inc/social-media.php');?>

            <? include('inc/regioes.php');?>

            <? include('inc/paginas-relacionadas.php');?>

            

            <? include('inc/copyright.php');?>
        </section>

    </main>

</div><!-- .wrapper -->
<? include('inc/footer.php');?>
</body>
</html>