<?
	$h1    		= 'Saco Hamper';
	$title 		= 'Saco Hamper';
	$desc  		= 'O saco hamper é ideal para embalar e transportar roupas sujas que foram utilizadas em hospitais, consultórios e outros ambientes médicos.';
	$key   		= 'sacos hampers, sacos hamper, saco hampers';
	$legendaImagem 	= 'Foto ilustrativa '.$h1.'';
	$var 		= 'Sacos Hamper';
	$produtos	= 'active';
	
	include('inc/head.php');
?>
<!-- função tabs regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
<script src="<?=$url;?>js/tabs.js" type="text/javascript"></script>

<!-- Tabs Regiões -->
<link rel="stylesheet" href="<?=$url;?>css/tabs.css" type="text/css" />
</head>
<body>

<div class="wrapper">
<? include('inc/topo.php');?>

    <main role="main">

        <section>

            <article>
            <?=$caminhoServicosDiversos?>  
            	<h1><?=$h1?></h1>
                <div class="picture-legend picture-right">
                    <img src="<?=$url;?><?=$pastaSacosDiversos?><?=$urlGaleria?>-01.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
              	</div>
                <p>Em ambientes hospitalares, a higiene é um fator essencial. Por isso, a indicação é utilizar o <strong>saco hamper</strong>. Confira vantagens desta embalagem.</p>
                <p>Em hospitais, enfermarias e outros ambientes relacionados à saúde, a higiene é sempre um ponto fundamental e que jamais deve ser deixado de lado. O lixo produzido nestes locais deve ser descartado de maneira adequada e de forma cautelosa, para que não contamine o ambiente e nem afete a saúde de outros pacientes e pessoas que estejam no local. Pensando em todas estas questões, foi desenvolvido o <strong>saco hamper</strong>.</p>
                <div class="picture-legend picture-left">
                    <img src="<?=$url;?><?=$pastaSacosDiversos?><?=$urlGaleria?>-02.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
              	</div>
                <p>O <strong>saco hamper</strong> é ideal para embalar e transportar roupas sujas que foram utilizadas em hospitais, consultórios e outros ambientes médicos. Este modelo de embalagem é fabricado com um dispositivo de fechamento seguro e totalmente higiênico, que é acoplado à boca do saco, proporcionando também praticidade.</p>
                <p>O <strong>saco hamper</strong> é feito com matéria-prima 100% virgem de PEAD, que é o polietileno de alta residente. A embalagem possui excelente resistência mecânica, e o polietileno, por ser uma resina de aspecto fosco, proporciona a opacidade necessária para este tipo de aplicação.</p> 
                <h2>Saco hamper com preço reduzido da JPR Embalagens</h2>
                <div class="picture-legend picture-right">
                    <img src="<?=$url;?><?=$pastaSacosDiversos?><?=$urlGaleria?>-03.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
              	</div>
                <p>E para adquirir <strong>saco hamper</strong> com preços em conta e ótimas condições de pagamento, conte com a JPR Embalagens. No mercado há mais de 15 anos, a empresa tem as melhores soluções quando o assunto é embalagens flexíveis e conta com profissionais sempre atualizados, que buscam inovações para aumentar cada vez mais a qualidade dos produtos e reduzir custos para os clientes.</p>
                <p>O <strong>saco hamper</strong> da JPR Embalagens pode embalar até 120 litros e tem medidas padrões de 90 cm de largura x 110 cm de comprimento ou 90 cm de largura x 100 cm de largura. Há opções nas cores verde, vermelho, amarelo e azul.</p>
                <p>Na JPR Embalagens, você encontra todos estes modelos de <strong>saco hamper</strong>, fabricados com materiais de alta qualidade, que proporcionam segurança, resistência e higiene para este tipo de embalagem.</p>
                <p>Os consultores da empresa proporcionam um atendimento totalmente personalizado e voltado às suas necessidades. Entre em contato para saber maiores informações, esclarecer dúvidas e solicitar já seu orçamento.</p>
                
            <? include('inc/saiba-mais.php');?>

            </article>

            <? include('inc/coluna-lateral.php');?>

            <br class="clear" />

            <? include('inc/social-media.php');?>

            <? include('inc/regioes.php');?>

            <? include('inc/paginas-relacionadas.php');?>

            

            <? include('inc/copyright.php');?>
        </section>

    </main>

</div><!-- .wrapper -->
<? include('inc/footer.php');?>
</body>
</html>