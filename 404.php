<?
	$h1    		= 'Página não encontrada';
	$title 		= 'Página não encontrada';
	$desc  		= '';
	$key   		= '';
	$var   		= 'Página não encontrada';
	
	include('inc/head.php');
?>
</head>
<body>

<div class="wrapper">
<? include('inc/topo.php');?>

    <main role="main">
    
        <section class="page-404">
            <article>
                <?=$caminho?>      
                <h1><?=$h1?></h1>
                
                <p class="msg-404">Desculpe!<br />
                    Escolha abaixo a página que deseja visualizar.
                </p>

                <ul class="sitemap">
                    <? include('inc/sub-menu.php');?>
                </ul>
                
            </article>
        </section>
    </main>

</div><!-- .wrapper -->
<? include('inc/footer.php');?>

</body>
</html>