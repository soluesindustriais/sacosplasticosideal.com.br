<?
	$h1    		= 'Quem Somos';
	$title 		= 'Empresa';
	$desc  		= 'Há mais de 15 anos a Embalagem Ideal possui experiência na fabricação de embalagens plásticas dos mais variados tipos. Confira!';
	$key   		= 'saco Ziplock, sacos Ziplocks, sacos Ziplock, saco Ziplocks, sacos com Ziplocks, sacos com Ziplock, saco com Ziplocks';
	$var 		= 'Empresa';
	$empresa	= 'active';
	
	include('inc/head.php');
?>
</head>
<body>

<div class="wrapper">
<? include('inc/topo.php');?>

    <main role="main">

        <section>

            <article>
            <?=$caminho?>  
            	<h1><?=$h1?></h1>
                
                <p>A Embalagem Ideal é uma empresa séria e altamente qualificada no segmento de embalagens flexíveis.</p>
                <p>Atuante no mercado há mais de 15 anos, dispõem de inúmeros profissionais e de uma assistência técnica impecável para fazer dela, a melhor fábrica quando o assunto é embalagens plásticas.</p>
                <p>O seu nome faz todo o sentido, já que para cada projeto a empresa idealiza a Embalagem Ideal de acordo com a necessidade do cliente.</p>
                <p>Tendo como foco o produto final a ser entregue, a empresa oferece um atendimento personalizado e ótimo custo benefício. Sua meta é sempre progredir, e com isso está sempre atenta na busca de reduzir perdas e custos.</p>
                <p>Há diversos fatores que fazem da Embalagem Ideal a melhor empresa do segmento, mas todo esse sucesso não foi alcançando por acaso, pois ao longo de sua existência a empresa fechou fortes parcerias com as mais importantes fábricas no ramo de embalagens flexíveis.</p>
                
                <img src="<?=$url;?>imagens/missao-jpr-embalagens.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                
            </article>

        </section>

    </main>

</div><!-- .wrapper -->
<? include('inc/footer.php');?>
</body>
</html>