<?
	$h1    		= 'Saco Infectante';
	$title 		= 'Saco Infectante';
	$desc  		= 'O saco infectante é um produto amplamente usado na rede médico-hospitalar para embalar resíduos infectantes. Eles são fabricados em polietileno...';
	$key   		= 'sacos Infectantes, sacos Infectante, saco Infectantes';
	$legendaImagem 	= 'Foto ilustrativa '.$h1.'';
	$var 		= 'Sacos Infectantes';
	$produtos	= 'active';
	
	include('inc/head.php');
?>
<!-- função tabs regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
<script src="<?=$url;?>js/tabs.js" type="text/javascript"></script>

<!-- Tabs Regiões -->
<link rel="stylesheet" href="<?=$url;?>css/tabs.css" type="text/css" />
</head>
<body>

<div class="wrapper">
<? include('inc/topo.php');?>

    <main role="main">

        <section>

            <article>
            <?=$caminhoServicosDiversos?>  
            	<h1><?=$h1?></h1>
                <div class="picture-legend picture-right">
                    <img src="<?=$url;?><?=$pastaSacosDiversos?><?=$urlGaleria?>-01.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
              	</div>
                <p>O cuidado com lixos de ambientes hospitalares é fundamental. Para isso, foi desenvolvido o <strong>saco infectante</strong>. Saiba mais sobre esse tipo de embalagem.</p>

                <p>Em ambientes hospitalares e ambientes médicos de um modo geral é essencial tomar todos os cuidados com o descarte de lixo, para que não haja danos ao meio ambiente e nem se prejudique a saúde de outros pacientes e pessoas presentes no local. Pensando nesses pontos, foi desenvolvido o <strong>saco infectante</strong>.</p>
                
                <p>O <strong>saco infectante</strong> é um produto amplamente usado na rede médico-hospitalar para embalar resíduos infectantes. Eles são fabricados em polietileno de alta densidade (também conhecidos pela sigla PEAD), na cor branco leitoso e vêm com a impressão do símbolo infectante.</p>
                <div class="picture-legend picture-left">
                    <img src="<?=$url;?><?=$pastaSacosDiversos?><?=$urlGaleria?>-02.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
              	</div>
                <p>A confecção do <strong>saco infectante</strong> é feita com solta reforçada, contínua e homogênea no fundo da embalagem. Isso garante excelente vedação, evitando que o produto saia da embalagem. Como é direcionado a produtos infectantes, este modelo de embalagem tem um aspecto opaco, que é resultado da matéria-prima utilizada durante o processo de produção. Desta forma, o <strong>saco infectante</strong> tem perfeita resistência mecânica.</p>
                
                <h2>Saco infectante com preços em conta na JPR Embalagens</h2>
                <p>Na hora de adquirir o <strong>saco infectante</strong>, conte com os benefícios da JPR Embalagens. A empresa atua no mercado há mais de 15 anos, e é especializada em levar até o cliente as melhores soluções em embalagens flexíveis. Os profissionais são atualizados e estão em constante busca por inovações, com o objetivo de melhorar ainda mais a qualidade das embalagens e de reduzir custos para os clientes.</p>
                <div class="picture-legend picture-right">
                    <img src="<?=$url;?><?=$pastaSacosDiversos?><?=$urlGaleria?>-03.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
              	</div>
                <p>A empresa trabalha com ampla linha de <strong>saco infectante</strong>, com os seguintes padrões: para armazenamento de até 15 litros, 30 litros, 50 litros, 100 litros, 200 litros e 240 litros. Todos eles são confeccionados com material de primeira linha e com todos os mecanismos de segurança necessários para que o produto embalado não tenha contato com o ambiente externo.</p>
                
                <p>Os consultores da JPR Embalagens proporcionam um atendimento personalizado, voltado para cada tipo de necessidade. Saiba mais sobre o <strong>saco infectante</strong> e conheça os benefícios da empresa entrando em contato. Solicite também o seu orçamento.</p>
                
            <? include('inc/saiba-mais.php');?>

            </article>

            <? include('inc/coluna-lateral.php');?>

            <br class="clear" />

            <? include('inc/social-media.php');?>

            <? include('inc/regioes.php');?>

            <? include('inc/paginas-relacionadas.php');?>

            

            <? include('inc/copyright.php');?>
        </section>

    </main>

</div><!-- .wrapper -->
<? include('inc/footer.php');?>
</body>
</html>