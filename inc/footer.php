<div class="clear"></div>
<footer>
	<div class="wrapper-footer">
        <div class="contact-footer">
            <address>
                <span><?=$nomeSite;?> - <?=$slogan;?></span>
                <?=$rua;?> - <?=$bairro;?> <br /> 
                <?=$cidade;?> - <?=$UF;?> - <?=$cep;?>
            </address>
            <?=$ddd;?> <strong><?=$fone;?></strong>
        </div>
        
        <div class="menu-footer">
            <nav>
            	<ul>
                    <li><a rel="nofollow" href="<?=$url;?>" title="Página inicial">Home</a></li>
                    <li><a href="<?=$url;?>empresa" title="Sobre a Empresa <?=$nomeSite;?>">Empresa</a></li>
                    <li><a href="<?=$url;?>produtos" title="Produtos <?=$nomeSite;?>">Produtos</a></li>
                    <li><a rel="nofollow" href="<?=$url;?>trabalhe-conosco" title="Trabalhe Conosco">Trabalhe Conosco</a></li>
                    <!-- <li><a rel="nofollow" href="<?=$url;?>contato" title="Pré Orçamento">Pré Orçamento</a></li> -->
                    <li><a href="<?=$url;?>mapa-site" title="Mapa do site <?=$nomeSite;?>">Mapa do site</a></li>
                </ul>
            </nav>  
        </div>
           
        
		<? include('inc/canais.php');?>
        
    <br class="clear" />
    
        <div class="copyright-footer">
            O inteiro teor deste site está sujeito à proteção de direitos autorais. Copyright © <?=$nomeSite;?>. (Lei 9610 de 19/02/1998)
            <div class="selos">
                
                <a rel="nofollow" href="http://validator.w3.org/check?uri=<?=$url;?><?=$urlPagina?>" target="_blank" title="Site Desenvolvido em HTML5 nos padrões internacionais W3C"><img src="<?=$url;?>imagens/selo-w3c-html5.png" alt="Site Desenvolvido em HTML5 nos padrões internacionais W3C" /></a>
                <a rel="nofollow" href="http://jigsaw.w3.org/css-validator/validator?uri=<?=$url;?><?=$urlPagina?>" target="_blank" title="Site Desenvolvido nos padrões internacionais W3C" ><img src="<?=$url;?>imagens/selo-w3c-css.png" alt="Site Desenvolvido nos padrões W3C" /></a>
                <img src="<?=$url;?>imagens/selo-w3c-doutores-web.png" alt="<?=$creditos;?>" title="site desenvolvido por www.doutoresdaweb.com.br" />
            </div>
        </div>
    </div>
</footer>

<div id="ei-modal" class="modal" style="display: none;" onclick="exitModal()">
    <div class="modal centro">
            <img src="<?=$url;?>imagens/banner-orcamentos.jpg" alt="Orçamento apenas pelo formulário de contato" title="Orçamento apenas pelo formulário de contato - click para fechar" />
    </div>
</div>

<!--<script type="text/javascript">
 //<![CDATA[ 
  function initialize() {
        var setCookie = function(cname,cvalue,exdays) {
            var d = new Date();
            d.setTime(d.getTime() + (exdays*24*60*60*1000));
            var expires = "expires=" + d.toGMTString();
            window.document.cookie = cname+"="+cvalue+"; "+expires;
        }
        
        
        var getCookie = function(cname) {
            var name = cname + "=";
            
            var ca = window.document.cookie.split(';');
            for(var i=0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0)==' ') c = c.substring(1);
                if (c.indexOf(name) != -1) {
                    return c.substring(name.length, c.length);
                }
            }
            return "";
        }
        
        var setModal = function (){
            document.getElementById("ei-modal").style.display="block";
        }
        
        if(!getCookie('firstAcess')){
            setCookie('firstAcess' , true ,  365*60);
            setModal();
        }
    }
    
    function exitModal(){
        document.getElementById("ei-modal").style.display="none";
    }
      
    window.onload = initialize();
    //]]>
</script>-->

<script src="https://apis.google.com/js/plusone.js" type="text/javascript"></script>
<script type="text/javascript" src="<?=$url;?>js/geral.js"></script>

<!-- Plugin facebook -->
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/pt_BR/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<!-- Google tag (gtag.js) -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-D23WW3S4NC"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-D23WW3S4NC');
</script>
<!-- Google tag (gtag.js) -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-JXJBP9RGKQ"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-JXJBP9RGKQ');
</script>

<script src="https://solucoesindustriais.com.br/js/dist/sdk-cotacao-solucs/package.js"></script>


    <!-- Script Launch start -->
    <script src="https://ideallaunch.solucoesindustriais.com.br/js/sdk/install.js" defer></script>
    <script>
        const aside = document.querySelector('aside');
        const data = '<div data-sdk-ideallaunch data-segment="Soluções Industriais - Oficial"></div>';
        aside != null ? aside.insertAdjacentHTML('afterbegin', data) : console.log("Não há aside presente para o Launch");
    </script>
    <!-- Script Launch end -->
<?php include 'inc/fancy.php'; ?><div style="display: none" id="exit-banner-div">
    <div data-sdk-ideallaunch="" data-placement="popup_exit" id="exit-banner-container"></div>
</div>

<script src="https://cdn.jsdelivr.net/npm/js-cookie@3.0.5/dist/js.cookie.min.js"></script>

<script src="https://cdn.jsdelivr.net/npm/@fancyapps/ui@5.0/dist/fancybox/fancybox.umd.js"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fancyapps/ui@5.0/dist/fancybox/fancybox.css" />

<script src='https://ideallaunch.solucoesindustriais.com.br/js/sdk/install.js'></script>

<script>
    setTimeout(function() {
        $("html").mouseleave(function() {
            if ($("#exit-banner-container").find('img').length > 0 &&
                !Cookies.get('banner_displayed')) {
                if ($('.fancybox-container').length == 0) {
                    $.fancybox.open({
                        src: '#exit-banner-div',
                        type: 'inline',
                        opts: {
                            afterShow: function() {
                                let minutesBanner = new Date(new Date().getTime() + 5 * 60 * 1000);
                                Cookies.set('banner_displayed', true, { expires: minutesBanner });
                            }
                        }
                    });
                }
            }
        });
    }, 4000);
</script>