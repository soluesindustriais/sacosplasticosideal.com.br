<?
	$creditos			= 'Doutores da Web - Marketing Digital';
	$siteCreditos		= 'www.doutoresdaweb.com.br';
	$nomeSite			= 'JPR Sacos';
	$slogan				= 'Soluções em Embalagens Flexíveis';

	$dir = $_SERVER['SCRIPT_NAME'];
	$dir = pathinfo($dir);
	$host = $_SERVER['HTTP_HOST'];
	$http = $_SERVER['REQUEST_SCHEME'];
	if ($dir["dirname"] == "/") { $url = $http."://".$host."/"; }
	else { $url = $http."://".$host.$dir["dirname"]."/";  }

	$bairro				= 'Jd. São Luiz';
	$cidade				= 'São Paulo';
	$UF					= 'SP';
	$cep				= 'CEP: 05802-140';
	$latitude			= '-23.6636857';
	$longitude			= '-46.7119115';
	$idCliente			= '114';
	$idAnalytics		= 'UA-45914525-13';

	
	$urlPagina = explode("/", $_SERVER['PHP_SELF']);
	$urlPagina	= str_replace('.php','',$urlPagina[sizeof($urlPagina)-1]);
	$urlPagina == "index"? $urlPagina= "" : "";
	$urlCanonical	= "http://" . $_SERVER['SERVER_NAME'] . $_SERVER ['REQUEST_URI'];
	
	//Breadcrumbs
	$caminho 			= '
		<div id="breadcrumb" itemscope itemtype="http://schema.org/Breadcrumb" >
			<a href="'.$url.'" title="Home" itemprop="url"><span  itemprop="title">Home</span></a> » 
			<strong><span class="page">'.$var.'</span></strong>
		</div>
		';

	$caminhoServicos	= '
		<div id="breadcrumb" itemscope itemtype="http://schema.org/Breadcrumb" >
			<a href="'.$url.'" title="Home" itemprop="url"><span itemprop="title">Home</span></a> » 
			<div itemprop="child" itemscope itemtype="http://schema.org/Breadcrumb">
				<a href="'.$url.'produtos" title="Produtos" class="category" itemprop="url"><span itemprop="title"> Produtos </span></a> »
				<div itemprop="child" itemscope itemtype="http://schema.org/Breadcrumb">
					<strong><span class="page" >'.$var.'</span></strong>
				</div>
			</div>
		</div>
		';
		$caminhoServicosPlasticos	= '
		<div id="breadcrumb" itemscope itemtype="http://schema.org/Breadcrumb" >
			<a href="'.$url.'" title="Home" itemprop="url"><span itemprop="title">Home</span></a> » 	
			<a href="'.$url.'produtos" title="Produtos" itemprop="url"><span itemprop="title">Produtos</span></a> » 
			<div itemprop="child" itemscope itemtype="http://schema.org/Breadcrumb">
				<a href="'.$url.'sacos-plasticos" title="Sacos Plásticos" class="category" itemprop="url"><span itemprop="title"> Sacos Plásticos </span></a> »
				<div itemprop="child" itemscope itemtype="http://schema.org/Breadcrumb">
					<strong><span class="page" >'.$var.'</span></strong>
				</div>
			</div>
		</div>
		';
		$caminhoServicosDiversos	= '
		<div id="breadcrumb" itemscope itemtype="http://schema.org/Breadcrumb" >
			<a href="'.$url.'" title="Home" itemprop="url"><span itemprop="title">Home</span></a> » 
			<a href="'.$url.'produtos" title="Produtos" itemprop="url"><span itemprop="title">Produtos</span></a> » 
			<div itemprop="child" itemscope itemtype="http://schema.org/Breadcrumb">
				<a href="'.$url.'sacos-diversos" title="Sacos Diversos" class="category" itemprop="url"><span itemprop="title"> Sacos Diversos </span></a> »
				<div itemprop="child" itemscope itemtype="http://schema.org/Breadcrumb">
					<strong><span class="page" >'.$var.'</span></strong>
				</div>
			</div>
		</div>
		';
	
	//Pasta de imagens, Galeria, url Facebook, etc.
	$pasta 				= 'imagens/servicos/';
	$pastaSacosDiversos = 'imagens/servicos/sacos-diversos/';
	$pastaSacosPlasticos = 'imagens/servicos/sacos-plasticos/';
	$urlGaleria 		= str_replace('.php','',$urlPagina);
		
	//Redes sociais
	$idFacebook			= ''; //Link para achar o ID da página do Face http://graph.facebook.com/Nome da página do Facebook
	
	$idGooglePlus		= 'https://plus.google.com/+SacosplasticosidealBrhome'; // ID da página da empresa no Google Plus
	
	$paginaFacebook		= ''; //Nome da página do Facebook ex: https://www.facebook.com/FacebookDevelopers
	
	$author = ''; // Link do perfil da empresa no g+ ou deixar em branco
	
	$link_tel = str_replace('(', '', $ddd);
	$link_tel = str_replace(')', '', $link_tel);
	$link_tel = str_replace('11', '', $link_tel);
	$link_tel .= '5511'.$fone;
	$link_tel = str_replace('-', '', $link_tel);
	
?>