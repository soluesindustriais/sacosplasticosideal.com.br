<?php
  function tratar_letras($info)
  {
	$info = ereg_replace("[ÁÀÂÃ]","A",$info);
	$info = ereg_replace("[áàâãª]","a",$info);
	$info = ereg_replace("[ÉÈÊË]","E",$info);
	$info = ereg_replace("[éèêë]","e",$info);
	$info = ereg_replace("[ÍÌÎÏ]","I",$info);
	$info = ereg_replace("[íìîï]","i",$info);
	$info = ereg_replace("[ÓÒÔÕ]","O",$info);
	$info = ereg_replace("[óòôõº]","o",$info);
	$info = ereg_replace("[ÚÙÛ]","U",$info);
	$info = ereg_replace("[úùû]","u",$info);
	$info = ereg_replace("ýÿ","y",$info);
	$info = str_replace("Ý","Y",$info);
	$info = str_replace("Ç","C",$info);
	$info = str_replace("ç","c",$info);
	$info = str_replace("Ñ","N",$info);
	$info = str_replace("ñ","n",$info);
	
	$info = strtr($info, "ÀÁÂÃÄÅÃáâãäåÒÓÔÕÖØòóôõöøÈÉÊËèéêëÇçÌÍÎÏìíîïÙÚÛÜùúûüÿÑñ", "AAAAAAaaaaaaOOOOOOooooooEEEEeeeeCcIIIIiiiiUUUUuuuuyNn");
	return $info;
  }  
?>