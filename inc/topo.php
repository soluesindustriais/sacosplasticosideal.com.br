<header>
	<div class="wrapper-top">
        <div class="logo"><a href="<?=$url;?>" title="Voltar a página Home"><?=$slogan;?> - <?=$nomeSite;?></a></div> 
        <div class="slider-wrapper">
            <div id="slider" class="nivoSlider">
                <img src="<?=$url;?>imagens/slider/sacos-embalagens-01.jpg" alt="sacos-embalagens" title="sacos-embalagens" />
                <img src="<?=$url;?>imagens/slider/sacos-embalagens-02.jpg" alt="sacos-embalagens" title="sacos-embalagens"/>
                <img src="<?=$url;?>imagens/slider/sacos-embalagens-03.jpg" alt="sacos-embalagens" title="sacos-embalagens"/>
                <img src="<?=$url;?>imagens/slider/sacos-embalagens-04.jpg" alt="sacos-embalagens" title="sacos-embalagens"/>
                <img src="<?=$url;?>imagens/slider/sacos-embalagens-05.jpg" alt="sacos-embalagens" title="sacos-embalagens"/>
            </div>
		</div>
	</div>
    <div class="clear"></div>
	<nav>
        <ul>
            <? include('inc/menu-top.php');?>
        </ul>
	</nav>

</header>
