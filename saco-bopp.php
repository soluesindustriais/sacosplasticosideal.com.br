<?
	$h1    		= 'Saco BOPP';
	$title 		= 'Saco BOPP';
	$desc  		= 'O saco BOPP pode ser de diversas maneiras. Outro tipo de saco BOPP é o perolizado, muito utilizado para embalagens de chocolates em geral...';
	$key   		= 'sacos BOPPs, sacos BOPP, saco BOPPs';
	$legendaImagem 	= 'Foto ilustrativa '.$h1.'';
	$var 		= 'Sacos BOPPs';
	$produtos	= 'active';
	
	include('inc/head.php');
?>
<!-- função tabs regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
<script src="<?=$url;?>js/tabs.js" type="text/javascript"></script>

<!-- Tabs Regiões -->
<link rel="stylesheet" href="<?=$url;?>css/tabs.css" type="text/css" />
</head>
<body>

<div class="wrapper">
<? include('inc/topo.php');?>

    <main role="main">

        <section>

            <article>
            <?=$caminhoServicosDiversos?>  
            	<h1><?=$h1?></h1>
                <div class="picture-legend picture-right">
                    <img src="<?=$url;?><?=$pastaSacosDiversos?><?=$urlGaleria?>-01.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
                </div>
                <p>Se você precisa de embalagem com brilho e proteção, não deixe de conhecer o <strong>saco BOPP</strong>. Confira as vantagens dessa embalagem.</p>
                <p>É importante investir na embalagem na hora de envolver os seus produtos. Por isso, uma ótima opção é o <strong>saco BOPP</strong>, que oferece brilho e proteção para o seu produto. Este tipo de embalagem é biorientado, ou seja, suas medidas não variam na transversal e longitudinal.</p>
                <p>O <strong>saco BOPP</strong> pode ser de diversas maneiras. O modelo transparente é fabricado com resina de polipropileno, utilizado mais em indústrias alimentícias, já que a estrutura da embalagem conserva melhor os alimentos e proporciona barreira a gases. Já o modelo metalizado é usado normalmente para embalar presentes e também em indústrias alimentícias, já que a metalização da embalagem protege o alimento da luz, fazendo com que o seu período de conservação seja mais prolongado.</p>
                <div class="picture-legend picture-left">
                    <img src="<?=$url;?><?=$pastaSacosDiversos?><?=$urlGaleria?>-02.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
                </div>
                <p>Outro tipo de <strong>saco BOPP</strong> é o perolizado, muito utilizado para embalagens de chocolates em geral e bombons, já que estes doces são muito sensíveis ao calor, além de ser utilizado também como sacos de presentes. Este modelo de embalagem é leve e funcional, ao mesmo tempo que protege.</p>
                <p>Outra vantagem do <strong>saco BOPP</strong> é a possibilidade de personalização de acordo com as suas necessidades. Os fechos desta embalagem podem ser, por exemplo, como aba adesiva, que facilita o fechamento do saco e dispensa o uso de seladoras. Além disso, é possível personalizar a aparência do produto, que pode ser liso ou impresso em até seis cores.</p>
                <h2>Saco BOPP com preço vantajoso</h2>
                <div class="picture-legend picture-right">
                    <img src="<?=$url;?><?=$pastaSacosDiversos?><?=$urlGaleria?>-03.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
                </div>
                <p>E na hora de adquirir o <strong>saco BOPP</strong>, conte com os benefícios da JPR Embalagens. A empresa atua no segmento de embalagens flexíveis a mais de 15 anos, levando até os clientes opções de qualidade, resistência e que oferecem segurança ao produto que foi embalada.</p>
                <p>Os profissionais da JPR Embalagens tem ampla experiência no segmento e estão sempre atualizados para melhorar a qualidade dos produtos e reduzir custos de produção. Os consultores oferecem um atendimento totalmente personalizado e voltado às suas necessidades.</p>
                <p>Entre em contato com a JPR Embalagens para saber mais sobre as vantagens e custos do <strong>saco BOPP</strong>. Solicite já o seu orçamento informando medidas e quantidade que você necessita.</p>
                                                
            <? include('inc/saiba-mais.php');?>

            </article>

            <? include('inc/coluna-lateral.php');?>

            <br class="clear" />

            <? include('inc/social-media.php');?>

            <? include('inc/regioes.php');?>

            <? include('inc/paginas-relacionadas.php');?>

            

            <? include('inc/copyright.php');?>
        </section>

    </main>

</div><!-- .wrapper -->
<? include('inc/footer.php');?>
</body>
</html>