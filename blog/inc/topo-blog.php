<?php if ($pageLoadingAnimation) include('inc/page-loading-animation.php'); ?>

<?php if(!$isMobile) : ?> <!-- DESKTOP -->
    <header id="scrollheader">
        <div class="wrapper">
            <div class="d-flex flex-sm-column flex-align-items-center justify-content-between justify-content-md-center gap-20">
                <div class="logo">
                    <a rel="nofollow" href="<?= $urlBase ?>" title="Voltar a página inicial">
                        <img src="<?= $urlBase ?>imagens//logo.jpg" alt="<?= $nomeSite ?>" title="<?= $nomeSite ?>" width="399" height="88">
                    </a>
                </div>
                <div class="d-flex align-items-center justify-content-end justify-content-md-center gap-20">
                    <nav id="menu">
                        <ul>
                            <? include('inc/menu-top.php'); ?>
                        </ul>
                    </nav>
                    
                    <?php 
                        include('inc/barra-inc.php');
                        include('inc/pesquisa-inc.php');
                    ?>
                </div>
            </div>
        </div>
        <div class="clear"></div>
    </header>
    <div id="header-block"></div>
<?php else: ?> <!-- MOBILE -->
    <header class="header-mobile">
        <div class="wrapper">
            <div class="header-mobile__logo">
                <a rel="nofollow" href="<?=$urlBase?>" title="Voltar a página inicial">                    
                    <img src="<?=$urlBase?>imagens//logo.jpg" alt="<?=$nomeSite?>" title="<?=$nomeSite?>" width="399" height="88">    
                </a>
            </div>
            <div class="header__navigation">
                <!--navbar-->
                <nav id="menu-hamburger">
                    <!-- Collapse button -->
                    <div class="menu__collapse">
                        <button class="collapse__icon" aria-label="Menu">
                            <span class="collapse__icon--1"></span>
                            <span class="collapse__icon--2"></span>
                            <span class="collapse__icon--3"></span>
                        </button>
                    </div>
                    <!-- collapsible content -->
                    <div class="menu__collapsible">
                        <div class="wrapper">
                            <!-- links -->
                            <ul class="menu__items droppable">
                                <? include('inc/menu-top-hamburger.php');?>
                            </ul>
                            <!-- links -->
                        </div>
                        <div class="clear"></div>
                    </div>
                    <!-- collapsible content -->
                </nav>
                <!--/navbar-->
            </div>
        </div>
    </header>

    <!-- <address class="header-mobile-contact">
        <a rel="nofollow" href="tel:<?= $fone[0][0] . $fone[0][1] ?>" title="Clique e ligue"><i class="fas fa-phone"></i></a>
        <?php if(isset($whatsapp)): ?>
            <a data-analytics rel="nofollow" href="<?= wppLink($whatsapp) ?>" target="_blank" title="Whatsapp <?= $nomeSite ?>"><i class="fab fa-whatsapp"></i></a>
        <?php endif; ?>
        <?php if(isset($emailContato)): ?>
            <a rel="nofollow" href="mailto:<?= $emailContato ?>" title="Envie um E-mail"><i class="fas fa-envelope"></i></a>
        <?php endif; ?>
    </address> -->
    
    <? include('inc/barra-inc.php'); ?>
<?php endif; ?>