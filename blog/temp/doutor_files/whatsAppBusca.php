<?php

  $urlProjeto="";
  $urlBlog="";
  $id="";
  $telefone="";
  $mensagem="&text=Ol%C3%A1%2C%20tudo%20bem%3F%20Te%20encontrei%20no%20google%20e%20gostaria%20de%20mais%20informa%C3%A7%C3%B5es.";

?>

<div class="whatsapp">
        <div class="btn-whatsapp" onclick="openModal()">
            <img src="<?php echo $urlBlog; ?>imagens/whatsapp3.svg" alt="WhatsApp" title="WhatsApp">
            <span style="display:none;">1</span>
        </div>
        <div class="modal-whatsapp">
            <div class="whatsapp-header">
                <img src="<?php echo $urlBlog; ?>imagens/logo.png" alt="Logo" title="Logo">
                <h3>Olá! Fale agora pelo WhatsApp </h3>
                <div class="close_modal_Whats" onclick="closeWhatsModal()">
                    <svg width="20px" height="20px" viewBox="0 0 1024 1024" xmlns="http://www.w3.org/2000/svg" fill="#000000">
                        <path fill="#ffffff" d="M195.2 195.2a64 64 0 0 1 90.496 0L512 421.504 738.304 195.2a64 64 0 0 1 90.496 90.496L602.496 512 828.8 738.304a64 64 0 0 1-90.496 90.496L512 602.496 285.696 828.8a64 64 0 0 1-90.496-90.496L421.504 512 195.2 285.696a64 64 0 0 1 0-90.496z"></path>
                    </svg>
                </div>
            </div>
            <div>
                <form action="javascript:void(0);" id="cotacao-whats" class="form-Whats" method="post">
                    <input type="hidden" name="acao" value="cotacao">
                    <input type="hidden" name="produto_nome" value="">
                    <input type="hidden" name="buscazap" value="buscazap2.0">
                    <input class="url-atual" type="hidden" name="produto_url" value="">
                    <input class="url-atual" type="hidden" name="produto_ref" value="">
                    <input type="hidden" name="imagem" value="">
                    <input type="hidden" name="site" value="<?php echo $urlProjeto; ?>">
                    <input type="hidden" name="email" value="whatsapp@whatsapp.co.br">
                    <input type="hidden" name="projeto" value="<?php echo $id; ?>">
                    <p>
                        <label>
                            <input type="hidden" name="nome" value="WhatsApp" id="Input-name" placeholder="Nome" required>
                        </label>
                    </p>
                    <p class="campo_vazio">
                           <label> Insira seu telefone </label>
                            <div class="whats_msg">
                                <input type="text" name="telefone" id="TelWhats" class="telefone" placeholder="( __ ) _____ - ____" required>
                                <span></span>
                            </div>
                      
                    </p>
                    <label>
                        <textarea name="mensagem" id="mensg" placeholder="Mensagem" rows="3" hidden>Url da página: <?= str_replace('www.','',$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'] )?> </textarea>
                    </label>
                    <input type="hidden" name="email_contato" value="form-whats@whatsapp.com.br">
                    <div class="clear"></div>
                    <button type="submit" class="btn_modal_WhatsApp" onclick="errorWhats()">Iniciar conversa</button>
                </form>
            </div>
        </div>
    </div>





<script>

//BuscaZap

const linkWhats ="<?php echo $telefone; ?>";
const mensagem ="<?php echo $mensagem; ?>";

setTimeout(function(){$(".btn-whatsapp span").show()}, 4000);

if(!sessionStorage.getItem("openWhatsApp")){
    setTimeout(function(){$(".modal-whatsapp").show(); sessionStorage.setItem("openWhatsApp", true); }, 4000);
   }

function openModal(){
 
    $('.modal-whatsapp').toggle();
 }
 
function send()
{
    const telWhats = $('#TelWhats').val();
    const numberWhats = telWhats.toString()

    $("#cotacao-whats").on("submit", function(){
    
            $.ajax({
                url: "<?php echo $urlBlog; ?>inc/emailBusca.php",
                dataType: "json",
                type: "POST",
                data: $("#cotacao-whats").serialize(),
                beforeSend: function () {
                    $("#TelWhats").val('');
                    $(".modal-whatsapp").hide();
                    $('.whats_msg span').html('')

                    if($(window).width() <= 768){
                        $(location).attr('href', `https://api.whatsapp.com/send?phone=55${linkWhats}${mensagem}`);
                    }else{
                       window.open(`https://web.whatsapp.com/send?phone=55${linkWhats}${mensagem}`, '_blank');
                    }

                    // toastr.success('Você será direcionado para o WhatsApp', 'Obrigado !!!')
                },
             
                complete: function () {
                 
                }
            });
        
    });

}

function errorWhats(){
   $('.campo_vazio').html('Preencha o campo corretamente.')
  
}
function closeWhatsModal(){
    $('.modal-whatsapp').hide()
};


$("#TelWhats").click(function(){
    const search = document.querySelector('#TelWhats');

  search.addEventListener('input', event =>{
      const count =  event.target.value;
      console.log(count)
      if(count.length <= 13){
        console.log('abaixo')
        $('.whats_msg span').html('<svg style="margin-right:10px;" width="30px" height="40px" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" stroke=""><g id="SVGRepo_bgCarrier" stroke-width="0"></g><g id="SVGRepo_tracerCarrier" stroke-linecap="round" stroke-linejoin="round"></g><g id="SVGRepo_iconCarrier"><path fill-rule="evenodd" clip-rule="evenodd" d="M12 22c5.523 0 10-4.477 10-10S17.523 2 12 2 2 6.477 2 12s4.477 10 10 10zm-1.5-5.009c0-.867.659-1.491 1.491-1.491.85 0 1.509.624 1.509 1.491 0 .867-.659 1.509-1.509 1.509-.832 0-1.491-.642-1.491-1.509zM11.172 6a.5.5 0 0 0-.499.522l.306 7a.5.5 0 0 0 .5.478h1.043a.5.5 0 0 0 .5-.478l.305-7a.5.5 0 0 0-.5-.522h-1.655z" fill="#e60000"></path></g></svg>')
        $('.whats_msg').css({"border":" 2px solid red"})
        $('.btn_modal_WhatsApp').attr('onclick','errorWhats()')
        $('.campo_vazio').html('')
   
    }else{
        $('.whats_msg span').html('<svg style="margin-right:10px;" width="30px" height="40px" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><g id="SVGRepo_bgCarrier" stroke-width="0"></g><g id="SVGRepo_tracerCarrier" stroke-linecap="round" stroke-linejoin="round"></g><g id="SVGRepo_iconCarrier"> <rect width="24" height="24" fill=""></rect> <path fill-rule="evenodd" clip-rule="evenodd" d="M7.25007 2.38782C8.54878 2.0992 10.1243 2 12 2C13.8757 2 15.4512 2.0992 16.7499 2.38782C18.06 2.67897 19.1488 3.176 19.9864 4.01358C20.824 4.85116 21.321 5.94002 21.6122 7.25007C21.9008 8.54878 22 10.1243 22 12C22 13.8757 21.9008 15.4512 21.6122 16.7499C21.321 18.06 20.824 19.1488 19.9864 19.9864C19.1488 20.824 18.06 21.321 16.7499 21.6122C15.4512 21.9008 13.8757 22 12 22C10.1243 22 8.54878 21.9008 7.25007 21.6122C5.94002 21.321 4.85116 20.824 4.01358 19.9864C3.176 19.1488 2.67897 18.06 2.38782 16.7499C2.0992 15.4512 2 13.8757 2 12C2 10.1243 2.0992 8.54878 2.38782 7.25007C2.67897 5.94002 3.176 4.85116 4.01358 4.01358C4.85116 3.176 5.94002 2.67897 7.25007 2.38782ZM15.7071 9.29289C16.0976 9.68342 16.0976 10.3166 15.7071 10.7071L12.0243 14.3899C11.4586 14.9556 10.5414 14.9556 9.97568 14.3899L11 13.3656L9.97568 14.3899L8.29289 12.7071C7.90237 12.3166 7.90237 11.6834 8.29289 11.2929C8.68342 10.9024 9.31658 10.9024 9.70711 11.2929L11 12.5858L14.2929 9.29289C14.6834 8.90237 15.3166 8.90237 15.7071 9.29289Z" fill="#037720"></path> </g></svg>')
        $('.whats_msg').css({"border":" 2px solid green"})
        $('.btn_modal_WhatsApp').attr('onclick','send()')
        $('.campo_vazio').html('')
    }
      if(count.length == 0){
        console.log('abaixo')
        $('.whats_msg span').html('')
        $('.whats_msg').css({"border":" 2px solid  transparent"})
        $('.campo_vazio').html('')
        $('.btn_modal_WhatsApp').attr('onclick','errorWhats()')
      }
  })
});

//Fim Buscazap 
</script>