$(function() {
    // CARREGA O IDIOMA PADRÃO SETADO NA SESSION
    $('.j_retIdioma').each(function() {
        var idioma = $(this).attr('data-idioma');
        var dados = {
            action: 'setIdioma',
            itemId: $(this).attr('data-idioma')
        };
        $.ajax({
            url: "_cdn/ajax/functions.php",
            data: dados,
            type: 'POST',
            dataType: 'json',
            success: function(resposta) {
                if (resposta.error) {
                    $('.j_retIdioma').html('-- Selecione um idioma -- <i class="fa fa-flag"></i>');
                } else if (resposta) {
                    $('.j_retIdioma').html(resposta.callback);
                }
            }
        });
    });
    // DEFINE O IDIOMA DO PROJETO
    $('.j_setIdioma').on('click', function() {
        var idioma = $(this).attr('data-idioma');
        var dados = {
            action: 'setIdioma',
            itemId: $(this).attr('href')
        };
        swal({
                title: "Cadastrar no idioma?",
                text: "Ao confirmar, todo item será cadastrado no idioma " + idioma + " selecionado. Deseja continuar?",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "Não",
                confirmButtonClass: "btn-info",
                confirmButtonText: "Sim",
                closeOnConfirm: false
            },
            function() {
                $.ajax({
                    url: "_cdn/ajax/functions.php",
                    data: dados,
                    type: 'POST',
                    dataType: 'json',
                    success: function(resposta) {
                        console.log(resposta);
                        if (resposta.error) {
                            swal("Ooops!", "Não é possível utilizar este idioma, tente atualizar a página ou trocar para o idioma principal.", "error");
                        } else if (resposta) {
                            $('.j_retIdioma').html(resposta.callback);
                            location.reload();
                        }
                    }
                });
            });
        return false;
    });
    // CARREGA A BANDEIRA APÓS ESCOLHER NO SELECT
    $('.j_flags').click(function() {
        var sl = $(this).is(':selected');
        if (sl) {
            var sg = $(this).attr('data-flag');
            $('.j_bandeira').html("");
            if (sg.length) {
                $('.j_bandeira').html("<i class='flag-icon flag-icon-" + sg + "'></i>");
            } else {
                $('.j_bandeira').html('<i class="fa fa-flag" aria-hidden="true"></i>');
            }
        }
    });
    $('.j_flags').trigger('click');
    // IMPORTAR ARQUIVOS PARA O BANCO
    $('.j_import').submit(function() {
        var form = $(this);
        var action = form.find('input[name="action"]').val();
        form.ajaxSubmit({
            url: "_cdn/ajax/functions.php",
            data: {
                action: action
            },
            dataType: 'json',
            beforeSubmit: function() {
                $('#callbacks').html('');
                form.find('.j_progress').fadeIn();
                form.find('.progress-bar').attr('style', "width: 0%");
            },
            uploadProgress: function(evento, posicao, total, completo) {
                var porcento = completo + '%';
                form.find('.progress-bar').attr('style', "width: " + porcento);
                form.find('.progress-bar').text(porcento);
                if (completo >= '99') {
                    $('.j_progress').fadeOut(function() {
                        form.find('.progress-bar').text('0%');
                        form.find('.progress-bar').attr('style', "width: 0%");
                    });
                }
            },
            success: function(data) {
                if (data.error) {
                    swal("Ooops!", data.error[0], "error");
                } else if (data.error == 0) {
                    swal("Ooops!", "Você não tem permissão para executar esta ação.", "error");
                } else if (data.error == 1) {
                    swal("Ooops!", "Não foi possível recuperar as informações cadastradas, avise um administrador.", "error");
                } else {
                    swal("Tudo certo!", data.result[0], "success");
                    // CLEAR INPUT FILE
                    if (!data.error) {
                        $('#callbacks').addClass('x_panel');
                        $('#callbacks').prepend(data.callback);
                        if (data.import) {
                            swal({
                                title: "Deseja continuar?",
                                text: "Foram encontrados " + data.total + " registro, este processo pode levar alguns minutos para ser concluído, deseja continuar?",
                                type: "warning",
                                showCancelButton: true,
                                confirmButtonClass: "btn-info",
                                confirmButtonText: "Confirmar",
                                closeOnConfirm: true
                            }, function() {
                                function fnRetorno(i, data) {
                                    var soma = i + 1;
                                    if (i >= data.total)
                                        return;
                                    $.ajax({
                                        url: "_cdn/ajax/functions.php",
                                        type: "POST",
                                        dataType: 'json',
                                        data: {
                                            action: 'GravaImport',
                                            dados: JSON.stringify(data.import[i]),
                                            configs: JSON.stringify(data.allDados),
                                            linha: soma
                                        },
                                        success: function(resposta, status) {
                                            console.log(resposta.callbacks);
                                            var porcento = ((resposta.registro * 100) / data.total).toFixed();
                                            $('#callbacks').find(function() {
                                                $('#callbacks').find('.progress-bar').attr('style', "width: " + porcento + "%");
                                                $('#callbacks').find('.progress-bar').text(porcento + "%");
                                                if (porcento == 100) {
                                                    $('#callbacks').find(".alert").removeClass('alert-info');
                                                    $('#callbacks').find(".alert").addClass('alert-success');
                                                    $('#callbacks').find(".text-info").text('Concluído!');
                                                    $('#callbacks').find('.progress-bar').addClass('progress-bar-success');
                                                    $('#callbacks').find(".alert").text('A importação foi finalizada, acesse o modulo para o qual importou.');
                                                    swal("Tudo certo!", "Arquivo importado para o banco!", "success");
                                                }
                                            });
                                            fnRetorno(++i, data);
                                        },
                                    });
                                }
                                fnRetorno(0, data);
                            });
                        }
                    }
                }
            },
        });
        return false;
    });
    $('.j_sendFileIntataneo').on('click', function() {
        $('.j_fundoModal').fadeIn();
        return false;
    });
    $('.j_sendForm').submit(function() {
        var form = $(this);
        var action = form.find('input[name="action"]').val();
        form.ajaxSubmit({
            url: "_cdn/ajax/functions.php",
            data: {
                action: action
            },
            dataType: 'json',
            success: function(data) {
                if (data.error) {
                    swal("Ooops!", data.error[0], "error");
                } else if (data.error == 0) {
                    swal("Ooops!", "Você não tem permissão para executar esta ação.", "error");
                } else if (data.error == 1) {
                    swal("Ooops!", "Não foi possível recuperar as informações cadastradas, avise um administrador.", "error");
                } else {
                    swal("Tudo certo!", data.result[0], "success");
                    // CLEAR INPUT FILE
                    if (!data.error) {
                        form.find('input[type="file"]').val('');
                        form.find('input[type="text"]').val('');
                        form.find('textarea').val('');
                        $('.j_fechar').trigger('click');
                        $('#callbacks').prepend(data.callback);
                    }
                }
            }
        });
        return false;
    });
    // PESQUISA ARQUIVO NA LISTA DURANTE OS CADASTROS
    $('.j_itemFileSearch').on('keyup', function() {
        var pesquisa = $(this).val().toLowerCase();
        var minimo = $(this).attr('data-min-length');
        if (pesquisa.length >= minimo) {
            $(".j_dowloads[data-filter!='" + pesquisa + "']").hide();
            $(".j_dowloads[data-filter*='" + pesquisa + "']").show();
        } else if (pesquisa.length <= minimo) {
            $(".j_dowloads[data-filter]").show();
        }
    });
    // SET NÚMERO DE PÁGINAS POR CONSULTA
    $('.j_change').on('change', function() {
        var quantidade = $(this).val();
        var url = $(this).attr('data-url');
        if (quantidade > 1) {
            location.href = url + '&perpage=' + quantidade;
        }
    });
    // FUNÇÕES DE LOADING SIMPLES
    $(".j_load").on('click', function() {
        $('.loading').fadeIn();
    });
    $("button[type='submit']").on('click', function() {
        $('.loading').fadeIn();
    });
    $("input[type='submit']").on('click', function() {
        $('.loading').fadeIn();
    });
    $('.loading').fadeOut();
    // INPUT DE TAGS
    function onAddTag(tag) {
        alert("Tag adicionada: " + tag);
    }

    function onRemoveTag(tag) {
        alert("Tag removida: " + tag);
    }

    function onChangeTag(input, tag) {
        alert("Tag Alterada: " + tag);
    }
    $('#tags_1').tagsInput({
        width: 'auto'
    });
    // MODALBOX FANCY
    $('.j_modalBox').fancybox({
        openEffect: 'none',
        closeEffect: 'none',
        helpers: {
            media: {}
        }
    });
    // IFRAME RESET AUTO AJUSTA OS VÍDEOS NO NAVEGADOR
    function VideoResize() {
        $('.htmlchars iframe').each(function() {
            var url = $(this).attr("src");
            var char = "?";
            if (url.indexOf("?") != -1) {
                var char = "&";
            }
            var iw = $(this).width();
            var width = $('.htmlchars').outerWidth();
            var height = (iw * 9) / 16;
            $(this).attr({
                'width': width,
                'height': height + 'px'
            });
        });
    }
    VideoResize();
    $(window).resize(function() {
        VideoResize();
    });
    // GET CEP
    $('.j_getCep').change(function() {
        var cep = $(this).val().replace('-', '').replace('.', '');
        if (cep.length === 8) {
            $.get("https://viacep.com.br/ws/" + cep + "/json", function(data) {
                if (!data.erro) {
                    $('.j_bairro').val(data.bairro);
                    $('.j_complemento').val(data.complemento);
                    $('.j_localidade').val(data.localidade);
                    $('.j_logradouro').val(data.logradouro);
                    $('.j_uf').val(data.uf);
                }
            }, 'json');
        }
    });
    // UPLOAD DOS POST´S
    $(".j_post_upload").submit(function() {
        var form = $(this);
        form.ajaxSubmit({
            url: "_cdn/ajax/functions.php",
            data: {
                action: 'sendimage'
            },
            dataType: 'json',
            success: function(data) {
                if (data.error == 0) {
                    swal("Ooops!", "Você não tem permissão para executar esta ação.", "error");
                } else if (data.error == 1) {
                    swal("Ooops!", data.mensagem, "error");
                } else if (data.error == 2) {
                    swal("Ooops!", "Não foi possível inserir a imagem no banco, avise o administrador do sistema.", "error");
                } else if (data.error == 3) {
                    swal("Ooops!", "Não foi possível recuperar os dados da imagem salva no banco de dados, avise o administrador do sistema.", "error");
                } else {
                    // INTERAGE COM TINYMCE
                    if (data.tinyMCE) {
                        tinyMCE.activeEditor.insertContent(data.tinyMCE);
                        $('.j_imageupload_content').slideUp(200, function() {
                            $('.j_imageupload').fadeOut(400);
                        });
                    }
                    // CLEAR INPUT FILE
                    if (!data.error) {
                        form.find('input[type="file"]').val('');
                        //form.find('.file-preview').html('');
                        form.find('.file-caption-name').html('');
                    }
                }
            }
        });
        return false;
    });
    // MODAL UPLOAD
    $('.j_fechar').click(function() {
        $("div#" + $(this).attr("id")).fadeOut("fast");
    });
    $('.j_note').on('click', function() {
        var empresa = $(this).attr('data-nome');
        var id = $(this).attr('rel');
        $('.j_fundoModal').fadeIn(function() {
            var notas = $(this);
            notas.find('h2').html("Cadastre uma nota para " + empresa);
        });
    });
    // CONTADOR DE CARACTERES
    $('.j_cont').append('<p class="caracteres">160 caracteres permitidos</p>');
    $('.j_contChars').on('keydown', function() {
        var max = 160;
        var len = $(this).val().length;
        if (len >= max) {
            $('.caracteres').text('Você atingiu o limite de caracteres');
            $('.j_cont').addClass('text-info');
        } else {
            var ch = max - len;
            $('.caracteres').text(ch + ' caracteres restantes');
            $('.j_cont').removeClass('text-info');
        }
    });
    // TINYMCE BASICO
    if ($('.j_word').length) {
        tinyMCE.init({
            selector: "textarea.j_word",
            language: 'pt_BR',
            menubar: false,
            theme: "modern",
            height: $(this).attr('rows') * 20,
            skin: 'lightgray',
            entity_encoding: "raw",
            theme_advanced_resizing: true,
            plugins: [
                "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                "save table contextmenu directionality emoticons template paste textcolor media"
            ],
            toolbar: "undo redo | insert | styleselect | table | forecolor | backcolor | pastetext | removeformat |  bold | italic | underline | strikethrough | bullist | numlist | alignleft | aligncenter | alignright |  link | unlink | drimage | media |  outdent | indent | fullscreen | preview | code | print",
            content_css: "css/tinyMCE.css",
            style_formats: [{
                title: 'Parágrafo',
                block: 'p'
            }, {
                title: 'Titulo 2',
                block: 'h2'
            }, {
                title: 'Titulo 3',
                block: 'h3'
            }, {
                title: 'Titulo 4',
                block: 'h4'
            }, {
                title: 'Titulo 5',
                block: 'h5'
            }, {
                title: 'Código',
                block: 'pre',
                classes: 'brush: php;'
            }],
            setup: function(editor) {
                editor.addButton('drimage', {
                    title: 'Enviar Imagem',
                    icon: 'image',
                    onclick: function() {
                        $('.j_imageupload').fadeIn(400, function() {
                            $('.j_imageupload_content').slideDown(100);
                        });
                    }
                });
            },
            link_title: true,
            target_list: false,
            theme_advanced_blockformats: "h1,h2,h3,h4,h5,p,pre",
            media_dimensions: false,
            media_poster: false,
            media_alt_source: false,
            media_embed: false,
            extended_valid_elements: "a[href|target=_blank|rel=nofollow|title]",
            imagemanager_insert_template: '<img src="{$url}" title="{$title}" alt="{$title}">',
            image_dimensions: false,
            relative_urls: false,
            remove_script_host: false
        });
    }
    // TINYMCE COMPLETO
    if ($('.j_word_full').length) {
        tinyMCE.init({
            selector: "textarea.j_word_full",
            language: 'pt_BR',
            menubar: true,
            theme: "modern",
            height: $(this).attr('rows') * 20,
            skin: 'lightgray',
            entity_encoding: "raw",
            theme_advanced_resizing: true,
            plugins: [
                "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                "save table contextmenu directionality emoticons template paste textcolor media"
            ],
            toolbar: "undo redo | insert | styleselect | table | forecolor | backcolor | pastetext | removeformat |  bold | italic | underline | strikethrough | bullist | numlist | alignleft | aligncenter | alignright |  link | unlink | drimage | media |  outdent | indent | fullscreen | preview | code | print",
            content_css: "css/tinyMCE.css",
            style_formats: [{
                title: 'Parágrafo',
                block: 'p'
            }, {
                title: 'Titulo 2',
                block: 'h2'
            }, {
                title: 'Titulo 3',
                block: 'h3'
            }, {
                title: 'Titulo 4',
                block: 'h4'
            }, {
                title: 'Titulo 5',
                block: 'h5'
            }],
            setup: function(editor) {
                editor.addButton('drimage', {
                    title: 'Enviar Imagem',
                    icon: 'image',
                    onclick: function() {
                        $('.j_imageupload').fadeIn(400, function() {
                            $('.j_imageupload_content').slideDown(100);
                        });
                    }
                });
            },
            link_title: true,
            target_list: false,
            theme_advanced_blockformats: "h1,h2,h3,h4,h5,p,pre",
            media_dimensions: false,
            media_poster: false,
            media_alt_source: false,
            media_embed: false,
            extended_valid_elements: "a[href|target=_blank|rel=nofollow|title]",
            imagemanager_insert_template: '<img src="{$url}" title="{$title}" alt="{$title}">',
            image_dimensions: false,
            relative_urls: false,
            remove_script_host: false
        });
    }
    // LIMPAR CAMPO DE INPUT
    $('.j_reset').val("");
    // VOLTAR O HISTORICO DO NAVEGADOR
    $('.j_back').click(function() {
        history.back();
    });
    // VERIFICA O STATUS DAS EMPRESA CADASTRADAS
    $('.j_statusRecursos').each(function() {
        var id = $(this).attr('rel');
        var acao = $(this).attr('tabindex');
        var dados = {
            action: acao,
            _id: id
        };
        $.ajax({
            url: "_cdn/ajax/functions.php",
            type: "POST",
            dataType: 'json',
            data: dados,
            success: function(resposta) {
                if (resposta.ret_status == 1) {
                    $('button[rel=' + resposta.ret_id + ']').fadeIn(function() {
                        $('.j_GetStatusRecursos[rel=' + resposta.ret_id + ']').html('<span class="text-danger">Offline</span>');
                        $('.j_statusRecursos[rel=' + resposta.ret_id + ']').html('<i class="fa fa-unlock"></i>');
                        $('.j_statusRecursos[rel=' + resposta.ret_id + ']').addClass('btn-warning');
                        $('.j_statusRecursos[rel=' + resposta.ret_id + ']').val(2);
                    });
                } else {
                    $('button[rel=' + resposta.ret_id + ']').fadeIn(function() {
                        $('.j_GetStatusRecursos[rel=' + resposta.ret_id + ']').html('<span class="text-success">Online</span>');
                        $('.j_statusRecursos[rel=' + resposta.ret_id + ']').html('<i class="fa fa-lock"></i>');
                        $('.j_statusRecursos[rel=' + resposta.ret_id + ']').addClass('btn-success');
                        $('.j_statusRecursos[rel=' + resposta.ret_id + ']').val(1);
                    });
                }
            }
        });
    });
    // VERIFICA E ALTERA O STATUS DE TODOS OS RECURSOS
    $('.j_statusRecursos').on('click', function() {
        var id = $(this).attr('rel');
        var status = $(this).val();
        var acao = $(this).attr('action');
        var dados = {
            action: acao,
            _id: id,
            _status: status
        };
        $.ajax({
            url: "_cdn/ajax/functions.php",
            type: "POST",
            dataType: 'json',
            data: dados,
            success: function(resposta) {
                if (resposta.error) {
                    swal("Ooops!", "Não foi possível alterar o status deste item.", "error");
                } else if (resposta.result == 0) {
                    swal("Ooops!", "Você não possuí permissão para alterar os status deste item.", "error");
                } else if (resposta.ret_status == 1) {
                    $('button[rel=' + resposta.ret_id + ']').fadeIn(function() {
                        $('.j_GetStatusRecursos[rel=' + resposta.ret_id + ']').html('<span class="text-danger">Offline</span>');
                        $('.j_statusRecursos[rel=' + resposta.ret_id + ']').html('<i class="fa fa-unlock"></i>');
                        $('.j_statusRecursos[rel=' + resposta.ret_id + ']').addClass('btn-warning');
                        $('.j_statusRecursos[rel=' + resposta.ret_id + ']').removeClass('btn-success');
                        $('.j_statusRecursos[rel=' + resposta.ret_id + ']').val(2);
                        swal("Tudo certo!", "Item alterado para Offline.", "success");
                    });
                } else {
                    $('button[rel=' + resposta.ret_id + ']').fadeIn(function() {
                        $('.j_GetStatusRecursos[rel=' + resposta.ret_id + ']').html('<span class="text-success">Online</span>');
                        $('.j_statusRecursos[rel=' + resposta.ret_id + ']').html('<i class="fa fa-lock"></i>');
                        $('.j_statusRecursos[rel=' + resposta.ret_id + ']').addClass('btn-success');
                        $('.j_statusRecursos[rel=' + resposta.ret_id + ']').removeClass('btn-warning');
                        $('.j_statusRecursos[rel=' + resposta.ret_id + ']').val(1);
                        swal("Tudo certo!", "Item alterado para Online.", "success");
                    });
                }
            }
        });
    });
    // FUNÇÕES DE AÇÃO GLOBAL
    $(".j_remove").click(function() {
        var id = $(this).attr('rel');
        var acao = $(this).attr('action');
        var dados = {
            action: acao,
            itemId: id
        };
        swal({
                title: "Deletar item?",
                text: "Deseja realmente deletar este item do sistema?",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-info",
                confirmButtonText: "Confirmar",
                closeOnConfirm: false
            },
            function() {
                $.ajax({
                    url: "_cdn/ajax/functions.php",
                    type: "POST",
                    dataType: 'json',
                    data: dados,
                    success: function(resposta) {
                        if (resposta.error) {
                            swal("Ooops!", "Não foi possível remover, tente atualizar a página.", "error");
                        } else if (resposta.result == 0) {
                            swal("Ooops!", "Você não possui permissão para executar esta operação.", "error");
                        } else if (resposta.result == 2) {
                            swal("Ooops!", "Não existe uma capa para remover.", "error");
                        } else if (resposta.result == 1) {
                            $('.j_item[id=' + id + ']').slideUp('fast');
                            swal("Ooops!", "O item foi removido do banco de dados, porém o arquivo relacionado não foi encontrado.", "info");
                        } else if (resposta.result == 3) {
                            swal("Ooops!", "Para deletar este item, remova todos os itens dentro deste bloco.", "info");
                        } else {
                            $('tr.child').slideUp('fast');
                            $('.j_item[id=' + id + ']').slideUp('fast');
                            swal("Tudo certo!", "O item foi deletado com sucesso.", "success");
                        }
                    }
                });
            });
    });
    // REMOVE USUÁRIOS
    $('.j_removeUser').click(function() {
        var id = $(this).attr('rel');
        var emp = $(this).attr('title');
        var dados = {
            action: 'removeUser',
            user_id: id,
            user_emp: emp
        };
        swal({
                title: "Deletar usuário?",
                text: "Deseja realmente deletar este usuário do sistema?",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-info",
                confirmButtonText: "Confirmar",
                closeOnConfirm: false
            },
            function() {
                $.ajax({
                    url: "_cdn/ajax/functions.php",
                    type: "POST",
                    dataType: 'json',
                    data: dados,
                    success: function(resposta) {
                        if (resposta.error) {
                            swal("Ooops!", "Não foi possível remover o usuário tente atualizar a página.", "error");
                        } else if (resposta.result == 0) {
                            swal("Ooops!", "Você não pode deletar o seu próprio usuário", "info");
                        } else if (resposta.result == 1) {
                            swal("Ooops!", "Você não pode deletar o último usuário administrador desta empresa.", "info");
                        } else if (resposta.result == 2) {
                            swal("Ooops!", "Você não tem permissão para esta ação.", "error");
                        } else {
                            $('.j_user[rel=' + id + ']').slideUp();
                            swal("Tudo certo!", "Usuário removido com sucesso.", "success");
                        }
                    }
                });
            });
    });
    // REMOVE COLABORADORES DA AGÊNCIA
    $('.j_removeFunc').click(function() {
        var id = $(this).attr('rel');
        var emp = $(this).attr('title');
        var dados = {
            action: 'removeFunc',
            user_id: id,
            user_emp: emp
        };
        swal({
                title: "Deletar usuário?",
                text: "Deseja realmente deletar este usuário do sistema?",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-info",
                confirmButtonText: "Confirmar",
                closeOnConfirm: false
            },
            function() {
                $.ajax({
                    url: "_cdn/ajax/functions.php",
                    type: "POST",
                    dataType: 'json',
                    data: dados,
                    success: function(resposta) {
                        if (resposta.error) {
                            swal("Ooops!", "Não foi possível remover o usuário tente atualizar a página.", "error");
                        } else if (resposta.result == 0) {
                            swal("Ooops!", "Você não pode deletar o seu próprio usuário", "info");
                        } else if (resposta.result == 1) {
                            swal("Ooops!", "Você não pode deletar o último usuário administrador desta empresa.", "info");
                        } else if (resposta.result == 2) {
                            swal("Ooops!", "Você não tem permissão para esta ação.", "error");
                        } else {
                            $('.j_user[rel=' + id + ']').slideUp();
                            swal("Tudo certo!", "Usuário removido com sucesso.", "success");
                        }
                    }
                });
            });
    });
    // ACESSA A EMPRESA
    $('.j_view').click(function() {
        var id = $(this).attr('id');
        var nome = $(this).attr('alt');
        var dados = {
            action: 'setEmpresa',
            empresa_id: id
        };
        swal({
                title: "Acessar empresa?",
                text: "Ao confirmar você estará navegando como se fosse um usuário da empresa " + nome + ".",
                type: "info",
                showCancelButton: true,
                confirmButtonClass: "btn-info",
                confirmButtonText: "Confirmar",
                closeOnConfirm: false
            },
            function() {
                $.post('_cdn/ajax/functions.php', dados, function(resposta) {
                    if (resposta.error) {
                        swal("Oooops!", "Não foi possivel acessar a empresa, tente atualizar a pagina.", "error");
                        $('.j_getEmp').html(nome);
                        //location.href = 'painel.php?exe=CMS/index';
                    } else {
                        swal("Tudo certo!", "Empresa alterada.", "success");
                        $('.j_getEmp').html(nome);
                        location.href = "painel.php";
                    }
                }, 'json');
            });
    });
    // VERIFICA O STATUS DAS EMPRESA CADASTRADAS
    $('.j_status').each(function() {
        var id = $(this).attr('rel');
        var dados = {
            action: 'verstatus',
            empresa_id: id
        };
        $.ajax({
            url: "_cdn/ajax/functions.php",
            type: "POST",
            dataType: 'json',
            data: dados,
            success: function(resposta) {
                if (resposta.status == 1) {
                    $('button[rel=' + resposta.id + ']').fadeIn(function() {
                        $('.j_getStatus[rel=' + resposta.id + ']').html('Ativada');
                        $('.j_status[rel=' + resposta.id + ']').html('<i class="fa fa-lock"></i>');
                        $('.j_status[rel=' + resposta.id + ']').addClass('btn-danger');
                        $('.j_status[rel=' + resposta.id + ']').val(2);
                    });
                } else {
                    $('button[rel=' + resposta.id + ']').fadeIn(function() {
                        $('.j_getStatus[rel=' + resposta.id + ']').html('Desativada');
                        $('.j_status[rel=' + resposta.id + ']').html('<i class="fa fa-unlock"></i>');
                        $('.j_status[rel=' + resposta.id + ']').addClass('btn-success');
                        $('.j_status[rel=' + resposta.id + ']').val(1);
                    });
                }
            }
        });
    });
    // FUNÇÃO DE AÇÃO PARA ATIVAR E DESATIVAR EMPRESA
    $('.j_status').on('click', function() {
        var id = $(this).attr('rel');
        var status = $(this).val();
        var dados = {
            action: 'UpStatus',
            empresa_id: id,
            empresa_status: status
        };
        $.ajax({
            url: "_cdn/ajax/functions.php",
            type: "POST",
            dataType: 'json',
            data: dados,
            success: function(resposta) {
                if (resposta.error) {
                    swal("Ooops!", "Você não tem permição para executar esta ação.", "error");
                } else if (resposta.error) {
                    swal("Ooops!", "O sistema se comportou de maneira inesperada. Revise os dados e tente novamente.", "error");
                } else if (resposta.status == 1) {
                    $('button[rel=' + resposta.id + ']').fadeIn(function() {
                        $('.j_getStatus[rel=' + resposta.id + ']').html('Ativada');
                        $('.j_status[rel=' + resposta.id + ']').html('<i class="fa fa-lock"></i>');
                        $('.j_status[rel=' + resposta.id + ']').addClass('btn-danger');
                        $('.j_status[rel=' + resposta.id + ']').removeClass('btn-success');
                        $('.j_status[rel=' + resposta.id + ']').val(2);
                        swal("Tudo certo!", "Empresa ativada com sucesso.", "success");
                    });
                } else {
                    $('button[rel=' + resposta.id + ']').fadeIn(function() {
                        $('.j_getStatus[rel=' + resposta.id + ']').html('Desativada');
                        $('.j_status[rel=' + resposta.id + ']').html('<i class="fa fa-unlock"></i>');
                        $('.j_status[rel=' + resposta.id + ']').addClass('btn-success');
                        $('.j_status[rel=' + resposta.id + ']').removeClass('btn-danger');
                        $('.j_status[rel=' + resposta.id + ']').val(1);
                        swal("Tudo certo!", "Empresa desativada com sucesso.", "success");
                    });
                }
            }
        });
    });
    // FUNÇÃO DE AÇÃO PARA ATIVAR E DESATIVAR EMPRESA E VERIFICAR SE EXISTE NAS DUAS TABELAS
    $('.j_AlteraStatus').on('click', function() {
        var id = $(this).attr('rel');
        var status = $(this).val();
        var dados = {
            action: 'UpStatus',
            empresa_id: id,
            empresa_status: status
        };
        $.ajax({
            url: "_cdn/ajax/functions.php",
            type: "POST",
            dataType: 'json',
            data: dados,
            success: function(resposta) {
                if (resposta.error) {
                    swal("Ooops!", "Você não tem permição para executar esta ação.", "error");
                } else if (resposta.result == 0) {
                    swal("Ooops!", "O sistema se comportou de maneira inesperada. Revise os dados e tente novamente.", "error");
                } else if (resposta.status == 1) {
                    $('.j_AlteraStatus[rel=' + resposta.id + ']').val(2).removeClass('btn-warning').addClass('btn-success').html('<i class="fa fa-unlock"></i>');
                    swal("Tudo certo!", "Empresa desbloqueada com sucesso.", "success");
                } else {
                    $('.j_AlteraStatus[rel=' + resposta.id + ']').val(1).removeClass('btn-success').addClass('btn-warning').html('<i class="fa fa-lock"></i>');
                    swal("Tudo certo!", "Empresa bloqueada com sucesso.", "success");
                }
            }
        });
    });
    // FUNÇÃO DE AÇÃO PARA ATIVAR E DESATIVAR UM ITEM
    $('.j_GlobalStatus').on('click', function() {
        var dados = {
            action: $(this).attr('data-action'),
            itemId: $(this).attr('data-id'),
            ativo: $(this).val()
        };
        $.ajax({
            url: "_cdn/ajax/functions.php",
            type: "POST",
            dataType: 'json',
            data: dados,
            success: function(resposta) {
                if (resposta.error) {
                    swal("Ooops!", "Você não tem permição para executar esta ação.", "error");
                } else if (resposta.result == 0) {
                    swal("Ooops!", "O sistema se comportou de maneira inesperada. Revise os dados e tente novamente.", "error");
                } else if (resposta.status == 1) {
                    $('.j_GlobalStatus[data-id=' + resposta.id + ']').val(2).removeClass('btn-warning').addClass('btn-success').html('<i class="fa fa-unlock"></i>');
                    swal("Tudo certo!", "Item ativado com sucesso.", "success");
                } else {
                    $('.j_GlobalStatus[data-id=' + resposta.id + ']').val(1).removeClass('btn-success').addClass('btn-warning').html('<i class="fa fa-lock"></i>');
                    swal("Tudo certo!", "Item desativado com sucesso.", "success");
                }
            }
        });
    });
    // NAVEGA NAS LISTA DE CIDADES E ESTADOS
    $('.j_loadstate').change(function() {
        var uf = $('.j_loadstate');
        var city = $('.j_loadcity');
        var patch = ($('#j_ajaxident').length ? $('#j_ajaxident').attr('class') + '/city.php' : '_cdn/ajax/city.php');
        city.attr('disabled', 'true');
        uf.attr('disabled', 'true');
        city.html('<option value=""> Carregando cidades... </option>');
        $.post(patch, {
            estado: $(this).val()
        }, function(cityes) {
            city.html(cityes).removeAttr('disabled');
            uf.removeAttr('disabled');
        });
    });
    // FUNÇÃO QUE FECHA AS MENSAGENS DO TRIGGER ERRROR APPÓS 5s.
    $('.j_close').on('click', function() {
        $(this).slideDown(400);
        setTimeout(function() {
            $('.j_close').slideUp(400);
        }, 8000);
    });
    // GATILHO QUE DISPARA A FUNÇÃO AO CARREGAR A PAGINA.
    $('.j_close').trigger('click');
    // PERSONALIZAÇÃO DO DATAPICKER user_nasc
    $('#j_nasc').daterangepicker({
        "singleDatePicker": true,
        "showDropdowns": true,
        "locale": {
            "format": "DD/MM/YYYY",
            "separator": " - ",
            "applyLabel": "Apply",
            "cancelLabel": "Cancel",
            "fromLabel": "De",
            "toLabel": "Até",
            "customRangeLabel": "Custom",
            "daysOfWeek": [
                "Do",
                "Se",
                "Te",
                "Qu",
                "Qu",
                "Se",
                "Sa"
            ],
            "monthNames": [
                "Janeiro",
                "Fevereiro",
                "Março",
                "Abril",
                "Maio",
                "Junho",
                "Julho",
                "Agosto",
                "Setembro",
                "Outubro",
                "Novembro",
                "Dezembro"
            ]
        },
        "opens": "left",
        "drops": "up"
    });

    $('#j_date').daterangepicker({
        "singleDatePicker": true,
        "showDropdowns": true,
        "timePicker24Hour": true,
        "locale": {
            "format": "YYYY/MM/DD",
            "separator": " - ",
            "applyLabel": "Apply",
            "cancelLabel": "Cancel",
            "fromLabel": "De",
            "toLabel": "Até",
            "customRangeLabel": "Custom",
            "daysOfWeek": [
                "Do",
                "Se",
                "Te",
                "Qu",
                "Qu",
                "Se",
                "Sa"
            ],
            "monthNames": [
                "Janeiro",
                "Fevereiro",
                "Março",
                "Abril",
                "Maio",
                "Junho",
                "Julho",
                "Agosto",
                "Setembro",
                "Outubro",
                "Novembro",
                "Dezembro"
            ]
        },
        "opens": "left",
        "drops": "up"
    });
});