<?php
//LISTAGEM DE MODULOS E SEUS ITENS PARA OS MENUS
/**
* <b>Como funciona</b>
* Como alguns modulos podem deixar o menu muito grande, pode ser necessário comentar algumas
* consultas para não ficar muito extenso no menu.
* Você pode ter muitas postagens de blogs, então não é interessante exibir os itens do blog no menu
* porém serão exibidas as categorias do blogs, a mesma dinâmica serve para outros modulos.
* ###TODAS AS LISTAGENS DE CADA MODULO DE MENU ESTÃO DISPONÍVEIS ABAIXO###
*/
###PRODUTOS####
$Read->ExeRead(TB_PRODUTO, "WHERE user_empresa = :emp AND cat_parent = :cat ORDER BY prod_title ASC", "emp=" . EMPRESA_CLIENTE . "&cat={$itemCat}");
if ($Read->getResult()):
  foreach ($Read->getResult() as $prodItem):
    ?>
    <option value="<?= trim(Check::CatByParent($prodItem['cat_parent'], EMPRESA_CLIENTE) . $prodItem['prod_name'], '/'); ?>"
      <?php
      if (trim(Check::CatByParent($prodItem['cat_parent'], EMPRESA_CLIENTE) . $prodItem['prod_name'], '/') == $sl_url || isset($post['sl_url'])):
        echo 'selected="selected"';
    endif;
  ?> style="color: #bbb; padding: 4px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&RightArrow;<?= $prodItem['prod_title'] ?></option>
  <?php
endforeach;
endif;
###BLOG###
$Read->ExeRead(TB_BLOG, "WHERE user_empresa = :emp AND cat_parent = :cat ORDER BY blog_title ASC", "emp=" . EMPRESA_CLIENTE . "&cat={$itemCat}");
if ($Read->getResult()):
  foreach ($Read->getResult() as $blogItem):
    ?>
    <option value="<?= trim(Check::CatByParent($blogItem['cat_parent'], EMPRESA_CLIENTE) . $blogItem['blog_name'], '/'); ?>"
      <?php
      if (trim(Check::CatByParent($blogItem['cat_parent'], EMPRESA_CLIENTE) . $blogItem['blog_name'], '/') == $sl_url || isset($post['sl_url'])):
        echo 'selected="selected"';
    endif;
  ?> style="color: #bbb; padding: 4px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&RightArrow;<?= $blogItem['blog_title'] ?></option>
  <?php
endforeach;
endif;
###QUEM SOMOS###
$Read->ExeRead(TB_QUEMSOMOS, "WHERE user_empresa = :emp AND cat_parent = :cat ORDER BY quem_title ASC", "emp=" . EMPRESA_CLIENTE . "&cat={$itemCat}");
if ($Read->getResult()):
  foreach ($Read->getResult() as $quemItem):
    ?>
    <option value="<?= trim(Check::CatByParent($quemItem['cat_parent'], EMPRESA_CLIENTE) . $quemItem['quem_name'], '/'); ?>"
      <?php
      if (trim(Check::CatByParent($quemItem['cat_parent'], EMPRESA_CLIENTE) . $quemItem['quem_name'], '/') == $sl_url || isset($post['sl_url'])):
        echo 'selected="selected"';
    endif;
  ?> style="color: #bbb; padding: 4px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&RightArrow;<?= $quemItem['quem_title'] ?></option>
  <?php
endforeach;
endif;
###NOVIDADES/NOTÍCIAS####
$Read->ExeRead(TB_NOTICIA, "WHERE user_empresa = :emp AND cat_parent = :cat ORDER BY noti_title ASC", "emp=" . EMPRESA_CLIENTE . "&cat={$itemCat}");
if ($Read->getResult()):
  foreach ($Read->getResult() as $notiItem):
    ?>
    <option value="<?= trim(Check::CatByParent($notiItem['cat_parent'], EMPRESA_CLIENTE) . $notiItem['noti_name'], '/'); ?>"
      <?php
      if (trim(Check::CatByParent($notiItem['cat_parent'], EMPRESA_CLIENTE) . $notiItem['noti_name'], '/') == $sl_url || isset($post['sl_url'])):
        echo 'selected="selected"';
    endif;
  ?> style="color: #bbb; padding: 4px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&RightArrow;<?= $notiItem['noti_title'] ?></option>
  <?php
endforeach;
endif;
###SERVIÇOS####
$Read->ExeRead(TB_SERVICO, "WHERE user_empresa = :emp AND cat_parent = :cat ORDER BY serv_title ASC", "emp=" . EMPRESA_CLIENTE . "&cat={$itemCat}");
if ($Read->getResult()):
  foreach ($Read->getResult() as $servItem):
    ?>
    <option value="<?= trim(Check::CatByParent($servItem['cat_parent'], EMPRESA_CLIENTE) . $servItem['serv_name'], '/'); ?>"
      <?php
      if (trim(Check::CatByParent($servItem['cat_parent'], EMPRESA_CLIENTE) . $servItem['serv_name'], '/') == $sl_url || isset($post['sl_url'])):
        echo 'selected="selected"';
    endif;
  ?> style="color: #bbb; padding: 4px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&RightArrow;<?= $servItem['serv_title'] ?></option>
  <?php
endforeach;
endif;