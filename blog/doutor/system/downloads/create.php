<!-- page content -->
<div class="right_col" role="main">  
    <div class="row">
        <?php
        $lv = 1;
        if (!APP_USERS || empty($userlogin) || $user_level < $lv):
            die(WSErro("Desculpe, você não tem permissão para acessar esta área. <a href='javascript:history.back();' class='btn primary'>Voltar</a>", WS_ERROR, null, "Doutores da Web"));
        endif;
        ?>	
    </div>
    <form data-parsley-validate class="form-horizontal form-label-left" method="post" enctype="multipart/form-data">
        <div class="page-title">
            <div class="title col-md-12 col-sm-6 col-xs-12">
                <h3><i class="fa fa-download"></i> Cadastro de downloads</h3>
            </div>
            <div class="clearfix"></div>
            <br/>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="pull-right">
                        <button type="submit" name="Cadastra" class="btn btn-primary"><i class="fa fa-save"></i></button>
                        <button type="button" class="btn btn-default" onclick="location = 'painel.php?exe=downloads/index'"><i class="fa fa-home"></i></button>
                    </div>
                    <div class="clearfix"></div>
                    <br>
                    <?php
                    $get = filter_input(INPUT_GET, 'get', FILTER_VALIDATE_BOOLEAN);
                    $post = filter_input_array(INPUT_POST, FILTER_DEFAULT);
                    if (isset($post) && isset($post['Cadastra'])):
                        unset($post['Cadastra']);
                        $post['user_empresa'] = $_SESSION['userlogin']['user_empresa'];
                        $post['dow_file'] = ( $_FILES['dow_file']['tmp_name'] ? $_FILES['dow_file'] : null );
                      
                        $cadastra = new Downloads();
                        $cadastra->ExeCreate($post);

                        if (!$cadastra->getResult()):
                            $erro = $cadastra->getError();
                            WSErro($erro[0], $erro[1], null, $erro[2]);
                        else:
                            $_SESSION['Error'] = $cadastra->getError();
                            header('Location: painel.php?exe=downloads/create&get=true');
                        endif;

                    endif;

                    if (isset($get) && $get == true && !isset($post) && isset($_SESSION['Error'])):
                        //COLOCAR ALERTA PERSONALIZADOS
                        WSErro($_SESSION['Error'][0], $_SESSION['Error'][1], null, $_SESSION['Error'][2]);
                        unset($_SESSION['Error']);
                    endif;
                    ?>                    
                    <div class="x_panel">
                        <div class="x_content">
                            <div class="x_title">
                                <h2>Preencha os campos abaixo para cadastrar um novo download no site</h2>                           
                                <div class="clearfix"></div>                            
                            </div>

                            <div class="clearfix"></div>
                            <br>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="dow_status">Publicar agora?</label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <div class="radio">
                                        <label>
                                            <input name="dow_status" id="optionsRadios1" type="radio" value="2" <?php
                                            if (isset($post['dow_status']) && $post['dow_status'] == 2): echo 'checked="true"';
                                            endif;
                                            ?>> Sim
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input name="dow_status" id="optionsRadios2" type="radio" value="1" <?php
                                            if (isset($post['dow_status']) && $post['dow_status'] == 1): echo 'checked="true"';
                                            endif;
                                            ?>> Não
                                        </label>
                                    </div>                 
                                </div>                                 
                            </div> 
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="cat_parent">Sessão ou categoria do item <span class="required">*</span></label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <!--inclusão das seleções de categorias-->
                                    <?php include("inc/categorias.inc.php"); ?>
                                    <!-- /inclusão das seleções de categorias-->
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="dow_file">Arquivo <span class="text-info">(.pdf, .doc, .xlsx, .csv)</span> <span class="required">*</span>
                                </label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <input type="file" id="app_file" name="dow_file" required="required">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="dow_title">Nome <span class="required">*</span>
                                </label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <input type="text" id="dow_title" name="dow_title" required="required" class="form-control col-md-7 col-xs-12" value="<?php
                                    if (isset($post['dow_title'])): echo $post['dow_title'];
                                    endif;
                                    ?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="dow_description">Breve descrição <span class="required">*</span>
                                </label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <textarea name="dow_description" required="required" class="form-control col-md-7 col-xs-12"><?php
                                        if (isset($post['dow_description'])): echo $post['dow_description'];
                                        endif;
                                        ?></textarea>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="pull-right">
            <button type="submit" name="Cadastra" class="btn btn-primary"><i class="fa fa-save"></i></button>
            <button type="button" class="btn btn-default" onclick="location = 'painel.php?exe=downloads/index'"><i class="fa fa-home"></i></button>
        </div>
        <div class="clearfix"></div>
        <br>
    </form>
    <div class="clearfix"></div>
</div>
