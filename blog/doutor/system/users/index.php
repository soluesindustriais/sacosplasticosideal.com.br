<!-- page content -->
<div class="right_col" role="main">   
  <div class="row">
    <?php
    $lv = 3;
    if (!APP_USERS || empty($userlogin) || $user_level < $lv):
      die(WSErro("Desculpe, você não tem permissão para acessar esta área. <a href='javascript:history.back();' class='btn primary'>Voltar</a>", WS_ERROR, null, "Doutores da Web"));
    endif;
    ?>	
  </div>
  <div class="page-title">
    <div class="title col-md-12 col-sm-6 col-xs-12">
      <h3><i class="fa fa-user"></i> Lista de usuários cadastrados</h3>
    </div>
    <div class="clearfix"></div>
    <br/>
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">  
        <div class="x_panel">
          <div class="x_content">
            <div class="x_title">
              <h2>Utilize o campo de pesquisa para filtrar sua busca.<small>Você também pode acessar, editar e remover usuários.</small></h2>                           
              <div class="clearfix"></div>                            
            </div>
            <?php
            $get = filter_input(INPUT_GET, 'get', FILTER_VALIDATE_BOOLEAN);
            if (isset($get) && $get == true && isset($_SESSION['Error'])):
              //COLOCAR ALERTA PERSONALIZADOS
              WSErro($_SESSION['Error'][0], $_SESSION['Error'][1], null, $_SESSION['Error'][2]);
              unset($_SESSION['Error']);
            endif;
            ?>                        
            <div class="table-responsive">
              <table id="datatable" class="table table-striped table-hover">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Nome</th>
                    <th>Cargo</th>
                    <th>Acesso</th>
                    <th>Ações</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $ReadUsuarios = new Read;
                  $ReadUsuarios->ExeRead(TB_USERS, "WHERE user_empresa = :id ORDER BY user_id DESC", "id={$_SESSION['userlogin']['user_empresa']}");
                  if (!$ReadUsuarios->getResult()):
                    WSErro("Nenhum usuário foi encontrado.", WS_INFOR, null, "MPI Technology");
                  else:
                    foreach ($ReadUsuarios->getResult() as $us):
                      extract($us);
                      ?>
                      <tr class="j_user" rel="<?= $user_id; ?>">
                        <th scope="row"><?= $user_id; ?></th>
                        <td><?= $user_name; ?> <?= $user_lastname; ?></td>
                        <td><?= $user_cargo; ?></td>
                        <td><?= GetNiveis($user_level); ?></td>
                        <td class="text-center" style="width: 100px;">
                          <div class="btn-group btn-group-xs">
                            <button type="button" class="btn btn-primary" onclick="location = 'painel.php?exe=users/profile&id=<?= $user_id; ?>'"><i class="fa fa-eye"></i></button>
                            <button type="button" class="btn btn-success" onclick="location = 'painel.php?exe=users/update&id=<?= $user_id; ?>'"><i class="fa fa-pencil"></i></button>
                            <?php if($user_level > 1): ?>
                              <button type="button" class="btn btn-danger j_removeUser" rel="<?= $user_id; ?>" title="<?= $user_empresa; ?>"><i class="fa fa-trash"></i></button>
                            <?php endif; ?>
                          </div>
                        </td>
                      </tr>
                      <?php
                    endforeach;
                  endif;
                  ?>
                </tbody>
              </table>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
  <div class="clearfix"></div>
</div>