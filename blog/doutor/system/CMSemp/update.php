<!-- page content -->
<div class="right_col" role="main">
  <div class="row">
    <?php
    $lv = 5;
    if (!APP_USERS || empty($userlogin) || $user_level < $lv):
      die(WSErro("Desculpe, você não tem permissão para acessar esta área. <a href='javascript:history.back();' class='btn primary'>Voltar</a>", WS_ERROR, null, "Doutores da Web"));
    endif;
    ?>
  </div>
  <form class="form-horizontal form-label-left input_mask" enctype="multipart/form-data" method="post" data-parsley-validate>
    <div class="page-title">
      <div class="title col-md-12 col-sm-6 col-xs-12">
        <h3><i class="fa fa-briefcase"></i> Atualizar empresa</h3>
      </div>
      <div class="clearfix"></div>
      <br/>
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="pull-right">
            <button type="submit" name="UpdateEmp" class="btn btn-primary"><i class="fa fa-save"></i></button>
            <button type="button" class="btn btn-default" onclick="location = 'painel.php?exe=CMSemp/index'"><i class="fa fa-home"></i></button>
          </div>
          <div class="clearfix"></div>
          <br/>
          <?php
          $get = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);
          $post = filter_input_array(INPUT_POST, FILTER_DEFAULT);
          if (isset($post) && isset($post['UpdateEmp'])):
            unset($post['UpdateEmp']);
          $post['empresa_capa'] = ( isset($_FILES['empresa_capa']['tmp_name']) ? $_FILES['empresa_capa'] : null );
          $update = new AdminCMS;
          $update->ExeUpdate($get, $post);
          $erro = $update->getError();
          if (!$update->getResult()):
            WSErro($erro[0], $erro[1], null, $erro[2]);
          else:
            WSErro($erro[0], $erro[1], null, $erro[2]);
          endif;
        endif;
        $ReadEmps = new Read;
        $ReadEmps->ExeRead(TB_EMP, "WHERE empresa_id = :id", "id={$get}");
        if (!$ReadEmps->getResult() && isset($get)):
          WSErro("A empresa não foi encontrada.", WS_ALERT, null, "MPI Technology");
      else:
        $extrair = $ReadEmps->getResult();
        extract($extrair[0]);
        ?>
        <div class="row">
          <div class="col-md-6 col-xs-12">
            <div class="x_panel">
              <div class="x_title">
                <h2>Dados da empresa</h2>
                <div class="clearfix"></div>
                <p>Após concluir a edição, os administradores serão notificado por e-mail com as novas informações da empresa.</p>
                <div class="clearfix"></div>
              </div>
              <br/>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="empresa_capa">Logo da empresa (Ao enviar, o antigo será substituído) <span class="required">*</span></label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                <?= Check::Image("../doutor/uploads/" . $empresa_capa, $empresa_name, 'img-responsive', 100); ?>
                <div class="clearfix"></div>
                <br/>
                  <input name="empresa_capa" id="app_cover" type="file" class="form-control">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="empresa_name">Nome fantasia <span class="required">*</span></label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <input name="empresa_name" id="empresa_name" required="required" type="text" class="form-control" value="<?php
                  if (isset($post['empresa_name'])): echo $post['empresa_name'];
                  else: echo $empresa_name;
                  endif;
                ?>" placeholder="Digite o nome fantasia da empresa">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="empresa_razaosocial">Razão social</label>
              <div class="col-md-9 col-sm-9 col-xs-12">
                <input name="empresa_razaosocial" id="empresa_razaosocial" required="required" type="text" class="form-control" value="<?php
                if (isset($post['empresa_razaosocial'])): echo $post['empresa_razaosocial'];
                else: echo $empresa_razaosocial;
                endif;
              ?>" placeholder="Digite a razão social da empresa">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="empresa_cnpj">CNPJ</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
              <input name="empresa_cnpj" id="empresa_cnpj" data-mask="99.999.999/9999-99" required="required" type="text" class="form-control" value="<?php
              if (isset($post['empresa_cnpj'])): echo $post['empresa_cnpj'];
              else: echo $empresa_cnpj;
              endif;
            ?>" placeholder="Digite o CNPJ da empresa">
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="empresa_ie">Inscrição estadual</label>
          <div class="col-md-9 col-sm-9 col-xs-12">
            <input name="empresa_ie" id="empresa_ie" required="required" type="text" class="form-control" value="<?php
            if (isset($post['empresa_ie'])): echo $post['empresa_ie'];
            else: echo $empresa_ie;
            endif;
          ?>" placeholder="Digite a inscrição estadual da empresa">
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="empresa_ramo">Ramo</label>
        <div class="col-md-9 col-sm-9 col-xs-12">
          <input name="empresa_ramo" id="empresa_ramo" required="required" type="text" class="form-control" value="<?php
          if (isset($post['empresa_ramo'])): echo $post['empresa_ramo'];
          else: echo $empresa_ramo;
          endif;
        ?>" placeholder="Digite o ramo em que a empresa atua">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="empresa_sobre">Sobre a empresa</label>
      <div class="col-md-9 col-sm-9 col-xs-12">
        <textarea name="empresa_sobre" id="empresa_sobre" placeholder="Conte um pouco sobre a história da empresa" rows="3" class="form-control"><?php
        if (isset($post['empresa_sobre'])): echo $post['empresa_sobre'];
          echo $empresa_sobre;
        endif;
      ?></textarea>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="uf">Estado</label>
    <div class="col-md-9 col-sm-9 col-xs-12">
      <select id="uf" name="empresa_uf" class="form-control j_loadstate" required="required">
        <option value="" selected="true">--Selecione o estado--</option>
        <?php
        $readState = new Read;
        $readState->ExeRead(TB_UF, "ORDER BY estado_nome ASC");
        foreach ($readState->getResult() as $estado):
          extract($estado);
          echo "<option value=\"{$estado_id}\" ";
          if (isset($post['empresa_uf']) && $post['empresa_uf'] == $estado_id): echo 'selected';
            elseif ($estado_id == $empresa_uf): echo 'selected';
            endif;
            echo "> {$estado_uf} / {$estado_nome} </option>";
          endforeach;
          ?>
        </select>
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="cidade">Cidade</label>
      <div class="col-md-9 col-sm-9 col-xs-12">
        <select id="cidade" name="empresa_cidade" class="form-control j_loadcity" required="required">
          <?php if (!isset($post['empresa_cidade']) && !isset($empresa_cidade)): ?>
          <option value="" selected disabled> Selecione antes um estado </option>
          <?php
        else:
          $empresa_uf = ( isset($empresa_uf) ? $empresa_uf : $post['empresa_uf'] );
          $City = new Read;
          $City->ExeRead(TB_CID, "WHERE estado_id = :uf ORDER BY cidade_nome ASC", "uf={$empresa_uf}");
          if ($City->getRowCount()):
            foreach ($City->getResult() as $cidade):
              extract($cidade);
              echo "<option value=\"{$cidade_id}\" ";
              if (isset($post['empresa_cidade']) && $post['empresa_cidade'] == $cidade_id):
                echo "selected";
                elseif ($cidade_id == $empresa_cidade): echo "selected";
                endif;
                echo "> {$cidade_nome} </option>";
              endforeach;
            endif;
          endif;
          ?>
        </select>
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="empresa_endereco">Endereço</label>
      <div class="col-md-9 col-sm-9 col-xs-12">
        <input name="empresa_endereco" id="empresa_endereco" required="required" type="text" class="form-control" value="<?php
        if (isset($post['empresa_endereco'])): echo $post['empresa_endereco'];
        else: echo $empresa_endereco;
        endif;
      ?>" placeholder="Digite o endereço da empresa">
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="empresa_cep">CEP</label>
    <div class="col-md-9 col-sm-9 col-xs-12">
      <input name="empresa_cep" id="empresa_cep" data-mask="99999-999" required="required" type="text" class="form-control" value="<?php
      if (isset($post['empresa_cep'])): echo $post['empresa_cep'];
      else: echo $empresa_cep;
      endif;
    ?>" placeholder="Digite o CEP da empresa">
  </div>
</div>
<div class="form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="empresa_fone">Telefone principal</label>
  <div class="col-md-9 col-sm-9 col-xs-12">
    <input name="empresa_fone" id="empresa_fone" data-mask="(99) 9999-9999" required="required" type="text" class="form-control" value="<?php
    if (isset($post['empresa_fone'])): echo $post['empresa_fone'];
    else: echo $empresa_fone;
    endif;
  ?>" placeholder="Digite o telefone principal da empresa">
</div>
</div>
<div class="form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="empresa_site">Site <span class="text-info">(opcional)</span></label>
  <div class="col-md-9 col-sm-9 col-xs-12">
    <input name="empresa_site" id="empresa_site" data-rule-url="false" data-rule-required="false" type="text" class="form-control" value="<?php
    if (isset($post['empresa_site'])): echo $post['empresa_site'];
    else: echo $empresa_site;
    endif;
  ?>" placeholder="Digite o endereço do site da empresa http://www.site.com.br">
</div>
</div>
</div>
</div>
<div class="col-md-6 col-xs-12">
  <div class="x_panel">
    <div class="x_title">
      <h2>Dados do plano</h2>
      <div class="clearfix"></div>
    </div>
    <br />
    <div class="form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="empresa_disco">Espaço em disco <span class="required">*</span></label>
      <div class="col-md-9 col-sm-9 col-xs-12">
        <input name="empresa_disco" id="empresa_disco" data-mask="999999" required="required" type="text" class="form-control" value="<?php
        if (isset($post['empresa_disco'])): echo $post['empresa_disco'];
        else: echo $empresa_disco;
        endif;
      ?>" placeholder="Digite o espaço em disco do plano (10000 para 10MB)">
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="empresa_users">Número de usuários</label>
    <div class="col-md-9 col-sm-9 col-xs-12">
      <input name="empresa_users" id="empresa_users" data-mask="999999" required="required" type="text" class="form-control" value="<?php
      if (isset($post['empresa_users'])): echo $post['empresa_users'];
      else: echo $empresa_users;
      endif;
    ?>" placeholder="Digite a quantidade de usuários do plano">
  </div>
</div>
</div>
</div>
</div>
<?php endif; ?>
</div>
</div>
</div>
<div class="pull-right">
  <button type="submit" name="UpdateEmp" class="btn btn-primary"><i class="fa fa-save"></i></button>
  <button type="button" class="btn btn-default" onclick="location = 'painel.php?exe=CMSemp/index'"><i class="fa fa-home"></i></button>
</div>
<div class="clearfix"></div>
<br/>
</form>
</div>