<?php
ob_start();
session_start();
require('_app/Config.inc.php');
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="shortcut icon" href="<?= RAIZ; ?>/imagens/favicon.png">
  <title><?= SITENAME; ?></title>
  <!-- jQuery -->
  <script src="<?= BASE; ?>/vendors/jquery/dist/jquery.min.js"></script>
  <!-- Custom JS-->
  <script async src="<?= BASE; ?>/_cdn/scripts.js"></script>
  
  <!-- Bootstrap -->
  <link href="<?= BASE; ?>/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
  <!-- Font Awesome -->
  <link href="<?= BASE; ?>/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <!-- Custom Theme Style -->
  <link href="<?= BASE; ?>/build/css/custom.min.css" rel="stylesheet">
</head>
<body class="login">
  <div class="loading"></div>
  <div>
    <div class="login_wrapper">
      <section class="login_content">
        <?php
        $get = filter_input(INPUT_GET, 'exe', FILTER_DEFAULT);
        $dataLogin = filter_input_array(INPUT_POST, FILTER_DEFAULT);
        if (!empty($get)):
          if ($get == 'restrito'):
            WSErro('Digite suas credenciais de acesso.', WS_ALERT, null, "MPI Technology");
          elseif ($get == 'logoff'):
            WSErro('Sua sessão foi finalizada.', WS_ACCEPT, null, 'MPI Technology');
          elseif ($get == 'recsuccess'):
            WSErro('Sua senha foi alterada, acesse sua conta.', WS_ACCEPT, null, 'MPI Technology');
          else:
            $readRecUser = new Read;
            $readRecUser->ExeRead(TB_USERS, "WHERE user_key = :key", "key={$get}");
            if (!$readRecUser->getResult()):
              WSErro('Token inválido.', WS_ALERT, null, "MPI Technology");
            else:
              $email = $readRecUser->getResult();
              $email = $email[0]['user_email'];
            endif;
          endif;
        endif;
        $login = new Login(1);
        if ($login->CheckLogin()):
          $login->LastAcess();
          header('Location: painel.php');
        endif;
        if (isset($dataLogin['AdminLogin'])):
          $login->ExeLogin($dataLogin);
          if (!$login->getResult()):
            $erro = $login->getError();
            WSErro($erro[0], $erro[1], $erro[2]);
          else:
            $login->LastAcess();
            header('Location: painel.php');
          endif;
        elseif (isset($dataLogin["AdminReset"])):
          $data['user_email'] = (isset($email) ? $email : null);
          $data['user_password'] = $dataLogin['pass'];
          $data['user_key'] = $get;
          unset($dataLogin);
          $recPass = new RecoveryAccount;
          $recPass->ExeUpdate($data);
          var_dump($recPass);
          if (!$recPass->getResult()):
            $erro = $recPass->getError();
            WSErro($erro[0], $erro[1], null, $erro[2]);
          else:
            header('Location: index.php?exe=recsuccess');
          endif;
        endif;
        ?>
        <div class="separator">
          <div>
            <img src="<?= BASE; ?>/images/logo-sig.webp" title="SIG (Sistema Interno de Gerenciamento)" alt="SIG (Sistema Interno de Gerenciamento)" class="img-responsive"/>
          </div>
        </div>
        <form method="post">
          <h1>Acesso ao sistema</h1>
          <div>
            <input type="text" name="user" class="form-control" placeholder="E-mail" required <?php
            if (isset($email)): echo "readonly " . 'value="' . $email . '"';
            endif;
          ?>/>
        </div>
        <div>
          <label class="text-muted"><?= (!isset($email) ? "" : "Digite sua nova senha"); ?></label>
          <input type="password" name="pass" class="form-control" placeholder="Senha" required />
        </div>
        <div>
          <input type="submit" name="<?= (!isset($email) ? "AdminLogin" : "AdminReset"); ?>" class="btn btn-default" value="<?= (!isset($email) ? "Acessar" : "Atualizar"); ?>">
          <?php if (!isset($email)): ?>
            <a href="recpassword.php" class="reset_pass">Esqueceu a senha?</a>
          <?php else: ?>
            <a href="index.php" class="reset_pass">Lembrou a senha?</a>
          <?php endif; ?>
        </div>
        <div class="clearfix"></div>
        <div class="separator">
          <div>
            <h1><i class="fa fa-cogs"></i> <?= SITENAME ?></h1>
            <p>©2016 <?= SITENAME; ?> Todos os direitos reservados.</p>
          </div>
        </div>
      </form>
    </section>
  </div>
</div>
</body>
</html>