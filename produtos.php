<?
	$h1    			= 'Produtos';
	$title 			= 'Produtos';
	$desc  			= 'A Embalagem ideal atua no mercado de embalagens flexíveis há mais de 15 anos, veja as categorias de nossos produtos.';
	$key   			= 'envelope Personalizado, envelopes Personalizado, envelope Personalizados';
	$var 			= 'Envelopes Diversos';
	$produtos		= 'active';
	
	include('inc/head.php');
?>

<!-- função tabs regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
<script src="<?=$url;?>js/tabs.js" type="text/javascript"></script>

<!-- Tabs Regiões -->
<link rel="stylesheet" href="<?=$url;?>css/tabs.css" type="text/css" />
</head>
<body>

<div class="wrapper">
<? include('inc/topo.php');?>

    <main role="main">

        <section>

            <article>
                <?=$caminho?>      
                <h1><?=$h1?></h1>
                
                <? include('inc/social-media.php');?>
                <p>Clique nas categorias abaixo e navegue por nossos produtos:</p>
           		<ul class="thumbnails style">
                     <li>
                          <a rel="nofollow" href="<?=$url;?>sacos-diversos" title="Sacos Diversos"><img src="<?=$url;?><?=$pastaSacosDiversos;?>sacos-diversos.jpg" alt="Sacos Diversos" title="Sacos Diversos" /></a>
                          <h4><a href="<?=$url;?>sacos-diversos" title="Sacos Diversos">Sacos Diversos</a></h4>
                     </li>
                     <li>
                          <a rel="nofollow" href="<?=$url;?>sacos-plasticos" title="Sacos Plásticos"><img src="<?=$url;?><?=$pastaSacosPlasticos;?>sacos-plasticos.jpg" alt="Sacos Plásticos" title="Sacos Plásticos" /></a>
                          <h4><a href="<?=$url;?>sacos-plasticos" title="Sacos Plásticos">Sacos Plásticos</a></h4>
                     </li>
            	</ul>
                                
                <? include('inc/social-media.php');?>
                <? include('inc/paginas-relacionadas.php');?>
                
                
            </article>
            <? include('inc/coluna-lateral.php');?>
        </section>

    </main>

</div><!-- .wrapper -->
 <? include('inc/footer.php');?>
</body>
</html>