<?
	$h1    		= 'Sacos de Polietileno';
	$title 		= 'Sacos de Polietileno';
	$desc  		= 'O saco de polietileno é uma embalagem bastante versátil e, por isso, tem ampla utilização em diversos segmentos. O produto é utilizado...';
	$key   		= 'saco polietileno, sacos polietileno, saco polietilenos, sacos polietileno, saco de polietileno, saco de polietilenos, sacos de polietileno';
	$legendaImagem 	= 'Foto ilustrativa '.$h1.'';
	$var 		= 'Saco de Polietileno';
	$produtos	= 'active';
	
	include('inc/head.php');
?>
<!-- função tabs regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
<script src="<?=$url;?>js/tabs.js" type="text/javascript"></script>

<!-- Tabs Regiões -->
<link rel="stylesheet" href="<?=$url;?>css/tabs.css" type="text/css" />
</head>
<body>

<div class="wrapper">
<? include('inc/topo.php');?>

    <main role="main">

        <section>

            <article>
            <?=$caminhoServicosDiversos?>  
            	<h1><?=$h1?></h1>
                <div class="picture-legend picture-right">
                    <img src="<?=$url;?><?=$pastaSacosDiversos?><?=$urlGaleria?>-01.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
              	</div>
                <p>Conheça opção de embalagem bastante versátil e usada em vários segmentos. Confira os benefícios dos <strong>sacos de polietileno</strong>.</p>
                <p>Um material ideal para embalar e transportar seus produtos é o <strong>saco de polietileno</strong>, que pode ser liso, impresso em até seis cores, transparente ou pigmentado em diversas cores.</p>
                <p>O <strong>saco de polietileno</strong> é uma embalagem bastante versátil e, por isso, tem ampla utilização em diversos segmentos. O produto é utilizado para embalar desde alimentos, até produtos de higiene e limpeza.</p>
                <div class="picture-legend picture-left">
                    <img src="<?=$url;?><?=$pastaSacosDiversos?><?=$urlGaleria?>-03.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
              	</div>
                <p>O <strong>saco de polietileno</strong> é ideal para o transporte de produtos que tem pesos elevados, na faixa de 20, 25 ou 30 kg. Para objetos mais leves, é possível fabricá-lo com espessura mais fina. O material utilizado para confecção é totalmente flexível, e, em virtude disso, é possível produzi-lo com 450 micras ou 0,450 mm.</p>
                <p>O <strong>saco de polietileno</strong> é um produto que pode ser produzido com matéria 100% reciclada, e tem foco total em sustentabilidade. Neste caso, a confecção é feita a partir da reutilização de embalagens como sacolas, sacarias e embalagens de alimentos, que são recicladas, dando origem a uma nova embalagem.</p>
                <p>Uma das funcionalidades do <strong>saco de polietileno</strong> é que ele pode ser termo-encolhível. Neste caso, o plástico pode ser encolhido por meio de túneis de encolhimento ou sopradores térmicos. Neste processo, o plástico é aquecido e encolhe, se moldando ao formato do produto. Por isso, este tipo de embalagem é muito utilizado em moveleiras e indústrias alimentícias.</p>
                <h2>Saco de polietileno tem diversos modelos</h2>
                <p>O <strong>saco de polietileno</strong> pode ser fabricado de diversas maneiras. Há opções com fecho zip, que proporcionam abertura e fechamento da embalagem de maneira muito mais prática; saco reforçado, para garantir ainda mais segurança; opções de sacos reciclados cristal ou canela, além de sacos para brindes e muito mais.</p>
                <div class="picture-legend picture-right">
                    <img src="<?=$url;?><?=$pastaSacosDiversos?><?=$urlGaleria?>-03.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
              	</div>
                <p>Na JPR Embalagens você encontra <strong>saco de polietileno</strong> feitos com materiais de qualidade garantida, o que resulta em muito mais proteção e segurança para o seu produto. A empresa atua no segmento de embalagens flexíveis há mais de 15 anos, sempre levando as melhores soluções para os nossos clientes.</p>
                <p>O <strong>saco de polietileno</strong> é totalmente personalizado conforme as suas necessidades e preferências. Saiba mais entrando em contato com um dos consultores, aproveite as vantagens e peça já o seu orçamento.</p>
                                
            <? include('inc/saiba-mais.php');?>

            </article>

            <? include('inc/coluna-lateral.php');?>

            <br class="clear" />

            <? include('inc/social-media.php');?>

            <? include('inc/regioes.php');?>

            <? include('inc/paginas-relacionadas.php');?>

            

            <? include('inc/copyright.php');?>
        </section>

    </main>

</div><!-- .wrapper -->
<? include('inc/footer.php');?>
</body>
</html>