<?
	$h1    		= 'Saco Metalizado';
	$title 		= 'Saco Metalizado';
	$desc  		= 'O saco metalizado é um produto bastante utilizado em lojas, supermercados e shoppings como embalagem para presente, por causa de seu aspecto metalizado...';
	$key   		= 'sacos Metalizados, sacos Metalizado, saco Metalizados';
	$legendaImagem 	= 'Foto ilustrativa '.$h1.'';
	$var 		= 'Sacos Metalizados';
	$produtos	= 'active';
	
	include('inc/head.php');
?>
<!-- função tabs regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
<script src="<?=$url;?>js/tabs.js" type="text/javascript"></script>

<!-- Tabs Regiões -->
<link rel="stylesheet" href="<?=$url;?>css/tabs.css" type="text/css" />
</head>
<body>

<div class="wrapper">
<? include('inc/topo.php');?>

    <main role="main">

        <section>

            <article>
            <?=$caminhoServicosDiversos?>  
            	<h1><?=$h1?></h1>
                <div class="picture-legend picture-right">
                    <img src="<?=$url;?><?=$pastaSacosDiversos?><?=$urlGaleria?>-01.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
              	</div>
                <p>Amplamente utilizado no comércio, o <strong>saco metalizado</strong> traz uma série de vantagens. Confira quais são.</p>
                <p>Na hora de fabricar um produto, um dos aspectos fundamentais é a embalagem, que garante a conservação do que foi embalado e também aprimora a parte estética. Por isso, uma ótima opção é o <strong>saco metalizado</strong>.</p>
                <p>O <strong>saco metalizado</strong> é um produto bastante utilizado em lojas, supermercados e shoppings como embalagem para presente, por causa de seu aspecto metalizado. Geralmente, esse tipo de embalagem é fabricado com espessuras finas, sendo, portanto, ideais para o transporte de objetos com peso leve. O <strong>saco metalizado</strong> permite que você dispense o uso de seladoras, já que ele pode ser feito com aba adesiva.</p>
                <div class="picture-legend picture-left">
                    <img src="<?=$url;?><?=$pastaSacosDiversos?><?=$urlGaleria?>-02.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
              	</div>
                <p>Outra utilização frequente do <strong>saco metalizado</strong> é na indústria alimentícia. Isso porque a embalagem proporciona barreira à luz, além de contribuir para uma conservação mais prolongada do alimento. A embalagem é prática e segura e a metalização faz com que o produto fique em sigilo, já que não é possível observar o que está sendo carregado dentro da embalagem.</p>
                <p>É possível personalizar o <strong>saco metalizado</strong> de acordo com as suas preferências. Fabricado em BOPP, o produto pode ter um aspecto totalmente liso ou também pode ser impresso em até seis cores diferentes.</p>
                <h2>Saco metalizado da JPR Embalagens</h2>
                <p>E para adquirir o <strong>saco metalizado</strong>, aproveite os benefícios oferecidos pela JPR Embalagens. Com mais de 15 anos de atuação no mercado, a empresa tem profissionais altamente capacitados e com experiência na área de embalagens flexíveis.</p>
                <div class="picture-legend picture-right">
                    <img src="<?=$url;?><?=$pastaSacosDiversos?><?=$urlGaleria?>-03.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
              	</div>
                <p>A equipe da JPR Embalagens se empenha para levar até o cliente as melhores opções de embalagens, com um atendimento totalmente personalizado e voltado às necessidades de cada cliente. Além disso, os profissionais estão sempre em busca de inovações para elevar ainda mais a qualidade dos produtos e reduzir os preços de fabricação, o que se resulta em um valor menor também para os clientes.</p>
                <p>Entrando em contato com um dos consultores, você fica sabendo mais sobre as aplicações e vantagens do <strong>saco metalizado</strong>. Aproveite os benefícios e solicite já seu orçamento, informando medidas e quantidade de sacos que você precisa.</p>
                
            <? include('inc/saiba-mais.php');?>

            </article>

            <? include('inc/coluna-lateral.php');?>

            <br class="clear" />

            <? include('inc/social-media.php');?>

            <? include('inc/regioes.php');?>

            <? include('inc/paginas-relacionadas.php');?>

            

            <? include('inc/copyright.php');?>
        </section>

    </main>

</div><!-- .wrapper -->
<? include('inc/footer.php');?>
</body>
</html>