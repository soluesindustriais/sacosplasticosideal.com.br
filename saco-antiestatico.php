<?
	$h1    		= 'Saco Antiestático';
	$title 		= 'Saco Antiestático';
	$desc  		= 'O saco antiestático é uma embalagem versátil e eficiente em caso de queda dos produtos. Confira maiores informações sobre este tipo de embalagem.';
	$key   		= 'sacos antiestaticos, sacos antiestatico, saco antiestatico, saco antiestatico, sacos antiestáticos, sacos antiestático, saco antiestático';
	$legendaImagem 	= 'Foto ilustrativa '.$h1.'';
	$var 		= 'Sacos Antiestaticos';
	$produtos	= 'active';
	
	include('inc/head.php');
?>
<!-- função tabs regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
<script src="<?=$url;?>js/tabs.js" type="text/javascript"></script>

<!-- Tabs Regiões -->
<link rel="stylesheet" href="<?=$url;?>css/tabs.css" type="text/css" />
</head>
<body>

<div class="wrapper">
<? include('inc/topo.php');?>

    <main role="main">

        <section>

            <article>
            <?=$caminhoServicosDiversos?>  
            	<h1><?=$h1?></h1>
                <div class="picture-legend picture-right">
                    <img src="<?=$url;?><?=$pastaSacosDiversos?><?=$urlGaleria?>-01.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
              	</div>
                <p>O <strong>saco antiestático</strong> é uma embalagem versátil e eficiente em caso de queda dos produtos. Confira maiores informações sobre este tipo de embalagem.</p>
                <p>Durante o transporte de alguma mercadoria, é possível que ocorram incidentes, como quedas ou impactos e estática, por exemplo. Para evitar danos relacionados a estes tipos de problemas, uma ótima solução é o <strong>saco antiestático</strong>.</p>
                <p>O <strong>saco antiestático</strong> é uma embalagem bastante versátil, que tem como função proteger os produtos contra impactos e quedas, além de ser uma embalagem que ocupa um espaço muito menor que uma caixa de papelão, fazendo com que ela seja uma ótima opção para armazenar e transportar produtos.</p>
                <div class="picture-legend picture-left">
                    <img src="<?=$url;?><?=$pastaSacosDiversos?><?=$urlGaleria?>-02.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
              	</div>
                <p>O <strong>saco antiestático</strong> é um produto que foi desenvolvido com materiais de primeira linha, que conferem segurança e resistência durante o transporte das suas mercadorias, fazendo com que elas cheguem intactas até o destino final. Além disso, este tipo de envelope pode ser personalizado, com fabricação sob medida, conforme a necessidade de cada cliente.</p>
                <p>Outras opções de <strong>saco antiestático</strong> são linhas de embalagens que tem bolha, como é o caso de saco plástico com bolhas, bobinas de plástico bolha, saco bolha colorido, saco bolha com aba adesiva e saco bolha comum. Todos estes produtos asseguram a proteção do que está sendo transportado.</p>
                <h2>Confira onde encontrar saco antiestático com qualidade</h2>
                <p>Para economizar na hora de adquirir <strong>saco antiestático</strong>, conte com a JPR Embalagens. A empresa atua no mercado de embalagens flexíveis há mais de 15 anos, sempre buscando as melhores opções neste segmento.</p>
                <div class="picture-legend picture-right">
                    <img src="<?=$url;?><?=$pastaSacosDiversos?><?=$urlGaleria?>-03.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
              	</div>
                <p>A JPR Embalagens tem equipe com vasta experiência na área e que está sempre em busca de inovações que visem elevar a segurança, a resistência e a qualidade das embalagens, além de reduzir os custos de produção, o que resulta em um custo menor para os clientes.</p>
                <p>Entrando em contato com um dos consultores da empresa, é possível saber mais sobre as vantagens que o saco antiestético proporciona para o transporte dos seus produtos. Esclareça suas dúvidas e aproveite as ótimas condições para solicitar já o seu orçamento, informando as medidas (largura x cumprimento) e a quantidade de <strong>saco antiestático</strong> que você precisa.</p>
                
            <? include('inc/saiba-mais.php');?>

            </article>

            <? include('inc/coluna-lateral.php');?>

            <br class="clear" />

            <? include('inc/social-media.php');?>

            <? include('inc/regioes.php');?>

            <? include('inc/paginas-relacionadas.php');?>

            

            <? include('inc/copyright.php');?>
        </section>

    </main>

</div><!-- .wrapper -->
<? include('inc/footer.php');?>
</body>
</html>