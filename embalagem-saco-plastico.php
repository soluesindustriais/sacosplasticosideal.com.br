<?
	$h1    		= 'Embalagem Saco Plástico';
	$title 		= 'Embalagem Saco Plástico';
	$desc  		= 'A embalagem saco plástico pode ser de diversos tipos, feitos em diferentes materiais, cores e com possibilidade de personalização. Confira maiores detalhes.';
	$key   		= 'embalagem saco plastico, embalagem sacos plasticos, embalagem sacos plastico, embalagem saco plasticos, embalagem sacos plásticos, embalagem sacos plástico';
	$legendaImagem 	= 'Foto ilustrativa '.$h1.'';
	$var 		= 'Embalagens Sacos Plasticos';
	$produtos	= 'active';
	
	include('inc/head.php');
?>
<!-- função tabs regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
<script src="<?=$url;?>js/tabs.js" type="text/javascript"></script>

<!-- Tabs Regiões -->
<link rel="stylesheet" href="<?=$url;?>css/tabs.css" type="text/css" />
</head>
<body>

<div class="wrapper">
<? include('inc/topo.php');?>

    <main role="main">

        <section>

            <article>
            <?=$caminhoServicosDiversos?>  
            	<h1><?=$h1?></h1>
                <div class="picture-legend picture-right">
                    <img src="<?=$url;?><?=$pastaSacosDiversos?><?=$urlGaleria?>-01.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
              	</div>
                <p>A <strong>embalagem saco plástico</strong> pode ser de diversos tipos, feitos em diferentes materiais, cores e com possibilidade de personalização. Confira maiores detalhes.</p>
                <p>A <strong>embalagem saco plástico</strong> é um produto amplamente utilizado em diversos setores. Isso porque a embalagem é um produto versátil, que pode ser fabricado em PEBD, PEAD, PP, BOPP, laminado, entre outros materiais.</p>
                <p>A <strong>embalagem saco plástico</strong> de polipropileno é bastante utilizada em indústrias do setor alimentício, em gráficas e também em confecções, já que a embalagem tem brilho e é transparente.</p>
                <div class="picture-legend picture-left">
                    <img src="<?=$url;?><?=$pastaSacosDiversos?><?=$urlGaleria?>-02.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
              	</div>
                <p>A <strong>embalagem saco plástico</strong> de polietileno é uma das embalagens mais usadas para bebidas, alimentos, leites, farinhas e também usado para produtos automotivos e metalúrgicos. Por ser uma embalagem atóxica e resistente, pode ser utilizada para embalar qualquer tipo de produto. Outra opção de <strong>embalagem saco plástico</strong> é o BOPP, que pode ser metalizado ou perolizado e que é ideal como embalagem para presentes, e por isso é bastante utilizado na área de varejo.</p>
                <p>A <strong>embalagem saco plástico</strong> também pode ser lacrada através de seladora, pode ter aba adesiva permanente ou do tipo abre e fecha. No caso de fechamento permanente, a aba se torna inviolável, sendo necessário danificar a embalagem para ter acesso ao produto.</p>
                <h2>Embalagem saco plástico também pode ser reciclada</h2>
                <p>Outra tipo de embalagem que contribui com o meio ambiente é a <strong>embalagem saco plástico</strong> reciclada, que pode ser feita em reciclado cristal ou canela. No caso do saco reciclado em cristal, a produção é a partir de aparas do material virgem. Por ser reciclado, a cor da embalagem se torna amarelo claro, mas a embalagem segue transparente e permitindo a visualização do que está sendo transportado.</p>
                <div class="picture-legend picture-right">
                    <img src="<?=$url;?><?=$pastaSacosDiversos?><?=$urlGaleria?>-03.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
              	</div>
                <p>A <strong>embalagem saco plástico</strong> reciclada canela é feita a partir das aparas do reciclado cristal e tem cor alterada para canela. Assim como a embalagem reciclado cristal, esta embalagem também fica transparente e permite a visualização do que está dentro.</p>
                <p>Para adquirir <strong>embalagem saco plástico</strong>, conte com os benefícios da JPR Embalagens. A empresa está há mais de 15 anos no mercado e fabrica todos estes tipos de embalagens descritos acima, além de ser especialista em embalagens flexíveis.</p>
                
                <p>Entre em contato com os consultores da JPR Embalagens para maiores informações e solicite o seu orçamento.</p>
                
            <? include('inc/saiba-mais.php');?>

            </article>

            <? include('inc/coluna-lateral.php');?>

            <br class="clear" />

            <? include('inc/social-media.php');?>

            <? include('inc/regioes.php');?>

            <? include('inc/paginas-relacionadas.php');?>

            

            <? include('inc/copyright.php');?>
        </section>

    </main>

</div><!-- .wrapper -->
<? include('inc/footer.php');?>
</body>
</html>