<?
	$h1    		= 'Sacos Plásticos';
	$title 		= 'Sacos Plásticos';
	$desc  		= 'Temos diversos tipos de Sacos Plásticos, como: Saco Plástico Resistente, Saco Plástico Sanfonado, Saco Plástico de Segurança e muito mais. Confira!';
	$key   		= 'saco Ziplock, sacos Ziplocks, sacos Ziplock, saco Ziplocks, sacos com Ziplocks, sacos com Ziplock, saco com Ziplocks';
	$var 		= 'Saco Plasticos';
	$produtos	= 'active';
	
	include('inc/head.php');
?>
<!-- função tabs regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
<script src="<?=$url;?>js/tabs.js" type="text/javascript"></script>

<!-- Tabs Regiões -->
<link rel="stylesheet" href="<?=$url;?>css/tabs.css" type="text/css" />
</head>
<body>

<div class="wrapper">
<? include('inc/topo.php');?>

    <main role="main">

        <section>

            <article>
            <?=$caminhoServicos?>  
            	<h1><?=$h1?></h1>
                
                <p>Veja abaixo o que a JPR Embalagens oferece de melhor quando o assunto é Sacos Plásticos:</p>
                
                <ul class="thumbnails">
                 <li>
                      <a rel="nofollow" href="<?=$url;?>saco-plastico-bolha" title="Saco Plástico Bolha"><img src="<?=$url;?><?=$pastaSacosPlasticos;?>thumb/saco-plastico-bolha-01.jpg" alt="Saco Plástico Bolha" title="Saco Plástico Bolha" /></a>
                      <h4><a href="<?=$url;?>saco-plastico-bolha" title="Saco Plástico Bolha">Saco Plástico Bolha</a></h4>
                 </li>
                 <li>
                      <a rel="nofollow" href="<?=$url;?>saco-plastico-branco" title="Saco Plástico Branco"><img src="<?=$url;?><?=$pastaSacosPlasticos;?>thumb/saco-plastico-branco-01.jpg" alt="Saco Plástico Branco" title="Saco Plástico Branco" /></a>
                      <h4><a href="<?=$url;?>saco-plastico-branco" title="Saco Plástico Branco">Saco Plástico Branco</a></h4>
                 </li>
                 <li>
                      <a rel="nofollow" href="<?=$url;?>saco-plastico-branco-leitoso" title="Saco Plástico Branco Leitoso"><img src="<?=$url;?><?=$pastaSacosPlasticos;?>thumb/saco-plastico-branco-leitoso-01.jpg" alt="Saco Plástico Branco Leitoso" title="Saco Plástico Branco Leitoso" /></a>
                      <h4><a href="<?=$url;?>saco-plastico-branco-leitoso" title="Saco Plástico Branco Leitoso">Saco Plástico Branco Leitoso</a></h4>
                 </li>
                 <li>
                      <a rel="nofollow" href="<?=$url;?>saco-plastico-fecho-ziplock" title="Saco Plástico Com Fecho Ziplock"><img src="<?=$url;?><?=$pastaSacosPlasticos;?>thumb/saco-plastico-fecho-ziplock-01.jpg" alt="Saco Plástico Com Fecho Ziplock" title="Saco Plástico Com Fecho Ziplock" /></a>
                      <h4><a href="<?=$url;?>saco-plastico-fecho-ziplock" title="Saco Plástico Com Fecho Ziplock">Saco Plástico Com Fecho Ziplock</a></h4>
                 </li>
                 <li>
                      <a rel="nofollow" href="<?=$url;?>saco-plastico-seguranca" title="Saco Plástico de Segurança"><img src="<?=$url;?><?=$pastaSacosPlasticos;?>thumb/saco-plastico-seguranca-01.jpg" alt="Saco Plástico de Segurança" title="Saco Plástico de Segurança" /></a>
                      <h4><a href="<?=$url;?>saco-plastico-seguranca" title="Saco Plástico de Segurança">Saco Plástico de Segurança</a></h4>
                 </li>
                 <li>
                      <a rel="nofollow" href="<?=$url;?>saco-plastico-fundo-quadrado" title="Saco Plástico Fundo Quadrado"><img src="<?=$url;?><?=$pastaSacosPlasticos;?>thumb/saco-plastico-fundo-quadrado-01.jpg" alt="Saco Plástico Fundo Quadrado" title="Saco Plástico Fundo Quadrado" /></a>
                      <h4><a href="<?=$url;?>saco-plastico-fundo-quadrado" title="Saco Plástico Fundo Quadrado">Saco Plástico Fundo Quadrado</a></h4>
                 </li>
                 <li>
                      <a rel="nofollow" href="<?=$url;?>saco-plastico-gofrado" title="Saco Plástico Gofrado"><img src="<?=$url;?><?=$pastaSacosPlasticos;?>thumb/saco-plastico-gofrado-01.jpg" alt="Saco Plástico Gofrado" title="Saco Plástico Gofrado" /></a>
                      <h4><a href="<?=$url;?>saco-plastico-gofrado" title="Saco Plástico Gofrado">Saco Plástico Gofrado</a></h4>
                 </li>
                 <li>
                      <a rel="nofollow" href="<?=$url;?>saco-plastico-grosso" title="Saco Plástico Grosso"><img src="<?=$url;?><?=$pastaSacosPlasticos;?>thumb/saco-plastico-grosso-01.jpg" alt="Saco Plástico Grosso" title="Saco Plástico Grosso" /></a>
                      <h4><a href="<?=$url;?>saco-plastico-grosso" title="Saco Plástico Grosso">Saco Plástico Grosso</a></h4>
                 </li>
                 <li>
                      <a rel="nofollow" href="<?=$url;?>saco-plastico-hot-melt" title="Saco Plástico Hot Melt"><img src="<?=$url;?><?=$pastaSacosPlasticos;?>thumb/saco-plastico-hot-melt-01.jpg" alt="Saco Plástico Hot Melt" title="Saco Plástico Hot Melt" /></a>
                      <h4><a href="<?=$url;?>saco-plastico-hot-melt" title="Saco Plástico Hot Melt">Saco Plástico Hot Melt</a></h4>
                 </li>
                 <li>
                      <a rel="nofollow" href="<?=$url;?>saco-plastico-incolor" title="Saco Plástico Incolor"><img src="<?=$url;?><?=$pastaSacosPlasticos;?>thumb/saco-plastico-incolor-01.jpg" alt="Saco Plástico Incolor" title="Saco Plástico Incolor" /></a>
                      <h4><a href="<?=$url;?>saco-plastico-incolor" title="Saco Plástico Incolor">Saco Plástico Incolor</a></h4>
                 </li>
                 <li>
                      <a rel="nofollow" href="<?=$url;?>saco-plastico-laminado" title="Saco Plástico Laminado"><img src="<?=$url;?><?=$pastaSacosPlasticos;?>thumb/saco-plastico-laminado-01.jpg" alt="Saco Plástico Laminado" title="Saco Plástico Laminado" /></a>
                      <h4><a href="<?=$url;?>saco-plastico-laminado" title="Saco Plástico Laminado">Saco Plástico Laminado</a></h4>
                 </li>
                 <li>
                      <a rel="nofollow" href="<?=$url;?>saco-plastico-leitoso" title="Saco Plástico Leitoso"><img src="<?=$url;?><?=$pastaSacosPlasticos;?>thumb/saco-plastico-leitoso-01.jpg" alt="Saco Plástico Leitoso" title="Saco Plástico Leitoso" /></a>
                      <h4><a href="<?=$url;?>saco-plastico-leitoso" title="Saco Plástico Leitoso">Saco Plástico Leitoso</a></h4>
                 </li>
                 <li>
                      <a rel="nofollow" href="<?=$url;?>saco-plastico-liso" title="Saco Plástico Liso"><img src="<?=$url;?><?=$pastaSacosPlasticos;?>thumb/saco-plastico-liso-01.jpg" alt="Saco Plástico Liso" title="Saco Plástico Liso" /></a>
                      <h4><a href="<?=$url;?>saco-plastico-liso" title="Saco Plástico Liso">Saco Plástico Liso</a></h4>
                 </li>
                 <li>
                      <a rel="nofollow" href="<?=$url;?>saco-plastico-mala-direta" title="Saco Plástico Mala Direta"><img src="<?=$url;?><?=$pastaSacosPlasticos;?>thumb/saco-plastico-mala-direta-01.jpg" alt="Saco Plástico Mala Direta" title="Saco Plástico Mala Direta" /></a>
                      <h4><a href="<?=$url;?>saco-plastico-mala-direta" title="Saco Plástico Mala Direta">Saco Plástico Mala Direta</a></h4>
                 </li>
                 <li>
                      <a rel="nofollow" href="<?=$url;?>saco-plastico-microperfurado" title="Saco Plástico Microperfurado"><img src="<?=$url;?><?=$pastaSacosPlasticos;?>thumb/saco-plastico-microperfurado-01.jpg" alt="Saco Plástico Microperfurado" title="Saco Plástico Microperfurado" /></a>
                      <h4><a href="<?=$url;?>saco-plastico-microperfurado" title="Saco Plástico Microperfurado">Saco Plástico Microperfurado</a></h4>
                 </li>
                 <li>
                      <a rel="nofollow" href="<?=$url;?>saco-plastico-oficio" title="Saco Plástico Oficio"><img src="<?=$url;?><?=$pastaSacosPlasticos;?>thumb/saco-plastico-oficio-01.jpg" alt="Saco Plástico Oficio" title="Saco Plástico Oficio" /></a>
                      <h4><a href="<?=$url;?>saco-plastico-oficio" title="Saco Plástico Oficio">Saco Plástico Oficio</a></h4>
                 </li>
                 <li>
                      <a rel="nofollow" href="<?=$url;?>saco-plastico-opaco" title="Saco Plástico Opaco"><img src="<?=$url;?><?=$pastaSacosPlasticos;?>thumb/saco-plastico-opaco-01.jpg" alt="Saco Plástico Opaco" title="Saco Plástico Opaco" /></a>
                      <h4><a href="<?=$url;?>saco-plastico-opaco" title="Saco Plástico Opaco">Saco Plástico Opaco</a></h4>
                 </li>
                 <li>
                      <a rel="nofollow" href="<?=$url;?>saco-plastico-documentos" title="Saco Plástico para Documentos"><img src="<?=$url;?><?=$pastaSacosPlasticos;?>thumb/saco-plastico-documentos-01.jpg" alt="Saco Plástico para Documentos" title="Saco Plástico para Documentos" /></a>
                      <h4><a href="<?=$url;?>saco-plastico-documentos" title="Saco Plástico para Documentos">Saco Plástico para Documentos</a></h4>
                 </li>
                 <li>
                      <a rel="nofollow" href="<?=$url;?>saco-plastico-jornal" title="Saco Plástico para Jornal"><img src="<?=$url;?><?=$pastaSacosPlasticos;?>thumb/saco-plastico-jornal-01.jpg" alt="Saco Plástico para Jornal" title="Saco Plástico para Jornal" /></a>
                      <h4><a href="<?=$url;?>saco-plastico-jornal" title="Saco Plástico para Jornal">Saco Plástico para Jornal</a></h4>
                 </li>
                 <li>
                      <a rel="nofollow" href="<?=$url;?>saco-plastico-pe" title="Saco Plástico PE"><img src="<?=$url;?><?=$pastaSacosPlasticos;?>thumb/saco-plastico-pe-01.jpg" alt="Saco Plástico PE" title="Saco Plástico PE" /></a>
                      <h4><a href="<?=$url;?>saco-plastico-pe" title="Saco Plástico PE">Saco Plástico PE</a></h4>
                 </li>
                 <li>
                      <a rel="nofollow" href="<?=$url;?>saco-plastico-resistente" title="Saco Plástico Resistente"><img src="<?=$url;?><?=$pastaSacosPlasticos;?>thumb/saco-plastico-resistente-01.jpg" alt="Saco Plástico Resistente" title="Saco Plástico Resistente" /></a>
                      <h4><a href="<?=$url;?>saco-plastico-resistente" title="Saco Plástico Resistente">Saco Plástico Resistente</a></h4>
                 </li>
                 <li>
                      <a rel="nofollow" href="<?=$url;?>saco-plastico-sanfonado" title="Saco Plástico Sanfonado"><img src="<?=$url;?><?=$pastaSacosPlasticos;?>thumb/saco-plastico-sanfonado-01.jpg" alt="Saco Plástico Sanfonado" title="Saco Plástico Sanfonado" /></a>
                      <h4><a href="<?=$url;?>saco-plastico-sanfonado" title="Saco Plástico Sanfonado">Saco Plástico Sanfonado</a></h4>
                 </li>
                 <li>
                      <a rel="nofollow" href="<?=$url;?>saco-plastico-sanfonado-zona-sul" title="Saco Plástico Sanfonado Zona Sul"><img src="<?=$url;?><?=$pastaSacosPlasticos;?>thumb/saco-plastico-sanfonado-zona-sul-01.jpg" alt="Saco Plástico Sanfonado Zona Sul" title="Saco Plástico Sanfonado Zona Sul" /></a>
                      <h4><a href="<?=$url;?>saco-plastico-sanfonado-zona-sul" title="Saco Plástico Sanfonado Zona Sul">Saco Plástico Sanfonado Zona Sul</a></h4>
                 </li>
                 <li>
                      <a rel="nofollow" href="<?=$url;?>saco-plastico-transparente" title="Saco Plástico Transparente"><img src="<?=$url;?><?=$pastaSacosPlasticos;?>thumb/saco-plastico-transparente-01.jpg" alt="Saco Plástico Transparente" title="Saco Plástico Transparente" /></a>
                      <h4><a href="<?=$url;?>saco-plastico-transparente" title="Saco Plástico Transparente">Saco Plástico Transparente</a></h4>
                 </li>
                 <li>
                      <a rel="nofollow" href="<?=$url;?>saco-plastico-transparente-4-furos" title="Saco Plástico Transparente Com 4 Furos"><img src="<?=$url;?><?=$pastaSacosPlasticos;?>thumb/saco-plastico-transparente-4-furos-01.jpg" alt="Saco Plástico Transparente Com 4 Furos" title="Saco Plástico Transparente Com 4 Furos" /></a>
                      <h4><a href="<?=$url;?>saco-plastico-transparente-4-furos" title="Saco Plástico Transparente Com 4 Furos">Saco Plástico Transparente Com 4 Furos</a></h4>
                 </li>
                 <li>
                      <a rel="nofollow" href="<?=$url;?>saco-plastico-transparente-adesivo" title="Saco Plástico Transparente Com Adesivo"><img src="<?=$url;?><?=$pastaSacosPlasticos;?>thumb/saco-plastico-transparente-adesivo-01.jpg" alt="Saco Plástico Transparente Com Adesivo" title="Saco Plástico Transparente Com Adesivo" /></a>
                      <h4><a href="<?=$url;?>saco-plastico-transparente-adesivo" title="Saco Plástico Transparente Com Adesivo">Saco Plástico Transparente Com Adesivo</a></h4>
                 </li>
                 <li>
                      <a rel="nofollow" href="<?=$url;?>saco-plastico-transparente-grande" title="Saco Plástico Transparente Grande"><img src="<?=$url;?><?=$pastaSacosPlasticos;?>thumb/saco-plastico-transparente-grande-01.jpg" alt="Saco Plástico Transparente Grande" title="Saco Plástico Transparente Grande" /></a>
                      <h4><a href="<?=$url;?>saco-plastico-transparente-grande" title="Saco Plástico Transparente Grande">Saco Plástico Transparente Grande</a></h4>
                 </li>
                 <li>
                      <a rel="nofollow" href="<?=$url;?>saco-plastico-transparente-embalagem" title="Saco Plástico Transparente para Embalagem"><img src="<?=$url;?><?=$pastaSacosPlasticos;?>thumb/saco-plastico-transparente-embalagem-01.jpg" alt="Saco Plástico Transparente para Embalagem" title="Saco Plástico Transparente para Embalagem" /></a>
                      <h4><a href="<?=$url;?>saco-plastico-transparente-embalagem" title="Saco Plástico Transparente para Embalagem">Saco Plástico Transparente para Embalagem</a></h4>
                 </li>
                 <li>
                      <a rel="nofollow" href="<?=$url;?>saco-plastico-vai-vem" title="Saco Plástico Vai-vem"><img src="<?=$url;?><?=$pastaSacosPlasticos;?>thumb/saco-plastico-vai-vem-01.jpg" alt="Saco Plástico Vai-vem" title="Saco Plástico Vai-vem" /></a>
                      <h4><a href="<?=$url;?>saco-plastico-vai-vem" title="Saco Plástico Vai-vem">Saco Plástico Vai-vem</a></h4>
                 </li>
                 <li>
                      <a rel="nofollow" href="<?=$url;?>saco-plastico-valvulado" title="Saco Plástico Valvulado"><img src="<?=$url;?><?=$pastaSacosPlasticos;?>thumb/saco-plastico-valvulado-01.jpg" alt="Saco Plástico Valvulado" title="Saco Plástico Valvulado" /></a>
                      <h4><a href="<?=$url;?>saco-plastico-valvulado" title="Saco Plástico Valvulado">Saco Plástico Valvulado</a></h4>
                 </li>
                 <li>
                      <a rel="nofollow" href="<?=$url;?>sacos-plasticos-adesivados" title="Sacos Plásticos Adesivados"><img src="<?=$url;?><?=$pastaSacosPlasticos;?>thumb/sacos-plasticos-adesivados-01.jpg" alt="Sacos Plásticos Adesivados" title="Sacos Plásticos Adesivados" /></a>
                      <h4><a href="<?=$url;?>sacos-plasticos-adesivados" title="Sacos Plásticos Adesivados">Sacos Plásticos Adesivados</a></h4>
                 </li>
                 <li>
                      <a rel="nofollow" href="<?=$url;?>sacos-plasticos-grandes" title="Sacos Plásticos Grandes"><img src="<?=$url;?><?=$pastaSacosPlasticos;?>thumb/sacos-plasticos-grandes-01.jpg" alt="Sacos Plásticos Grandes" title="Sacos Plásticos Grandes" /></a>
                      <h4><a href="<?=$url;?>sacos-plasticos-grandes" title="Sacos Plásticos Grandes">Sacos Plásticos Grandes</a></h4>
                 </li>
                 <li>
                      <a rel="nofollow" href="<?=$url;?>sacos-plasticos-embalagem" title="Sacos Plásticos para Embalagem"><img src="<?=$url;?><?=$pastaSacosPlasticos;?>thumb/sacos-plasticos-embalagem-01.jpg" alt="Sacos Plásticos para Embalagem" title="Sacos Plásticos para Embalagem" /></a>
                      <h4><a href="<?=$url;?>sacos-plasticos-embalagem" title="Sacos Plásticos para Embalagem">Sacos Plásticos para Embalagem</a></h4>
                 </li>
                 <li>
                      <a rel="nofollow" href="<?=$url;?>sacos-plasticos-personalizados" title="Sacos Plásticos Personalizados"><img src="<?=$url;?><?=$pastaSacosPlasticos;?>thumb/sacos-plasticos-personalizados-01.jpg" alt="Sacos Plásticos Personalizados" title="Sacos Plásticos Personalizados" /></a>
                      <h4><a href="<?=$url;?>sacos-plasticos-personalizados" title="Sacos Plásticos Personalizados">Sacos Plásticos Personalizados</a></h4>
                 </li>
                 <li>
                      <a rel="nofollow" href="<?=$url;?>sacos-plasticos-zip" title="Sacos Plásticos ZIP"><img src="<?=$url;?><?=$pastaSacosPlasticos;?>thumb/sacos-plasticos-zip-01.jpg" alt="Sacos Plásticos ZIP" title="Sacos Plásticos ZIP" /></a>
                      <h4><a href="<?=$url;?>sacos-plasticos-zip" title="Sacos Plásticos ZIP">Sacos Plásticos ZIP</a></h4>
                 </li>
                 <li>
                      <a rel="nofollow" href="<?=$url;?>sacos-plasticos-zip-lock" title="Sacos Plásticos ZIP Lock"><img src="<?=$url;?><?=$pastaSacosPlasticos;?>thumb/sacos-plasticos-zip-lock-01.jpg" alt="Sacos Plásticos ZIP Lock" title="Sacos Plásticos ZIP Lock" /></a>
                      <h4><a href="<?=$url;?>sacos-plasticos-zip-lock" title="Sacos Plásticos ZIP Lock">Sacos Plásticos ZIP Lock</a></h4>
                 </li>
            </ul>
                
            <? include('inc/saiba-mais.php');?>

            </article>

            <? include('inc/coluna-lateral.php');?>

            <br class="clear" />

            <? include('inc/social-media.php');?>

            <? include('inc/regioes.php');?>

            <? include('inc/paginas-relacionadas.php');?>

            

            <? include('inc/copyright.php');?>
        </section>

    </main>

</div><!-- .wrapper -->
<? include('inc/footer.php');?>
</body>
</html>