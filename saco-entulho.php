<?
	$h1    		= 'Saco de Entulho';
	$title 		= 'Saco de Entulho';
	$desc  		= 'O saco de entulho, também conhecido como saco para material de obra, é fabricado em polietileno de baixa densidade, virgem ou...';
	$key   		= 'saco entulho, sacos entulhos, sacos entulho, saco entulhos, sacos de entulhos, sacos de entulho, saco de entulhos';
	$legendaImagem 	= 'Foto ilustrativa '.$h1.'';
	$var 		= 'Sacos de Entulho';
	$produtos	= 'active';
	
	include('inc/head.php');
?>
<!-- função tabs regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
<script src="<?=$url;?>js/tabs.js" type="text/javascript"></script>

<!-- Tabs Regiões -->
<link rel="stylesheet" href="<?=$url;?>css/tabs.css" type="text/css" />
</head>
<body>

<div class="wrapper">
<? include('inc/topo.php');?>

    <main role="main">

        <section>

            <article>
            <?=$caminhoServicosDiversos?>  
            	<h1><?=$h1?></h1>
                <div class="picture-legend picture-right">
                    <img src="<?=$url;?><?=$pastaSacosDiversos?><?=$urlGaleria?>-01.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
              	</div>
                <p>Entulhos requerem embalagens resistentes e que suportem o peso. Por isso, conheça as vantagens do <strong>saco de entulho</strong>.</p>
                <p>O descarte de entulho deve ser feito em embalagens especializadas para este tipo de material, para que haja segurança. Por isso, a opção ideal é o <strong>saco de entulho</strong>.</p>
                <p>O entulho e restos de construção, como pedaços de tijolos, pisos cerâmicos e restos de argamassa sempre sobram no final, causando transtorno a quem está fazendo as obras. O descarte correto é complicado e, em algumas cidades, ocorre em pequenas quantidades por meio de serviços de coleta domiciliar. Mas, neste caso, estes restos devem estar armazenados em <strong>saco de entulho</strong>. É possível ainda descartar estes resíduos em postos autorizados. Também neste caso é necessário que os restos de obras estejam depositados em <strong>saco de entulho</strong>.</p>
                <div class="picture-legend picture-left">
                    <img src="<?=$url;?><?=$pastaSacosDiversos?><?=$urlGaleria?>-02.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
              	</div>
                <p>Além desta aplicação, o <strong>saco de entulho</strong> de outras funções, tais como o acondicionamento de produtos químicos, farmacêuticos, granulados e acondicionamento de pó, além de transporte de materiais de construção como brita e areia.</p>
                <p>O <strong>saco de entulho</strong>, também conhecido como saco para material de obra, é fabricado em polietileno de baixa densidade, virgem ou reciclado, que é confeccionado de forma mais resistente, para suportar carga maior de resíduos sem que haja danos. A produção é feita dentro de especificações técnicas e padrão de qualidade extremamente elevada, garantindo que os produtos sejam transportados de maneira segura e correta.</p>
                <div class="picture-legend picture-right">
                    <img src="<?=$url;?><?=$pastaSacosDiversos?><?=$urlGaleria?>-03.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
              	</div>
                <p>O <strong>saco de entulho</strong> pode ser ecologicamente correto. Neste caso, ele é fabricado a partir de matéria-prima reciclada, utilizando outras embalagens, como sacos plásticos, sacos de alimentos, sacolas, areais, entre outras embalagens.</p>
                <h2>Saco de entulho da JPR Embalagens</h2>
                <p>Conte com os benefícios da JPR Embalagens na hora de adquirir o <strong>saco de entulho</strong>. A empresa está presente no mercado há mais de 15 anos, levando até os clientes as soluções mais inteligentes na área de embalagens flexíveis.</p>
                <p>O <strong>saco de entulho</strong> pode ser personalizado conforme as suas necessidades e é fabricado com materiais extremamente resistentes e de qualidade. A JPR Embalagens oferece preço em conta e ótimas condições de pagamento. Confira os benefícios e aproveite as vantagens para solicitar já seu orçamento.</p>
                
            <? include('inc/saiba-mais.php');?>

            </article>

            <? include('inc/coluna-lateral.php');?>

            <br class="clear" />

            <? include('inc/social-media.php');?>

            <? include('inc/regioes.php');?>

            <? include('inc/paginas-relacionadas.php');?>

            

            <? include('inc/copyright.php');?>
        </section>

    </main>

</div><!-- .wrapper -->
<? include('inc/footer.php');?>
</body>
</html>