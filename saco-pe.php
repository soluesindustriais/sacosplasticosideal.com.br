<?
	$h1    		= 'Saco PE';
	$title 		= 'Saco PE';
	$desc  		= 'O saco PE é um produto versátil e protetor, que é amplamente utilizado por diversos segmentos. Há possibilidade de personalizar...';
	$key   		= 'sacos PEs, sacos PE, saco PEs';
	$legendaImagem 	= 'Foto ilustrativa '.$h1.'';
	$var 		= 'Sacos PEs';
	$produtos	= 'active';
	
	include('inc/head.php');
?>
<!-- função tabs regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
<script src="<?=$url;?>js/tabs.js" type="text/javascript"></script>

<!-- Tabs Regiões -->
<link rel="stylesheet" href="<?=$url;?>css/tabs.css" type="text/css" />
</head>
<body>

<div class="wrapper">
<? include('inc/topo.php');?>

    <main role="main">

        <section>

            <article>
            <?=$caminhoServicosDiversos?>  
            	<h1><?=$h1?></h1>
                
                <p>Embalagem versátil e de qualidade elevada, o <strong>saco PE</strong> tem ampla utilização em diversos segmentos. Confira as vantagens.</p>
                <p>A embalagem é um ponto fundamental para manter a conservação dos seus produtos e mercadorias. Por isso, uma ótima opção é o <strong>saco PE</strong>, que pode ser fabricado em polietileno de alta ou de baixa densidade.</p>
                <p>O <strong>saco PE</strong> é um produto versátil e protetor, que é amplamente utilizado por diversos segmentos. Há possibilidade de personalizar a embalagem conforme sua preferência ou sua necessidade, com opção de liso ou impresso em até seis cores.</p>
                <h2>Saco PE reciclado</h2>
                <p>O <strong>saco PE</strong> é uma das embalagens que podem ser 100% recicladas e utilizadas novamente, resultando em custos ainda menores. E existem três tipos de opções de fabricação no caso da embalagem reciclada, veja:</p>
                
                <ul class="list">
                    <li><strong>saco PE</strong> reciclado cristal: é fabricado a partir de sobra ou descarte de plástico virgem. Por causa do processo de reciclagem, tem aspecto amarelado e tem pontos, mas o aspecto visual do saco permanece sendo transparente, possibilitando a visualização interna da embalagem.</li>
                    <li><strong>saco PE</strong> reciclado canela: é feito com sobras ou descartes de plástico reciclado cristal, entre outros, o que propicia um aspecto tom de canela para a embalagem. Assim como o reciclado cristal, a transparência do saco é mantida.</li>
                    <li><strong>saco PE</strong> reciclado colorido: a produção é feito a partir da mistura de embalagens como sacos, sacos de lixo, sacolas e embalagens para alimentos, entre outros. Geralmente, fica com cor cinza escuro, verde escuro ou preto e perde completamente a transparência.</li>
                </ul>
                
                <p>Vale destacar que tanto o <strong>saco PE</strong> com matéria virgem quanto o reciclado tem qualidade elevada e garantem total proteção para o seu produto. É por isso que o material é adotado por laboratórios, editoras, gráficas, confecções, indústrias têxteis, alimentícias, entre outras.</p>
                <p>E para adquirir o <strong>saco PE</strong>, conte com as vantagens da JPR Embalagens. A empresa atua no segmento de embalagens flexíveis a mais de 15 anos, sempre levando até os clientes os melhores produtos e as melhores soluções nesta área.</p>
                <p>Entrando em contato com um dos consultores da JPR Embalagens você fica sabendo mais sobre os diferentes tipos de saco e as vantagens que eles proporcionam. A embalagem pode ser personalizada de acordo com a sua necessidade. Saiba mais entrando em contato e solicite já o seu orçamento.</p>
                
            <? include('inc/saiba-mais.php');?>

            </article>

            <? include('inc/coluna-lateral.php');?>

            <br class="clear" />

            <? include('inc/social-media.php');?>

            <? include('inc/regioes.php');?>

            <? include('inc/paginas-relacionadas.php');?>

            

            <? include('inc/copyright.php');?>
        </section>

    </main>

</div><!-- .wrapper -->
<? include('inc/footer.php');?>
</body>
</html>