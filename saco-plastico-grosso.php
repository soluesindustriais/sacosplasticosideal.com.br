<?
	$h1    		= 'Saco Plástico Grosso';
	$title 		= 'Saco Plástico Grosso';
	$desc  		= 'O saco plástico grosso é um produto que pode ser fabricado em polietileno de alta densidade (PEAD) ou polietileno de baixa densidade e que...';
	$key   		= 'saco plastico Grosso, sacos plastico Grosso, saco plasticos Grosso, saco plastico Grossos, sacos plástico Grosso, saco plásticos Grosso, saco plástico Grossos';
	$legendaImagem 	= 'Foto ilustrativa '.$h1.'';
	$var 		= 'Sacos Plasticos Grosso';
	$produtos	= 'active';
	
	include('inc/head.php');
?>
<!-- função tabs regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
<script src="<?=$url;?>js/tabs.js" type="text/javascript"></script>

<!-- Tabs Regiões -->
<link rel="stylesheet" href="<?=$url;?>css/tabs.css" type="text/css" />
</head>
<body>

<div class="wrapper">
<? include('inc/topo.php');?>

    <main role="main">

        <section>

            <article>
            <?=$caminhoServicosPlasticos?>  
            	<h1><?=$h1?></h1>
                <div class="picture-legend picture-right">
                    <img src="<?=$url;?><?=$pastaSacosPlasticos?><?=$urlGaleria?>-01.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
                </div>
                <p>O <strong>saco plástico grosso</strong> é uma embalagem reforçada e que proporciona ainda mais segurança. Confira maiores informações.</p>
                <p>Alguns materiais precisam ser reforçados para que possam ser armazenados ou transportados de maneira correta. Pensando nisso, foi desenvolvido o <strong>saco plástico grosso</strong>, que é ideal para suportar pesos entre 10 e 30 kg.</p>
                <p>O <strong>saco plástico grosso</strong> é um produto que pode ser fabricado em polietileno de alta densidade (PEAD) ou polietileno de baixa densidade e que é amplamente usado por indústrias têxtis, químicas, de construção civil, metalúrgicas ou automotivas.</p>
                <div class="picture-legend picture-left">
                    <img src="<?=$url;?><?=$pastaSacosPlasticos?><?=$urlGaleria?>-02.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
                </div>
                <p>Geralmente, o <strong>saco plástico grosso</strong> é fabricado com espessura a partir de 200 micras (0,2 mm), fazendo com que a embalagem seja resistente e homogênea. Outra vantagem é a solda para reforçar a embalagem, impedindo que o produto entre em contato com a área externa, ou seja, saia da embalagem. O <strong>saco plástico grosso</strong> ainda possui resistência ao frio, à tração e ao impacto, além de serem flexíveis.</p>
                <h2>Saco plástico grosso ecologicamente correto</h2>
                <p>Uma maneira de contribuir com o meio ambiente é investindo em <strong>saco plástico grosso</strong> feito com matéria-prima reciclada. Além de cuidar da sustentabilidade do planeta, esta embalagem é uma forma de melhorar a imagem da sua empresa no mercado, justamente pelo alinhamento com as causas ambientais e também uma forma de reduzir significativamente os custos com embalagem, que são bem menores do que embalagens feitas com matéria-prima 100% virgem. </p>
                <p>No caso de <strong>saco plástico grosso</strong>, há três opções, confira:</p>
                <div class="picture-legend picture-right">
                    <img src="<?=$url;?><?=$pastaSacosPlasticos?><?=$urlGaleria?>-03.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
                </div>
                <ul class="list">
                    <li>Reciclado cristal: é feito a partir de aparas de material virgem, combinado com embalagens que já foram recicladas, como de alimentos, sacarias ou sacolas. Por causa do processo de reciclagem, a embalagem adquire alguns pontos e aspecto amarelado, mas a transparência é mantida.</li>
                    <li>Reciclado canela: o saco é fabricado a partir de aparas de plásticos reciclados, ficando com um tom canela, mas ainda permitindo a visualização do que há dentro da embalagem, uma vez que a transparência é mantida. Este tipo de embalagem mantém a resistência e a segurança contra rasgos e rupturas, por ter solda reforçada.</li>
                    <li>Reciclado colorido: neste caso, o <strong>saco plástico grosso</strong> é feito com uma mistura de embalagens recicladas, ficando sem padrão de cor e perdendo a transparência. Esta é uma das opções mais baratas no segmento de embalagens plásticas flexíveis.</li>
                </ul>
                    
                <p>Para adquirir o <strong>saco plástico grosso</strong>, aproveite as vantagens da JPR Embalagens. A empresa atua na área de embalagens plásticas flexíveis há mais de 15 anos, levando as melhores soluções para os clientes, além de ter atendimento totalmente personalizado. Confira entrando em contato com os consultores e solicite já seu orçamento.</p>
                
            <? include('inc/saiba-mais.php');?>

            </article>

            <? include('inc/coluna-lateral.php');?>

            <br class="clear" />

            <? include('inc/social-media.php');?>

            <? include('inc/regioes.php');?>

            <? include('inc/paginas-relacionadas.php');?>

            

            <? include('inc/copyright.php');?>
        </section>

    </main>

</div><!-- .wrapper -->
<? include('inc/footer.php');?>
</body>
</html>