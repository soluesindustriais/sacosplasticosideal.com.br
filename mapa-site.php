<?
	$h1    		= 'Mapa do site';
	$title 		= 'Mapa do site';
	$desc  		= 'Atalhos para todas as páginas deste site...';
	$key   		= 'atalho para as páginas do site, mapa do site';
	$var   		= 'Mapa do site';
	
	include('inc/head.php');
?>
</head>
<body>

<div class="wrapper">
<? include('inc/topo.php');?>

    <main role="main">

        <section>
			<?=$caminho?>

            <h1><?=$h1?></h1>

            <ul class="sitemap">
                <? include('inc/menu-top.php');?>
            </ul>
            
            <br class="clear" />
        </section>

    </main>
    
</div><!-- .wrapper -->
<? include('inc/footer.php');?>
</body>
</html>