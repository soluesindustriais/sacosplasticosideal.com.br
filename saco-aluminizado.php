<?
	$h1    		= 'Saco Aluminizado';
	$title 		= 'Saco Aluminizado';
	$desc  		= 'Amplamente utilizado no segmento de alimentação, o saco aluminizado protege o produto que está embalado. Confira maiores detalhes e informações.';
	$key   		= 'sacos aluminizados, sacos aluminizado, saco aluminizados';
	$legendaImagem 	= 'Foto ilustrativa '.$h1.'';
	$var 		= 'Sacos Aluminizados';
	$produtos	= 'active';
	
	include('inc/head.php');
?>
<!-- função tabs regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
<script src="<?=$url;?>js/tabs.js" type="text/javascript"></script>

<!-- Tabs Regiões -->
<link rel="stylesheet" href="<?=$url;?>css/tabs.css" type="text/css" />
</head>
<body>

<div class="wrapper">
<? include('inc/topo.php');?>

    <main role="main">

        <section>

            <article>
            <?=$caminhoServicosDiversos?>  
            	<h1><?=$h1?></h1>
                <div class="picture-legend picture-right">
                    <img src="<?=$url;?><?=$pastaSacosDiversos?><?=$urlGaleria?>-01.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
              	</div>
                <p>Amplamente utilizado no segmento de alimentação, o <strong>saco aluminizado</strong> protege o produto que está embalado. Confira maiores detalhes e informações.</p>
                <p>A embalagem é um aspecto fundamental para o seu produto. O modelo escolhido para embalar, armazenar e transportar o seu produto deve garantir segurança e proteção extremas, com o objetivo de fazer com que ele passe por estas três fases sem que haja qualquer tipo de comprometimento na qualidade ou problemas como rasgos, rupturas, arranhões, problemas relacionados a quedas, entre outros.</p>
                <p>Além disso, outra vantagem de contar com embalagens de qualidade é a imagem que a sua empresa passa no mercado. Muitas vezes, o primeiro contato de um consumidor com uma marca ocorre justamente por meio da embalagem. Por isso, uma opção de qualidade é o <strong>saco aluminizado</strong>.</p>
                <div class="picture-legend picture-left">
                    <img src="<?=$url;?><?=$pastaSacosDiversos?><?=$urlGaleria?>-02.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
              	</div>
                <p>O <strong>saco aluminizado</strong> é um produto que é fabricado em estruturas de polietileno combinado com alumínio. Ele pode ser confeccionado em diversos tipos de modelos, soldas, cortes e tamanhos, de modo a elevar ainda mais a extrema proteção e a segurança que este tipo de embalagem já proporciona para o seu produto.</p>
                <p>O <strong>saco aluminizado</strong> pode ser feito em medidas personalizadas, conforme as necessidades de cada cliente. Também é possível personalizar outros aspectos deste tipo de embalagem, deixando-a lisa ou impressa, transparente ou pigmentada em até seis cores diferentes.</p>
                <p>Por ser um produto de qualidade garantida, o <strong>saco aluminizado</strong> é amplamente utilizado em diversos segmentos, especialmente no alimentício, por conservar a qualidade de produtos como salgadinhos, café, entre outros.</p>
                <h2>Saco aluminizado com preço em conta e ótimas condições</h2>
                <div class="picture-legend picture-right">
                    <img src="<?=$url;?><?=$pastaSacosDiversos?><?=$urlGaleria?>-03.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
              	</div>
                <p>Para pedir o <strong>saco aluminizado</strong>, conte com todo o profissionalismo da JPR Embalagens. A empresa tem atuação há mais de 15 anos no mercado, mais especificamente no segmento de embalagens plásticas flexíveis, levando até os clientes as soluções mais eficientes nesta área.</p>
                <p>O <strong>saco aluminizado</strong> da JPR Embalagens é fabricado com materiais de primeira linha, para assegurar proteção e segurança. Além disso, a empresa dispõe de preços em conta e ótimas condições de pagamento, pelo fato de ter equipamentos modernos e equipe sempre atualizada para reduzir custos e sempre elevar a qualidade dos produtos.</p>
                <p>O atendimento da JPR Embalagens é totalmente personalizado e voltado às necessidades específicas de cada cliente. Saiba mais entrando em contato e solicitando o seu orçamento de <strong>saco aluminizado</strong>.</p>
                
            <? include('inc/saiba-mais.php');?>

            </article>

            <? include('inc/coluna-lateral.php');?>

            <br class="clear" />

            <? include('inc/social-media.php');?>

            <? include('inc/regioes.php');?>

            <? include('inc/paginas-relacionadas.php');?>

            

            <? include('inc/copyright.php');?>
        </section>

    </main>

</div><!-- .wrapper -->
<? include('inc/footer.php');?>
</body>
</html>