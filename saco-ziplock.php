<?
	$h1    		= 'Saco Ziplock';
	$title 		= 'Saco Ziplock';
	$desc  		= 'O saco ziplock é uma opção de embalagem moderna e segura, que é feita em polietileno de baixa densidade e ainda pode ser personalizado...';
	$key   		= 'saco Ziplock, sacos Ziplocks, sacos Ziplock, saco Ziplocks, sacos com Ziplocks, sacos com Ziplock, saco com Ziplocks';
	$legendaImagem 	= 'Foto ilustrativa '.$h1.'';
	$var 		= 'Sacos Ziplock';
	$produtos	= 'active';
	
	include('inc/head.php');
?>
<!-- função tabs regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
<script src="<?=$url;?>js/tabs.js" type="text/javascript"></script>

<!-- Tabs Regiões -->
<link rel="stylesheet" href="<?=$url;?>css/tabs.css" type="text/css" />
</head>
<body>

<div class="wrapper">
<? include('inc/topo.php');?>

    <main role="main">

        <section>

            <article>
            <?=$caminhoServicosDiversos?>  
            	<h1><?=$h1?></h1>
                <div class="picture-legend picture-right">
                    <img src="<?=$url;?><?=$pastaSacosDiversos?><?=$urlGaleria?>-01.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
                </div>
                <p>Utilize uma embalagem segura e moderna para armazenar e transportar os seus produtos. Conheça os benefícios do <strong>saco ziplock</strong>.</p>
                <p>Uma das queixas de muitos consumidores é dificuldade de manuseio de embalagens, especialmente para abri-las ou fechá-las. Por isso, uma opção altamente recomendada é o <strong>saco ziplock</strong>.</p>
                <p>O <strong>saco ziplock</strong> é uma opção de embalagem moderna e segura, que é feita em polietileno de baixa densidade e ainda pode ser personalizado, sendo liso ou impresso em até seis cores. A embalagem tem infinitas aplicações devido às vantagens que ela proporciona, tais como a resistência, o fato de ser um produto atóxico e por garantir a proteção contra umidade e poeira.</p>
                <div class="picture-legend picture-left">
                    <img src="<?=$url;?><?=$pastaSacosDiversos?><?=$urlGaleria?>-02.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
                </div>
                <p>O <strong>saco ziplock</strong> é uma embalagem bastante prática e moderno. O fecho zip é confeccionado em formato de dois trilhos (sistema macho e fêmea), feito em polietileno e colocado na parte superior da embalagem. Devido a este sistema e modo de fabricação, a embalagem pode ser aberta inúmeras vezes sem que haja perda da aderência.</p>
                <p>O <strong>saco ziplock</strong> é uma embalagem bastante prática e versátil e este é um novo argumento de venda para a sua empresa. Seu consumidor certamente notará as vantagens de ter uma embalagem simples na hora de manusear os produtos.</p>
                <p>Existe também uma opção de <strong>saco ziplock</strong> ecologicamente correta. Trata-se do saco fabricado com aditivo oxibiodegradável, que tem a função de fazer com que a embalagem se degrade em um período de até seis meses quando entra em contato com o meio ambiente. Esta é uma forma de melhorar a imagem da sua empresa, por indicar o alinhamento com causas ambientais.</p>
                <h2>Saco ziplock da JPR Embalagens</h2>
                <div class="picture-legend picture-right">
                    <img src="<?=$url;?><?=$pastaSacosDiversos?><?=$urlGaleria?>-03.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
                </div>
                <p>Para adquirir o <strong>saco ziplock</strong>, conte com os profissionais da JPR Embalagens. Nossa empresa está no mercado há mais de 15 anos, sempre desenvolvendo e levando até os clientes as melhores soluções na área de embalagens flexíveis.</p>
                <p>Nossa equipe de profissionais está constantemente atualizada e em busca de inovações que visem aumentar ainda mais a qualidade dos nossos produtos e reduzir custos de produção, que se traduzem em preços menores também para os clientes.</p>
                <p>Nossos consultores disponibilizam um atendimento totalmente personalizado e voltado para as suas necessidades. Saiba mais e aproveite os benefícios entrando em contato. Solicite também o seu orçamento.</p>
                
            <? include('inc/saiba-mais.php');?>

            </article>

            <? include('inc/coluna-lateral.php');?>

            <br class="clear" />

            <? include('inc/social-media.php');?>

            <? include('inc/regioes.php');?>

            <? include('inc/paginas-relacionadas.php');?>

            

            <? include('inc/copyright.php');?>
        </section>

    </main>

</div><!-- .wrapper -->
<? include('inc/footer.php');?>
</body>
</html>