<?
	$h1    			= 'Saco Zip';
	$title 			= 'Saco Zip';
	$desc  			= 'Invista em embalagens que ofereçam qualidade e tenham fechamento simples. Conte com o saco zip. Confira as vantagens.';
	$key   			= 'sacos zip';
	$var 			= 'Sacos Zip';
	$home 			= 'active';
	
	include('inc/head.php');
?>
<!-- função tabs regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
<script src="<?=$url;?>js/tabs.js" type="text/javascript"></script>

<!-- Tabs Regiões -->
<link rel="stylesheet" href="<?=$url;?>css/tabs.css" type="text/css" />
</head>

<body>
<div class="wrapper">
	<? include('inc/topo.php');?>
    <main role="main">

        <section>

            <article>  
                <h1><?=$h1?></h1>
                
                <? include('inc/social-media.php');?>
                
                <ul class="box">
                	<li>
                    	<h2><a href="<?=$url?>sacos-diversos" title="Sacos Diversos">Sacos Diversos</a></h2>
                        <a href="<?=$url?>sacos-diversos" title="Sacos Diversos"><img class="picture-left" src="<?=$url;?>imagens/sacos-diversos.jpg" alt="<?=$h1?>" title="<?=$var?>" /></a>
                        <p>Confira nossos tipos de Sacos, são diversos modelos: Saco Adesivado Transparente, Saco Aluminizado, Saco Antiestático, Saco Bopp, Saco com Fecho, Saco de Entulho e muito mais. Confira!</p>
                        <a class="button" href="<?=$url?>sacos-diversos" title="<?=$h1?>">Saiba mais</a>
                    </li>
                    <li>
                    	<h2><a href="<?=$url?>sacos-plasticos" title="Sacos Plásticos">Sacos Plásticos</a></h2>
                        <a href="<?=$url?>sacos-plasticos" title="Sacos Plásticos"><img class="picture-left" src="<?=$url;?>imagens/sacos-plasticos.jpg" alt="<?=$h1?>" title="<?=$var?>" /></a>
                        <p>Fabricamos diversos tipos de Sacos Plásticos, confira nossos modelos: Saco Plástico Bolha, Saco Plástico Branco, Saco Plástico Branco Leitoso, Saco Plástico com Fecho Ziplock e mais. Veja!</p>
                         <a class="button" href="<?=$url?>sacos-plasticos" title="<?=$h1?>">Saiba mais</a>
                    </li>
                </ul>
                
            <p>Invista em embalagens que ofereçam qualidade e tenham fechamento simples. Conte com o <strong>saco zip</strong>. Confira as vantagens.</p>
            <p>Na hora de escolher uma embalagem para embalar ou transportar os seus produtos, alguns pontos devem ser levados em consideração: a qualidade do material utilizado, a segurança e a proteção que este material oferece para o seu produto e também outras questões mais práticas, como o fechamento. E é neste ponto que o <strong>saco zip</strong> mais se destaca.</p>
            <p>O <strong>saco zip</strong> é um tipo de embalagem que tem um sistema de fechamento moderno e bastante prático. Apenas o fecho zip continua protegendo e garantindo a integridade do produto após a abertura. Afinal, basta fechar novamente o zip para que a embalagem fique completamente lacrada.</p>
            <p>Devido a esta praticidade e ao fato de ser uma embalagem que proporciona segurança, proteção e é inovadora, o <strong>saco zip</strong> tem sido amplamente utilizado nas áreas alimentícias, de confecção, gráficas, laboratórios, entre outros.</p>
            <p>O <strong>saco zip</strong> transmite uma imagem positiva da sua marca, indicando que a sua empresa deseja oferecer sempre o melhor e inovar. Outra vantagem é a possibilidade de personalização conforme preferência e necessidade de cada cliente. O cliente pode desenvolver o <strong>saco zip</strong> da maneira que preferir, personalizando divulgando sua marca, por exemplo, para alcançar novos mercados.</p>
            <h2>Saco zip oxibiodegradável</h2>
            <p>Uma opção para cuidar do meio ambiente é o <strong>saco zip</strong> fabricado com aditivo oxibiodegradável. O objetivo deste material é fazer com que a embalagem não deixe resíduos nocivos ao meio ambiente e se degrade rapidamente, em um período de até seis meses em contato com a natureza.</p>
            <p>Na JPR Embalagens você encontra o <strong>saco zip</strong> comum ou este com aditivo oxibiodegradável, além de soluções personalizadas para a sua empresa. A JPR Embalagens atua há mais de 15 anos na área de embalagens flexíveis, com opções de qualidade para a sua marca.</p>
            <p>Os profissionais da empresa estão constantemente atualizados e sempre em busca de inovações que visam aumentar ainda mais a qualidade dos nossos produtos e reduzir custos, fazendo com que os preços para os clientes sejam reduzidos.</p>
            <p>Entre em contato com um dos consultores da JPR Embalagens para ter mais informações sobre o <strong>saco zip</strong>. Aproveite os benefícios e solicite já o seu orçamento.</p>
                	
             <? include('inc/saiba-mais.php');?>

            </article>

            <? include('inc/coluna-lateral.php');?>

            <br class="clear" />

            <? include('inc/social-media.php');?>

            <? include('inc/regioes.php');?>

            <? include('inc/paginas-relacionadas.php');?>

            

            <? include('inc/copyright.php');?>
        </section>

    </main>

</div><!-- .wrapper -->
<? include('inc/footer.php');?>
</body>
</html>