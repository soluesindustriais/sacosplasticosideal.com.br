<?
	$h1    		= 'Saco Plástico de Segurança';
	$title 		= 'Saco Plástico de Segurança';
	$desc  		= 'O saco plástico de segurança são fabricados tanto em polietileno de baixa densidade, também conhecido pela sigla PEBD, como polietileno de alta densidade...';
	$key   		= 'saco plastico Segurança, sacos plastico Segurança, saco plasticos Segurança, saco plastico Seguranças, sacos plástico Segurança, saco plásticos Segurança';
	$legendaImagem 	= 'Foto ilustrativa '.$h1.'';
	$var 		= 'Sacos Plasticos de Segurança';
	$produtos	= 'active';
	
	include('inc/head.php');
?>
<!-- função tabs regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
<script src="<?=$url;?>js/tabs.js" type="text/javascript"></script>

<!-- Tabs Regiões -->
<link rel="stylesheet" href="<?=$url;?>css/tabs.css" type="text/css" />
</head>
<body>

<div class="wrapper">
<? include('inc/topo.php');?>

    <main role="main">

        <section>

            <article>
            <?=$caminhoServicosPlasticos?>  
            	<h1><?=$h1?></h1>
                <div class="picture-legend picture-right">
                    <img src="<?=$url;?><?=$pastaSacosPlasticos?><?=$urlGaleria?>-01.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
                </div>
                <p>Existem embalagens que são reforçados para aguentar um peso maior, como é o caso do <strong>saco plástico de segurança</strong>. Confira as vantagens.</p>
                <p>As embalagens devem ser sempre produzidas com material de qualidade para a proteção adequada dos seus produtos. Por isso, conheça o <strong>saco plástico de segurança</strong>.</p>
                <p>O <strong>saco plástico de segurança</strong> são fabricados tanto em polietileno de baixa densidade, também conhecido pela sigla PEBD, como polietileno de alta densidade (PEAD), e são ideais para suportar pesos entre 10 e 30 kg.</p>
                <div class="picture-legend picture-left">
                    <img src="<?=$url;?><?=$pastaSacosPlasticos?><?=$urlGaleria?>-02.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
                </div>
                <p>Além disso, o <strong>saco plástico de segurança</strong> é feito com espessura a partir de 0,2 mm, ou 200 micras. Assim, consegue-se uma embalagem homogênea e resistente, que é complementada por uma solda para reforço, fazendo com que o produto não saia da embalagem.</p>
                <p>O <strong>saco plástico de segurança</strong> ainda possui resistência ao frio, são flexíveis e resistentes à tração e ao impacto. Por todas estas vantagens, é uma embalagem de ampla utilização no mercado, especialmente em indústrias têxteis, metalúrgicas, químicas, automotivas e de construção civil.</p>
                <div class="picture-legend picture-right">
                    <img src="<?=$url;?><?=$pastaSacosPlasticos?><?=$urlGaleria?>-03.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
                </div>
                <h2>Saco plástico de segurança reciclado</h2>
                <p>É possível ainda adquirir <strong>saco plástico de segurança</strong> desenvolvido pensando na sustentabilidade do planeta, que são os reciclados. Há três opções, confira:</p>
                
                <ul class="list">
                    <li>Reciclado cristal: nesta opção, o <strong>saco plástico de segurança</strong> é feito misturando aparas de material virgem e embalagens que já foram recicladas, como embalagens de alimentos, sacolas ou sacarias. O produto fica amarelo claro e com alguns pontos devido à reciclagem, porém mantém a transparência.</li>
                    <li>Reciclado canela: é produzido a partir de mistura de plásticos reciclados, ficando com um aspecto cor de canela (marrom claro), mas mantendo a transparência interna.</li>
                    <li>Reciclado colorido: é feito a partir da mistura de uma série de embalagens recicláveis, ficando sem padrão de cor e sem transparência.</li>
                </ul>
                
                <p>Além da questão ambiental, outra vantagem é a boa imagem da sua marca no mercado, justamente por indicar que a sua empresa está preocupada com a sustentabilidade do planeta. Mais um benefício é a redução em torno de 30% dos custos da embalagem com material reciclado em relação a embalagens convencionais.</p>
                <p>Para adquirir o <strong>saco plástico de segurança</strong>, aproveite as vantagens da JPR Embalagens, com preços reduzidos e ótimas condições de pagamento. Com mais de 15 anos de presença na área de embalagens flexíveis, a empresa tem as melhores soluções e dispõe de um atendimento totalmente personalizado. Confira e solicite seu orçamento.</p>
                
            <? include('inc/saiba-mais.php');?>

            </article>

            <? include('inc/coluna-lateral.php');?>

            <br class="clear" />

            <? include('inc/social-media.php');?>

            <? include('inc/regioes.php');?>

            <? include('inc/paginas-relacionadas.php');?>

            

            <? include('inc/copyright.php');?>
        </section>

    </main>

</div><!-- .wrapper -->
<? include('inc/footer.php');?>
</body>
</html>