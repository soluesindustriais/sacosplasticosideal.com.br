<?
	$h1    		= 'Solicite um Orçamento';
	$title 		= 'Solicite um Orçamento';
	$desc  		= 'Solicite um Orçamento da embalagem desejada e em breve entraremos em contato';
	$key   		= '';
	$var   		= 'Solicite um Orçamento';
	$contato	= 'active';
	 	
	include('inc/head.php');
	
?>
<script src="<?=$url?>js/validation.js" type="text/javascript"></script>
</head>
<body>

<div class="wrapper">
<? include('inc/topo.php');?>

    <main role="main">

        <section>
            
            <div class="container contact">
                <?=$caminho?>
                <h1><?=$h1?></h1>
                
                <div class="form">
                    <!--        
                    <form enctype="multipart/form-data" id="formContato" method="post" name="contato-envia" action="contato-envia" class="form2">
                        <label for="nome">Nome:<span>*</span> </label>
                            <input onKeyUp="UcWords(this)" type="text" name="nome" class="nome" value="" id="nome" size="43"/>
                            
                        <label for="email">E-mail:<span>*</span> </label>
                            <input onKeyUp="minusculas(this)" type="text" name="email" class="email" value="" id="email" size="43"/>
                            
                        <label for="telefone">DDD/Telefone:<span>*</span> </label>
                            <input type="text" name="ddd" class="ddd" value="" id="ddd" size="2" maxlength="2"/>
                            <input type="text" name="telefone" class="telefone" value="" id="telefone" size="34" maxlength="9"/>
                            
                        <label>Como nos conheceu?:<span>*</span> </label>
                            <select name="como_conheceu">
                                <option value="">Selecione</option>
                                <option  value="Busca do Google">Busca do Google</option>
                                <option  value="Outros Buscadores">Outros Buscadores</option>
                                <option  value="Links patrocinados">Links patrocinados</option>
                                <option  value="Outros Anúncios">Outros Anúncios</option>
                                <option  value="Facebook">Facebook</option>
                                <option  value="Twitter">Twitter</option>
                                <option  value="Google+">Google+</option>
                                <option  value="Indicação">Indicação</option>
                                <option  value="Outros">Outros</option>
                            </select>
                            
                        <label>Mensagem:<span>*</span> </label>
                            <textarea name="mensagem" cols="37" rows="10"></textarea>
                            
                        <span class="bt-submit">
                            <input type="submit" value="Enviar" class="ir" />
                        </span>
                    </form>
                    <span class="obrigatorio">Os campos com * são obrigatórios</span>
                    
                </div>
                    -->

                    <iframe src="http://www.embalagensflexiveis.com.br/formulario/pre-orcamento.php" width="549" height="950"></iframe>
                    
                </div>    
                <div class="contact-form">
                    <h3><?=$nomeSite;?> - <?=$slogan;?></h3>
                    <strong><?=$rua;?> - <?=$bairro;?> - <?=$cidade;?> - <?=$UF;?> - <?=$cep;?></strong><br />
<strong><?=$ddd.' '.$fone;?></strong><br /><br />
                    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d6347.3955952484275!2d-46.718309!3d-23.666203!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce51b3b5029ae3%3A0x66d0f4ff02b068c1!2sRua+Periperi%2C+51+-+Vila+Socorro!5e1!3m2!1spt-BR!2sbr!4v1396436694786" width="400" height="300" style="border:0"></iframe>
                </div>
            </div>
                

        </section>

    </main>
	<br class="clear" />

</div><!-- .wrapper -->
<? include('inc/footer.php');?>
</body>
</html>