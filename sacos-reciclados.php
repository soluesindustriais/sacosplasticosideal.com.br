<?
	$h1    		= 'Sacos Reciclados';
	$title 		= 'Sacos Reciclados';
	$desc  		= 'Os sacos reciclados podem ser feitos em vários modelos e cores. O produto é resistente a quedas, rasgos, arranhões e rupturas, garantindo...';
	$key   		= 'saco reciclado, sacos reciclado, saco Reciclados';
	$legendaImagem 	= 'Foto ilustrativa '.$h1.'';
	$var 		= 'Saco Reciclado';
	$produtos	= 'active';
	
	include('inc/head.php');
?>
<!-- função tabs regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
<script src="<?=$url;?>js/tabs.js" type="text/javascript"></script>

<!-- Tabs Regiões -->
<link rel="stylesheet" href="<?=$url;?>css/tabs.css" type="text/css" />
</head>
<body>

<div class="wrapper">
<? include('inc/topo.php');?>

    <main role="main">

        <section>

            <article>
            <?=$caminhoServicosDiversos?>  
            	<h1><?=$h1?></h1>
                <div class="picture-legend picture-left">
                    <img src="<?=$url;?><?=$pastaSacosDiversos?><?=$urlGaleria?>-01.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
              	</div>
                <p><strong>Sacos reciclados</strong> tem qualidade elevada e ainda contribuem com o meio ambiente. Confira maiores informações.</p>
                <p>A preocupação com o meio ambiente é um ponto fundamental para manter a sustentabilidade do planeta. Além disso, este é um novo argumento de venda, afinal os consumidores enxergam como algo positivo o fato de a sua empresa estar alinhada com causas ambientais. Por isso, confira as opções de <strong>sacos reciclados</strong>.</p>
                <div class="picture-legend picture-right">
                    <img src="<?=$url;?><?=$pastaSacosDiversos?><?=$urlGaleria?>-02.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
              	</div>
                <p>Os <strong>sacos reciclados</strong> podem ser feitos em vários modelos e cores. O produto é resistente a quedas, rasgos, arranhões e rupturas, garantindo total proteção ao que está embalado durante a armazenagem ou o transporte. Além disso, este tipo de embalagem é muito mais econômica do que os sacos convencionais.</p>
                <h2>Modelos de sacos reciclados</h2>
                <p>Os <strong>sacos reciclados</strong> podem ser fabricados em três modelos diferentes, no tipo cristal, canela e colorido. Veja as diferenças entre eles:</p>
                
                <ul class="list">
                    <li>Cristal: os <strong>sacos reciclados</strong> feitos com cristal são fabricados a partir de aparas de material virgem, misturadas com embalagens que já foram recicladas, como é o caso de embalagens de alimentos, sacolas plásticas e sacarias. Devido ao processo de reciclagem, a embalagem adquire um tom amarelado e tem alguns pontos. Vale destacar que ainda assim é possível ver o que está sendo transportado, já que a transparência é mantida.</li>
                    <li>Canela: <strong>sacos reciclados</strong> do tipo canela são os feitos com aparas de plásticos reciclados. Ele tem esse nome justamente pela cor da embalagem, que fica canela (marrom claro), mas ainda mantendo a transparência, o que permite visualização do produto que está dentro da embalagem. Este produto mantém resistência e segurança a rasgos e rupturas, já que é confeccionado com solda reforçada.</li>
                    <li>Colorido: já os <strong>sacos reciclados</strong> são feitos com mistura de embalagens recicladas, ficando sem cor padrão e também sem transparência. É uma das opções mais baratas na área de embalagens plásticas flexíveis.</li>
                </ul>
                <div class="picture-legend picture-left">
                    <img src="<?=$url;?><?=$pastaSacosDiversos?><?=$urlGaleria?>-03.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
              	</div>
                <p>Para adquirir os <strong>sacos reciclados</strong>, conte com as vantagens da JPR Embalagens. A empresa atua no mercado há mais de 15 anos e traz as melhores soluções na área de embalagens flexíveis, conforme cada necessidade.</p>
                <p>Os <strong>sacos reciclados</strong> podem ser totalmente personalizados de acordo com a preferência e a necessidade dos clientes, além de poderem ser lisos ou impressos em até seis cores. Entre em contato para saber maiores informações sobre a empresa e solicite seu orçamento informando medidas e quantidade necessária.</p>
                
            <? include('inc/saiba-mais.php');?>

            </article>

            <? include('inc/coluna-lateral.php');?>

            <br class="clear" />

            <? include('inc/social-media.php');?>

            <? include('inc/regioes.php');?>

            <? include('inc/paginas-relacionadas.php');?>

            

            <? include('inc/copyright.php');?>
        </section>

    </main>

</div><!-- .wrapper -->
<? include('inc/footer.php');?>
</body>
</html>